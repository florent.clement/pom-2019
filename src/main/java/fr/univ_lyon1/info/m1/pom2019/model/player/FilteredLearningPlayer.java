package fr.univ_lyon1.info.m1.pom2019.model.player;

import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.strategy.AbstractStrategy;
import fr.univ_lyon1.info.m1.pom2019.strategy.FilteredLearningStrategy;

public class FilteredLearningPlayer extends LearningPlayer {

    public FilteredLearningPlayer(int[][] checkers, boolean whiteColor, Player opponent) {
        super(checkers, whiteColor, opponent);
    }

    @Override
    public int getStrategyId() {
        return 2;
    }

    @Override
    public String getNameIa() {
        return "Naive player using filtered learning strategy";
    }

    @Override
    public AbstractStrategy<?, ?> getStrategy(
            AbstractPawn abstractPawn,
            Player player,
            Player opponent) {
        return new FilteredLearningStrategy<>(
                abstractPawn,
                player,
                opponent
        );
    }
}
