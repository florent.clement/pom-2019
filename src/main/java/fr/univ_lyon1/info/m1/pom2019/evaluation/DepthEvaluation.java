package fr.univ_lyon1.info.m1.pom2019.evaluation;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.app.ConsoleApp;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.globals.Tools;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import scala.Tuple4;
import scala.Tuple5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The type Depth evaluation.
 */
public abstract class DepthEvaluation implements Evaluate {

    private static final Map<String, Integer> preCalc = new HashMap<>();
    /**
     * The Evaluate.
     */
    Evaluate evaluateRecur;


    /**
     * Instantiates a new Depth evaluation.
     *
     * @param evaluateRecur the evaluate
     */
    public DepthEvaluation(Evaluate evaluateRecur) {
        this.evaluateRecur = evaluateRecur;
    }

    /**
     * Reset pre calc.
     */
    public static void resetPreCalc() {
        preCalc.clear();
    }

    private static StringBuilder boardToString(int[][] board) {
        StringBuilder stringBuilder = new StringBuilder();
        ConsoleApp.printCheckers(board, stringBuilder);
        return stringBuilder;
    }

    /**
     * Contains pre calc boolean.
     *
     * @param board the board
     * @param step  the step
     * @return the boolean
     */
    protected static boolean containsPreCalc(int[][] board, Integer step) {
        StringBuilder stringBuilder = boardToString(board);
        stringBuilder.append(step);
        return preCalc.containsKey(stringBuilder.toString());
    }

    /**
     * Gets pre calc.
     *
     * @param board the board
     * @param step  the step
     * @return the pre calc
     */
    protected static Integer getPreCalc(int[][] board, Integer step) {
        StringBuilder stringBuilder = boardToString(board);
        stringBuilder.append(step);
        return preCalc.get(stringBuilder.toString());
    }

    /**
     * Add pre calc.
     *
     * @param board the board
     * @param val   the val
     * @param step  the step
     */
    protected static void addPreCalc(int[][] board, Integer val, Integer step) {
        StringBuilder stringBuilder = boardToString(board);
        stringBuilder.append(step);
        preCalc.put(stringBuilder.toString(), val);
    }

    /**
     * Gets all possible boards.
     *
     * @param currentBoard the current board
     * @param owner        the owner
     * @return the all possible owner boards
     */
    protected final List<Tuple5<int[][], AbstractPawn, Boolean,
            List<AbstractPawn>, List<AbstractPawn>>> getAllPossibleBoardsOpponent(
            int[][] currentBoard, AbstractPawn abstractPawn,
            List<AbstractPawn> owner, List<AbstractPawn> opponent) {
        return this.getAllPossibleBoards(
                currentBoard,
                null,
                opponent,
                owner
        ).stream().map(e ->
                new Tuple5<>(e._1(), abstractPawn, owner.contains(abstractPawn),
                        e._4(), e._3())).collect(Collectors.toList());
    }

    /**
     * Gets all possible boards.
     *
     * @param currentBoard the current board
     * @param currentPawn  the current pawn
     * @param owner        the owner
     * @return the all possible owner boards
     */
    protected final List<Tuple4<int[][], AbstractPawn,
            List<AbstractPawn>, List<AbstractPawn>>> getAllPossibleBoards(
            int[][] currentBoard, AbstractPawn currentPawn,
            List<AbstractPawn> owner,
            List<AbstractPawn> opponent) {

        List<Tuple4<int[][], AbstractPawn,
                List<AbstractPawn>, List<AbstractPawn>>> result = new ArrayList<>();


        // owner
        List<List<Action>> listActionOwner = this.getListActionPlayer(currentBoard, owner);

        List<String> boardFilter = new ArrayList<>();

        for (List<Action> actionList : listActionOwner) {

            int[][] newBoard = Tools.applyActionOnTempBoard(currentBoard, actionList);

            AbstractPawn newCurrentPawn = currentPawn;
            List<AbstractPawn> newOwner = new ArrayList<>();

            for (AbstractPawn itP : owner) {
                Action actP = actionList.stream().filter(e -> e.getPiece() == itP)
                        .findFirst().orElse(null);

                if (actP == null) {
                    newOwner.add(itP);
                } else {
                    AbstractPawn newPawn = itP.clone();
                    newPawn.setPosition(actP.getLastMovements());
                    newOwner.add(itP);
                    if (itP == currentPawn) {
                        newCurrentPawn = newPawn;
                    }
                }
            }

            String boardStr = boardToString(newBoard).toString();

            if (!boardFilter.contains(boardStr)) {
                result.add(new Tuple4<>(newBoard, newCurrentPawn,
                        newOwner,
                        opponent.stream().filter(e -> actionList.stream()
                                .noneMatch(c -> c.getCapturedPieces().contains(e.getPosition())))
                                .collect(Collectors.toList())));
                boardFilter.add(boardStr);
            }


        }

        return result;
    }

    private List<List<Action>> getListActionPlayer(int[][] board, List<AbstractPawn> pawnList) {
        List<Action> list = new ArrayList<>();
        for (AbstractPawn abstractPawn : pawnList) {
            list.addAll(abstractPawn.getDplPossible(board));
        }
        return Tools.matchActions(list, Constants.MAX_MOVE, board);
    }

    /**
     * Gets evaluate.
     *
     * @return the evaluate
     */
    public Evaluate getEvaluate() {
        return evaluateRecur;
    }

    /**
     * Reduce move tuple 2.
     *
     * @param listMoves    the list moves
     * @param abstractPawn the abstract pawn
     * @param owner        the owner
     * @param opponent     the opponent
     * @return the tuple 2
     */
    abstract Tuple5<int[][], AbstractPawn, Boolean,
            List<AbstractPawn>, List<AbstractPawn>> maxMove(
            List<Tuple5<int[][], AbstractPawn, Boolean, List<AbstractPawn>,
                    List<AbstractPawn>>> listMoves, AbstractPawn abstractPawn,
            List<AbstractPawn> owner, List<AbstractPawn> opponent);

    /**
     * Min move tuple 2.
     *
     * @param listMoves    the list moves
     * @param abstractPawn the abstract pawn
     * @param owner        the owner
     * @param opponent     the opponent
     * @return the tuple 2
     */
    abstract Tuple4<int[][], AbstractPawn, List<AbstractPawn>, List<AbstractPawn>> minMove(
            List<Tuple4<int[][], AbstractPawn, List<AbstractPawn>, List<AbstractPawn>>> listMoves,
            AbstractPawn abstractPawn, List<AbstractPawn> owner, List<AbstractPawn> opponent);

    @Override
    public final Integer evaluate(int[][] board, AbstractPawn abstractPawn,
                                  List<AbstractPawn> owner, List<AbstractPawn> opponent) {

        if (containsPreCalc(board, 1)) {
            return getPreCalc(board, 1);
        }

        List<Tuple5<int[][], AbstractPawn, Boolean,
                List<AbstractPawn>, List<AbstractPawn>>> allMoves;

        allMoves = this.getAllPossibleBoardsOpponent(
                board,
                abstractPawn,
                owner,
                opponent
        );

        Tuple5<int[][], AbstractPawn, Boolean,
                List<AbstractPawn>, List<AbstractPawn>> max = this.maxMove(
                        allMoves, abstractPawn, owner, opponent);

        Integer value;
        if (max == null) {
            value = 0;
        } else {
            value = this.getEvaluate().evaluate(max._1(), abstractPawn, owner, opponent);
        }
        addPreCalc(board, value, 1);
        return value;
    }

    /**
     * Evaluate integer.
     *
     * @param board        the board
     * @param abstractPawn the abstract pawn
     * @param owner        the owner
     * @param opponent     the opponent
     * @param depth        the depth
     * @param whiteColor   the white color
     * @return the integer
     */
    public Integer evaluate(int[][] board, AbstractPawn abstractPawn,
                            List<AbstractPawn> owner, List<AbstractPawn> opponent,
                            int depth, boolean whiteColor) {

        if (containsPreCalc(board, depth)) {
            return getPreCalc(board, depth);
        }


        if (depth == 1) {
            return this.evaluate(board, abstractPawn, owner, opponent);
        } else {


            List<Tuple5<int[][], AbstractPawn, Boolean, List<AbstractPawn>,
                    List<AbstractPawn>>> allMoves = this.getAllPossibleBoardsOpponent(
                    board,
                    abstractPawn,
                    owner,
                    opponent
            ).stream().filter(Tuple5::_3).collect(Collectors.toList());

            Integer value;

            if (allMoves.isEmpty()) {
                value = 0; //TODO
            } else {


                List<Tuple4<int[][], AbstractPawn, List<AbstractPawn>,
                        List<AbstractPawn>>> result = new ArrayList<>();

                for (Tuple5<int[][], AbstractPawn, Boolean, List<AbstractPawn>,
                        List<AbstractPawn>> move : allMoves) {

                    result.addAll(this.getAllPossibleBoards(
                            move._1(),
                            move._2(),
                            move._4(),
                            opponent
                    ));
                }

                Tuple4<int[][], AbstractPawn, List<AbstractPawn>, List<AbstractPawn>> minMove;
                minMove = this.minMove(result, abstractPawn, owner, opponent);

                if (minMove != null) {
                    value = this.evaluate(
                            minMove._1(),
                            minMove._2(),
                            minMove._3(),
                            minMove._4(),
                            depth - 1,
                            whiteColor
                    );
                } else {
                    value = 0;
                }
            }

            addPreCalc(board, value, depth);
            return value;
        }
    }
}
