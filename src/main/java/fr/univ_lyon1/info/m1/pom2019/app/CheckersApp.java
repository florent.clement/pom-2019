package fr.univ_lyon1.info.m1.pom2019.app;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.logger.LoggerApp;
import fr.univ_lyon1.info.m1.pom2019.view.CaseView;
import fr.univ_lyon1.info.m1.pom2019.view.DameView;
import fr.univ_lyon1.info.m1.pom2019.view.PieceView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Checkerboard display control class.
 */
public class CheckersApp {

    private static GridPane checkers;
    private static List<CaseView> caseViewList;
    private static Map<Integer, Class<? extends PieceView>> mapTypePawn;

    /**
     * Initializes the rendering pane.
     *
     * @param pane GridPane of the interface.
     */
    public static void init(GridPane pane) {
        CheckersApp.checkers = pane;
        mapTypePawn = PieceView.mapTypePawn();
        caseViewList = new ArrayList<>();

        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {
            for (int y = 0; y < Constants.CHECKERS_SIZE; y++) {
                CaseView caseView = new CaseView(x,
                        Constants.CHECKERS_SIZE - 1 - y);
                CheckersApp.checkers.add(caseView, caseView.getX(),
                        Constants.CHECKERS_SIZE - 1 - caseView.getY());
                caseViewList.add(caseView);
            }
        }
    }

    private static void clearGridPane() {
        for (CaseView caseView : caseViewList) {
            if (caseView.containsAPiece()) {
                caseView.removePieceView();
            }
        }
    }

    /**
     * Load the pawn array into the checkerboard.
     *
     * @param checkers the checkers
     */
    public static void fill(int[][] checkers) {
        clearGridPane();

        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {
            for (int y = 0; y < Constants.CHECKERS_SIZE; y++) {

                int id = checkers[x][y];
                CaseView caseView = getCaseView(x, y);

                if (id != 0) {

                    Class<? extends PieceView> classPieceView = mapTypePawn.get(Math.abs(id));

                    try {
                        PieceView pieceView = classPieceView
                                .getConstructor(int.class, CaseView.class)
                                .newInstance(id > 0 ? 0 : 1, caseView);
                        caseView.setPieceView(pieceView);

                    } catch (Exception e) {
                        LoggerApp.error(e.getMessage());
                    }

                }
            }
        }

    }

    /**
     * Retourne la case associé à la position X et Y.
     *
     * @param x Position x dans le damier
     * @param y Position y dans le damier
     * @return Retourne une CaseView si existe sinon null
     */
    public static CaseView getCaseView(int x, int y) {
        return caseViewList.stream()
                .filter(caseView ->
                        caseView.getX() == x && caseView.getY() == y).findAny().orElse(null);
    }

    /**
     * Déplace une pièce d'une position (x1,y1) à (x2,y2).
     *
     * @param x1    Position x de départ
     * @param y1    Position y de départ
     * @param x2    Position x d'arrivée
     * @param y2    Position y d'arrivée
     * @param color couleur
     */
    public static void echange(int x1, int y1, int x2, int y2, Color color) {
        CaseView dep = getCaseView(x1, y1);
        CaseView arr = getCaseView(x2, y2);

        if (dep != null && arr != null) {
            if (dep.containsAPiece()) {

                PieceView pieceView = dep.getPieceView();

                if (arr.containsAPiece()) {
                    LoggerApp.warn(
                            "Une pièce était déjà présente à la position x:" + x2 + ", y:" + y2
                    );
                } else {
                    LoggerApp.message(
                            "Pièce déplacé de la position x:"
                                    + x1 + " y:" + y1 + "à x:" + x2 + ", y:" + y2,
                            color
                    );

                }

                dep.removePieceView();
                arr.setPieceView(pieceView);


                checkers.requestLayout();
            } else {
                LoggerApp.warn(
                        "Aucune pièce pièce déplaçable à la position x:" + x1 + ", y:" + y1
                );
            }
        } else {
            LoggerApp.warn(
                    "Une sélection de pièce a eu lieu hors du damier"
            );
        }
    }

    /**
     * Supprime une pièce du damier.
     *
     * @param x Position en X
     * @param y Position en Y
     */
    public static void removePiece(int x, int y) {

        CaseView caseView = getCaseView(x, y);

        if (caseView != null) {
            caseView.removePieceView();
            LoggerApp.message("Pion supprimé en X:" + x + ", Y:" + y);
        } else {
            LoggerApp.warn("Aucun pion à supprimer en X:" + x + ", Y:" + y);
        }

    }

    /**
     * Transforms piece at x,y coordinates to a queen.
     *
     * @param x     X position.
     * @param y     Y position.
     * @param color Queen color.
     */
    public static void changeToDame(int x, int y, int color) {
        CaseView caseView = getCaseView(x, y);

        if (caseView != null) {
            caseView.removePieceView();
            DameView dameView = new DameView(color, caseView);
            caseView.setPieceView(dameView);
            LoggerApp.message("Pion transformé en dame en X:" + x + ", Y:" + y);
        } else {
            LoggerApp.warn("Aucun pion à changer en X:" + x + ", Y:" + y);
        }
    }

    /**
     * Gets checkers.
     *
     * @return the checkers
     */
    public static GridPane getCheckers() {
        return checkers;
    }
}
