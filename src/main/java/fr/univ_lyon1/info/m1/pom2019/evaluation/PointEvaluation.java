package fr.univ_lyon1.info.m1.pom2019.evaluation;

import com.google.common.collect.ImmutableMap;
import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.ConvertAble;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.Profile;

import java.util.Map;
import java.util.List;
import java.util.stream.Stream;

/**
 * The type Point evaluation.
 */
public class PointEvaluation implements Evaluate {

    private final Map<String, Integer> weights = ImmutableMap.of(
            "protect", 1,
            "offensive", 1,
            "risk", -1,
            "conversion", 1,
            "control", 1
    );

    /**
     * Protect evaluate integer.
     *
     * @param board        the board
     * @param abstractPawn the abstract pawn
     * @param owner        the owner
     * @param coord        the coord
     * @return the integer
     */
    protected Integer protectEvaluate(int[][] board, AbstractPawn abstractPawn,
                                      List<AbstractPawn> owner, Coord coord) {

        return owner.stream().filter(e -> e != abstractPawn)
                .map(e -> e.getBlockCoord(board))
                .filter(coords -> coords.contains(coord)).toArray().length;
    }

    /**
     * Offensive evaluate integer.
     *
     * @param board        the board
     * @param abstractPawn the abstract pawn
     * @return the integer
     */
    protected Integer offensiveEvaluate(int[][] board, AbstractPawn abstractPawn) {
        return abstractPawn.getDplPossible(board).stream()
                .map(Action::getCapturedPieces)
                .distinct().toArray().length;
    }

    /**
     * Risk evaluate integer.
     *
     * @param board        the board
     * @param abstractPawn the abstract pawn
     * @param opponent     the opponent
     * @return the integer
     */
    protected Integer riskEvaluate(int[][] board, AbstractPawn abstractPawn,
                                   List<AbstractPawn> opponent) {

        return opponent.stream()
                .map(e -> e.getDplPossible(board))
                .map(e -> e.stream().filter(a -> a.getCapturedPieces()
                        .contains(abstractPawn.getPosition())).toArray().length)
                .filter(e -> e > 0).toArray().length;
    }

    /**
     * Conversion evaluate integer.
     *
     * @param board        the board
     * @param abstractPawn the abstract pawn
     * @param coord        the coord
     * @return the integer
     */
    protected Integer conversionEvaluate(int[][] board, AbstractPawn abstractPawn, Coord coord) {
        if (abstractPawn instanceof ConvertAble) {
            ConvertAble convertAble = (ConvertAble) abstractPawn;
            List<Class<? extends AbstractPawn>> conversionList =
                    convertAble.getConversionList();
            int result = 0;

            for (Class<? extends AbstractPawn> convertClass : conversionList) {
                if (convertAble.isConvertible(convertClass, board, coord)) {
                    result++;
                }
            }
            return result;
        } else {
            return 0;
        }
    }

    /**
     * Control evaluate integer.
     *
     * @param board        the board
     * @param abstractPawn the abstract pawn
     * @return the integer
     */
    protected Integer controlEvaluate(int[][] board, AbstractPawn abstractPawn) {
        return abstractPawn.getDplPossible(board).stream()
                .map(Action::getLastMovements)
                .distinct().toArray().length;
    }


    @Override
    public Integer evaluate(int[][] board, AbstractPawn abstractPawn,
                            List<AbstractPawn> owner, List<AbstractPawn> opponent) {

        return this.calcProtectEvaluateWithCoefficient(board, abstractPawn, owner)
                + this.calcOffensiveEvaluateWithCoefficient(board, abstractPawn)
                + this.calcRiskEvaluationWithCoefficient(board, abstractPawn, opponent)
                + this.calcConversionEvaluateWithCoefficient(board, abstractPawn)
                + this.calcControlEvaluateWithCoefficient(board, abstractPawn);
    }

    private Integer calcProtectEvaluateWithCoefficient(
            int[][] board, AbstractPawn abstractPawn,  List<AbstractPawn> owner) {
        return this.protectEvaluate(board, abstractPawn, owner, abstractPawn.getPosition())
                * this.weights.get("protect");
    }

    private Integer calcOffensiveEvaluateWithCoefficient(int[][] board, AbstractPawn abstractPawn) {
        return this.offensiveEvaluate(board, abstractPawn)
                * this.weights.get("offensive");
    }

    private Integer calcRiskEvaluationWithCoefficient(
            int[][] board, AbstractPawn abstractPawn, List<AbstractPawn> opponent) {
        return this.riskEvaluate(board, abstractPawn, opponent)
                * this.weights.get("risk");
    }

    private Integer calcConversionEvaluateWithCoefficient(
            int[][] board, AbstractPawn abstractPawn) {
        return this.conversionEvaluate(board, abstractPawn, abstractPawn.getPosition())
                * this.weights.get("conversion");
    }

    private Integer calcControlEvaluateWithCoefficient(
            int[][] board, AbstractPawn abstractPawn) {
        return this.controlEvaluate(board, abstractPawn)
                * this.weights.get("control");
    }

    /**
     * Determine profile profile . type.
     *
     * @param abstractPawn the abstract pawn
     * @param board        the board
     * @param owner        the owner
     * @param opponent     the opponent
     * @return the profile . type
     */
    public Profile.Type determineProfile(AbstractPawn abstractPawn, int[][] board,
                                         List<AbstractPawn> owner, List<AbstractPawn> opponent) {

        int protect = this.calcProtectEvaluateWithCoefficient(board, abstractPawn,owner);
        int offensive = this.calcOffensiveEvaluateWithCoefficient(board, abstractPawn);
        int risk = this.calcRiskEvaluationWithCoefficient(board, abstractPawn, opponent);
        int conversion = this.calcConversionEvaluateWithCoefficient(board, abstractPawn);
        int control = this.calcControlEvaluateWithCoefficient(board, abstractPawn);

        int max = Stream.of(
                protect,
                offensive,
                risk,
                conversion,
                control
        ).max(Integer::compare).get();

        if (max == protect) {
            return Profile.Type.Protect;
        } else if (max == offensive) {
            return Profile.Type.Offensive;
        } else if (max == risk) {
            return Profile.Type.Risk;
        } else if (max == conversion) {
            return Profile.Type.Conversion;
        } else {
            return Profile.Type.Control;
        }
    }
}
