package fr.univ_lyon1.info.m1.pom2019.model.player;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NaivePlayerTest {

    int[][] damier;

    @Before
    public void init() {

        this.damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];

        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {
            for (int y = 0; y < Constants.CHECKERS_SIZE; y++) {
                damier[x][y] = 0;
            }
        }
    }
}