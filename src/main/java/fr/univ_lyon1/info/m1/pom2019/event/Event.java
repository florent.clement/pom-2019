package fr.univ_lyon1.info.m1.pom2019.event;


import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

/**
 * Abstract class Event.
 */
public abstract class Event<G extends Group, V extends Vote> {

    private final Listeners<G, V> source;

    public Event(Listeners<G, V> src) {
        this.source = src;
    }

    public abstract Type typeEvent();

    public Listeners<G, V> getSource() {
        return this.source;
    }

    public abstract int[][] getCurrentCheckers();

    public enum Type {
        MOVE,
        UPDATE_CHECKERS,
        BLOCK,
        POSSIBLE_ACTION,
        EXISTING_GROUP,
        REQUEST_JOIN_GROUP,
        REQUEST_LEAVE_GROUP,
        AUTHORIZE_VOTE_GROUP,
        VOTE,
        PROTECTORS,
        LEARNING_FLAG
    }
}