package fr.univ_lyon1.info.m1.pom2019.learning;

import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Class to generate pawn location surroundings.
 */
public class Location {
    private final int strategyId;
    private final Coord originCoord;
    private final int originId;
    private final int grade;
    private Map<Coord, Integer> surroundings = new LinkedHashMap<>();

    /**
     * Default Location constructor.
     * @param strategyId strategy ID as int. 0 for tests.
     * @param originCoord coord in Location center.
     * @param originId id in Location center.
     * @param grade Location rating.
     */
    public Location(int strategyId, Coord originCoord, int originId, int grade) {
        this.strategyId = strategyId;
        this.originCoord = originCoord;
        this.originId = originId;
        this.grade = grade;
    }

    public int getStrategyId() {
        return strategyId;
    }

    public Coord getOriginCoord() {
        return originCoord;
    }

    public int getOriginId() {
        return originId;
    }

    public int getGrade() {
        return grade;
    }

    public Map<Coord, Integer> getSurroundings() {
        return surroundings;
    }

    public void setSurroundings(Map<Coord, Integer> surroundings) {
        this.surroundings = surroundings;
    }

    /**
     * Method for generation location from board.
     * @param board Board used to compute surroundings.
     * @return generated location.
     */
    public Map<Coord, Integer> generateSurroundings(int[][] board) {
        surroundings = new LinkedHashMap<>();

        for (int x = originCoord.getX() - Constants.LOCATION_RADIUS;
             x <= originCoord.getX() + Constants.LOCATION_RADIUS; ++x) {

            for (int y = originCoord.getY() - Constants.LOCATION_RADIUS;
                 y <= originCoord.getY() + Constants.LOCATION_RADIUS; ++y) {

                if (x < 0 || x >= Constants.CHECKERS_SIZE
                        || y < 0 || y >= Constants.CHECKERS_SIZE) {
                    continue;
                }

                if (board[x][y] != 0) {
                    surroundings.put(new Coord(x, y), board[x][y]);
                }
            }
        }

        return surroundings;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Location)) {
            return false;
        }
        Location location = (Location) obj;
        boolean checkStrategy = strategyId == location.strategyId;
        boolean checkCoord = originCoord.equals(location.originCoord);
        boolean checkId = originId == location.originId;
        boolean checkGrade = grade == location.grade;
        boolean checkMap = false;
        if (surroundings.size() == location.surroundings.size()) {
            checkMap = true;
            for (Map.Entry<Coord, Integer> entry : location.surroundings.entrySet()) {
                checkMap = surroundings.containsKey(entry.getKey())
                        && surroundings.get(entry.getKey()).equals(entry.getValue());
            }
        }

        return checkStrategy && checkCoord && checkId && checkGrade && checkMap;
    }

    @Override
    public int hashCode() {
        return Objects.hash(strategyId, originCoord, originId, grade, surroundings);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("[\n");

        builder.append("strategyId:").append(strategyId).append(";\n");
        builder.append("originCoord:").append(originCoord.toString()).append(";\n");
        builder.append("originId:").append(originId).append(";\n");
        builder.append("grade:").append(grade).append(";\n");

        builder.append("surroundings:{\n");

        Iterator<Map.Entry<Coord, Integer>> iterator = surroundings.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Coord, Integer> entry = iterator.next();
            builder.append(entry.getKey().toString()).append("=")
                    .append(entry.getValue());
            if (iterator.hasNext()) {
                builder.append("\n&\n");
            } else {
                builder.append("\n");
            }
        }

        builder.append("}\n");

        builder.append("]\n");

        return builder.toString();
    }
}
