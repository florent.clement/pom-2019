package fr.univ_lyon1.info.m1.pom2019.view;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;

public class DameView extends PieceView {

    private final Cylinder pawn;

    /**
     * Default constructor for Queens, color is the Piece id and Parent the corresponding CaseView.
     *
     * @param color  color of the piece, should be 2 for white and -2 for black.
     * @param parent CaseView in which the Queen is currently at.
     */
    public DameView(int color, CaseView parent) {
        super(color, parent);
        this.pawn = new Cylinder(Math.min(parent.getWidth(), parent.getHeight()) * 0.4,
                Math.min(parent.getWidth(), parent.getHeight()) * 0.4);
        PhongMaterial material = new PhongMaterial();
        if (color > 0) {
            material.setDiffuseColor(Color.WHITE);
            material.setSpecularColor(Color.BLACK);
        } else {
            int black = 60;
            material.setDiffuseColor(Color.rgb(black, black, black));
            material.setSpecularColor(Color.WHITE);
        }
        this.pawn.setMaterial(material);
    }

    @Override
    Node getNode() {
        return this.pawn;
    }

    @Override
    void resize(CaseView parent) {
        this.parent = parent;
        this.pawn.setRadius(Math.min(parent.getWidth(), parent.getHeight()) * 0.4);
        this.pawn.setHeight(Math.min(parent.getWidth(), parent.getHeight()) * 0.4);
    }

    @Override
    public int getId() {
        return 2; // Queen pawn category.
    }
}
