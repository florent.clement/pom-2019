package fr.univ_lyon1.info.m1.pom2019.strategy;

import fr.univ_lyon1.info.m1.pom2019.evaluation.LearningEvaluation;
import fr.univ_lyon1.info.m1.pom2019.evaluation.NaiveEvaluation;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.LearningPlayer;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.vote.NaiveVote;

public class ExtendedLearningStrategy
        extends GroupLimitStrategy<LearningEvaluation, NaiveVote> {

    /**
     * Extended learning strategy default constructor.
     * @param abstractPawn the abstract pawn
     * @param player       the player
     * @param opponent     the opponent
     */
    public ExtendedLearningStrategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        super(abstractPawn, player, opponent);
    }

    @Override
    protected LearningEvaluation createEvaluate() {
        if (this.getPlayer() instanceof LearningPlayer) {
            LearningPlayer player = (LearningPlayer) this.getPlayer();
            return new LearningEvaluation(new NaiveEvaluation(),
                    player.getMemory(), player.getStrategyId());
        } else {
            return new LearningEvaluation(new NaiveEvaluation(), null, 0);
        }
    }
}
