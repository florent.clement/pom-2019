package fr.univ_lyon1.info.m1.pom2019.learning;

import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

public class LocationBuilderTest {

    @Test
    public void buildFromString() {
        // Given
        String test = "[\n" +
                        "strategyId:0;\n" +
                        "originCoord:(1,2);\n" +
                        "originId:-1;\n" +
                        "grade:1;\n" +
                        "surroundings:{\n" +
                        "(2,1)=-1" +
                        "\n&\n" +
                        "(1,2)=-2" +
                        "\n}\n" +
                        "]\n";

        // When
        Location location = LocationBuilder.buildFromString(test);

        // Then
        Assert.assertNotNull(location);
        Assert.assertFalse(location.getSurroundings().isEmpty());
        Assert.assertEquals(test, location.toString());
    }

    @Test
    public void nullityTests() {
        // Nullity Tests
        String test = "[\n" +
                "originCoord:(1,2);\n" +
                "originId:1;\n" +
                "grade:-1;\n" +
                "location:{\n" +
                "(2,1)=-1" +
                "\n&\n" +
                "(1,2)=-2" +
                "\n}\n" +
                "]\n";
        Assert.assertNull(LocationBuilder.buildFromString(test));

        test = "[\n" +
                "originCoord:(1,2);\n" +
                "originId:a;\n" +
                "grade:1;\n" +
                "location:{\n" +
                "(2,1)=-1" +
                "\n&\n" +
                "(1,2)=-2" +
                "\n}\n" +
                "]\n";
        //Assert.assertNull(LocationBuilder.buildFromString(test));

        test = "[\n" +
                "originCoord:a;\n" +
                "originId:-1;\n" +
                "grade:1;\n" +
                "location:{\n" +
                "(2,1)=-1" +
                "\n&\n" +
                "(1,2)=-2" +
                "\n}\n" +
                "]\n";
        //Assert.assertNull(LocationBuilder.buildFromString(test));

        test = "[\n" +
                "originCoord:(1,2);\n" +
                "originId:-1;\n" +
                "grade:1;\n" +
                "location:{}\n" +
                "]\n";
        Assert.assertNull(LocationBuilder.buildFromString(test));

        test = "[\n" +
                "originCoord:(1,2);\n" +
                "originId:-1;\n" +
                "grade:1;\n" +
                "]\n";
        Assert.assertNull(LocationBuilder.buildFromString(test));

        test = "[\n" +
                "originId:1;\n" +
                "grade:-1;\n" +
                "location:{\n" +
                "(2,1)=-1" +
                "\n&\n" +
                "(1,2)=-2" +
                "\n}\n" +
                "]\n";
        Assert.assertNull(LocationBuilder.buildFromString(test));

        test = "[\n" +
                "originCoord:(1,2);\n" +
                "grade:1;\n" +
                "location:{\n" +
                "(2,1)=-1" +
                "\n&\n" +
                "(1,2)=-2" +
                "\n}\n" +
                "]\n";
        Assert.assertNull(LocationBuilder.buildFromString(test));

        test = "[\n" +
                "originCoord:(1,2);\n" +
                "originId:-1;\n" +
                "location:{\n" +
                "(2,1)=-1" +
                "\n&\n" +
                "(1,2)=-2" +
                "\n}\n" +
                "]\n";
        Assert.assertNull(LocationBuilder.buildFromString(test));

        test = "\n" +
                "originCoord:(1,2);\n" +
                "originId:-1;\n" +
                "grade:1;\n" +
                "location:{\n" +
                "(2,1)=-1" +
                "\n&\n" +
                "(1,2)=-2" +
                "\n}\n" +
                "\n";
        Assert.assertNull(LocationBuilder.buildFromString(test));

        test = "\n" +
                "originCoord:(1,2);\n" +
                "originId:-1;\n" +
                "grade:1;\n" +
                "location:\n" +
                "(2,1)=-1" +
                "\n&\n" +
                "(1,2)=-2" +
                "\n\n" +
                "\n";
        Assert.assertNull(LocationBuilder.buildFromString(test));

        test = "\n" +
                "originCoord:(1,2),\n" +
                "originId:-1,\n" +
                "grade:1,\n" +
                "location:{\n" +
                "(2,1)=-1" +
                "\n&\n" +
                "(1,2)=-2" +
                "\n}\n" +
                "\n";
        Assert.assertNull(LocationBuilder.buildFromString(test));

        test = "\n" +
                "originCoord=(1,2);\n" +
                "originId=-1;\n" +
                "grade=1;\n" +
                "location={\n" +
                "(2,1):-1" +
                "\n&\n" +
                "(1,2):-2" +
                "\n}\n" +
                "\n";
        Assert.assertNull(LocationBuilder.buildFromString(test));
    }

    @Test
    public void recreate() {
        // Given
        Random random = new Random();

        for (int type = 1; type <= 2; ++type) {
            for (int color = -1; color <= 1; color += 2) {
                for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                    for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                        if((x + y) % 2 == 1) {
                            continue;
                        }
                        int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                        board[x][y] = type * color;

                        for (int locX = x - Constants.LOCATION_RADIUS;
                             locX <= x + Constants.LOCATION_RADIUS; ++locX) {
                            for (int locY = y - Constants.LOCATION_RADIUS;
                                 locY <= y + Constants.LOCATION_RADIUS; ++locY) {
                                if((locX + locY) % 2 == 1 || locX < 0 || locY < 0
                                        || locX >= Constants.CHECKERS_SIZE
                                        || locY>= Constants.CHECKERS_SIZE
                                        || locX == x || locY == y) {
                                    continue;
                                }
                                if (random.nextInt(100) % 2 == 0) {
                                    int randType = random.nextInt(100) % 2 == 0 ? 1 : 2;
                                    int randColor = random.nextInt(100) % 2 == 0 ? -1 : 1;
                                    board[locX][locY] = randType * randColor;
                                }
                            }
                        }

                        Location location = new Location(0, new Coord(x, y), type * color, 1);
                        location.generateSurroundings(board);

                        // When
                        Location test = LocationBuilder.buildFromString(location.toString());

                        // Then
                        Assert.assertNotNull(test);
                        Assert.assertEquals(location, test);
                    }
                }
            }
        }
    }
}