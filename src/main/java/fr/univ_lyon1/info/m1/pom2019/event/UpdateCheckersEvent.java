package fr.univ_lyon1.info.m1.pom2019.event;

import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

/**
 * Class UpdateCheckersEvent.
 */
public class UpdateCheckersEvent<G extends Group, V extends Vote> extends Event<G, V> {


    private final int[][] currentCheckers;
    private final int[][] newCheckers;

    /**
     * Checker Update Event Builder.
     *
     * @param src             Event source
     * @param currentCheckers current currentCheckers
     * @param newCheckers     New checker
     */
    public UpdateCheckersEvent(Listeners<G, V> src, int[][] currentCheckers, int[][] newCheckers) {
        super(src);
        this.currentCheckers = currentCheckers;
        this.newCheckers = newCheckers;
    }

    @Override
    public Type typeEvent() {
        return Type.UPDATE_CHECKERS;
    }

    @Override
    public int[][] getCurrentCheckers() {
        return currentCheckers;
    }

    /**
     * Get new checkers int [ ] [ ].
     *
     * @return the int [ ] [ ]
     */
    public int[][] getNewCheckers() {
        return newCheckers;
    }
}