package fr.univ_lyon1.info.m1.pom2019.evaluation;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;

import java.util.List;

public class NaiveEvaluation implements Evaluate {

    @Override
    public Integer evaluate(int[][] board, AbstractPawn abstractPawn,
                            List<AbstractPawn> owner, List<AbstractPawn> opponent) {

        int res = 0;
        List<Action> actions = abstractPawn.getDplPossible(board);

        // If pawn is in danger in current position and can move add 5 points to evaluation.
        int currentRisk = Evaluators.evaluateImmediateCapture(board,
                abstractPawn.getPosition().getX(), abstractPawn.getPosition().getY(),
                abstractPawn.getId());
        res += actions.size() > 0 && currentRisk > 0 ? 5 : 0;

        for (Action action : actions) {

            Coord lastMove = action.getLastMovements();

            boolean white = action.getPiece().getId() > 0;

            if ((white && lastMove.getY() == Constants.CHECKERS_SIZE - 1)
                    || (!white && lastMove.getY() == 0)) {
                System.out.println("Evaluating action at end of board: " + lastMove);
            }

            int riskAtLastMove = Evaluators.evaluateImmediateCapture(board,
                    lastMove.getX(), lastMove.getY(), abstractPawn.getId());

            int captures = action.getCapturedPieces().size();

            // Balance points between capturing and risk being captured.
            res += captures - riskAtLastMove;

            // If pawn can convert and is at risk then no points added.
            res += action.isConvert() ? 5 : 0;
        }

        return res;
    }
}
