package fr.univ_lyon1.info.m1.pom2019.model.player;

import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.strategy.AbstractStrategy;
import fr.univ_lyon1.info.m1.pom2019.strategy.ExtendedLearningStrategy;

public class ExtendedLearningPlayer extends LearningPlayer {

    public ExtendedLearningPlayer(int[][] checkers, boolean whiteColor, Player opponent) {
        super(checkers, whiteColor, opponent);
    }

    @Override
    public String getNameIa() {
        return "Naive player using extended learning strategy";
    }

    @Override
    public AbstractStrategy<?, ?> getStrategy(
            AbstractPawn abstractPawn,
            Player player,
            Player opponent) {
        return new ExtendedLearningStrategy(
                abstractPawn,
                player,
                opponent
        );
    }

    @Override
    public int getStrategyId() {
        return 1;
    }
}
