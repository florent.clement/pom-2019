package fr.univ_lyon1.info.m1.pom2019.model.pawn;

import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.logger.LoggerApp;
import org.reflections.Reflections;
import org.reflections.ReflectionsException;

import java.util.Set;

/**
 * Constructeur de pion.
 */
public class PionBuilder {


    /**
     * Retourne un pion en fonction de l'id.
     * @param id Id du pion à obtenir
     * @return Retourne un pion ou null si l'id n'existe pas
     */
    public static AbstractPawn build(int id, Coord coord) {

        Reflections reflections = new Reflections(
                "fr.univ_lyon1.info.m1.pom2019.model.pawn");

        Set<Class<? extends AbstractPawn>> lBuilder;
        try {
            lBuilder = reflections.getSubTypesOf(AbstractPawn.class);
        } catch (ReflectionsException r) {
            return null; // Aucun pion trouvé
        }

        for (Class<? extends AbstractPawn> builder : lBuilder) {
            try {
                AbstractPawn abstractPawn = builder.getConstructor(Coord.class,int.class)
                        .newInstance(coord, id);

                if (abstractPawn.typePawnId() == Math.abs(id)) {
                    return abstractPawn;
                }
            } catch (Exception e) {
                e.printStackTrace();
                LoggerApp.error(e.getMessage());
            }
        }

        return null;
    }
}
