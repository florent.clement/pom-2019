## [Version 4.0](https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/14)

### Ajouts

* Ajout de la stratégie Learning basé sur l'évaluation de votes
* Ajout de la stratégie Learning basé sur le filtrage de coups
* Ajout de Player correspondants
* Ajout de l'évaluateur de situation en mémoire
* Ajout de la classe Location pour modéliser les situations en mémoire
* Ajout du système de persistance et chargement des situations en mémoire
* Ajout de classe abstraite ```Learning Player``` pour plus de cohérence


### Modifications

* Fix de l'activation/désactivation de groupe pour ne lancer les votes 
que sur un groupe ( ajout des ``events`` associés )
* Fix du filtrage de coups de NaiveVote pour permettre les conversions de pions sans capture
* Fix de problème de conversion à cause de duplications d'actions lors de la recherche récursive
* Modification du temps de sleep pour dynamiser les temps d'attente en fonction du temps de calcul

## [Version 3.0](https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/13)

### Ajouts

* Ajout de la stratégie MinMax
* Ajout de l'évaluateur en profondeur
* Ajout d'un activation/désactivation de groupe pour ne lancer les votes 
que sur un groupe ( ajout des ``events`` associés )


### Modifications

* En tête des classes de stratégies ( template )
* Rajout de classe abstraite ```AbstractVoteStrategy``` et 
```GroupManagerStrategy``` pour plus de cohérence

## [Version 2.3](https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/12)

### Ajouts

* Stratégie par profil de pion

### Modifications

* Possibilités de récupérer la liste des pions de l'adversaire à 
l'intérieur des stratégies


## [Version 2.2](https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/11)


### Ajouts

* GroupLimitSize ( groupe limité en nombre de pion )
* GroupLimitStrategy stratégie qui a pour objectif de faire des petits groupes mais en maximisant l'utlité de chaque membre
* Profile ( début d'implémentation d'un système de profil )
* GroupEvaluationStrategy qui a pour but de retirer les pions inutiles avant la création de groupe
* PointEvaluation qui évalue un pion selon plusieurs critères dont nous pouvons faire varier le coef

### Modifications

 * Utilisation de la class ```Convertable``` qui ne l'était pas jusqu'à present
 * Code Review important avec refractoring de code pour éviter la duplication de code 
 * Correction création de thread en boucle ( amélioration notable des perfs )

### Suppresion

* ConvertPawn n'était finalement pas utile au vue de la modélisation actuelle

## [Version 2.1](https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/10)

### Ajouts

* Stratégie avec vote de coup
* Evaluation de coup
* Conversion de pion
* Filtre règle


## [Version 2.0](https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/9)

### Ajouts

* Ajout Dame
* Résolution naïve en différentes steps
* Un pion par thread


### Modications

* Passage **centralisé** à **distribué**

## [Version 1.0](https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/5)

### Ajouts

* Ajout ConsoleApp
* Ajout Tools pour les fonctions globales

### Modifications

* Suppression du damier dans les Pions
* recurSearch corrigé
* GroupBuilder optimisé ( implémentation de notre propre algorithme )
* Mises à jour des tests unitaires

## (Version 0.5)[https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/6]

### Ajouts

* Première version Stratégie Naive


### Modifications

* Déplacement du Logger à droite du damier
* Correction thread à la fermeture de l'application
* Correction X Y dans le damier
* Correction prise de pion : fix thread


## [Version 0.4](https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/4)

### Ajouts

* Sélection du type de joueur à faire affronter
* Suppression de pièce sur le damier
* Exécution d'une ia en fonction de sa stratégie
* Génération de groupe


### Modifications

* correction radius à l'ouverture
* Ajout d'une seconde class abstraite pour les joueurs : 
[iaPlayer](src/main/java/fr/univ_lyon1/info/m1/pom2019/model/player/IaPlayer.java)
* Taille de fenêtre


## [Version 0.3](https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/3)

### Ajouts

* Première version des 
[Joueurs](src/main/java/fr/univ_lyon1/info/m1/pom2019/model/player/Player.java)
 ( sans décision )  
* Choix de la génération de partie 
[Une seul pièce](src/main/java/fr/univ_lyon1/info/m1/pom2019/model/checkers/OnePieceBuilder.java)
ou 
[format classique](src/main/java/fr/univ_lyon1/info/m1/pom2019/model/checkers/ClassicBuilder.java)
* Ajout des pièces de manière transparante dans la listPion du joueur
* Ajout des contraintes de déplacement
* Récupération de la liste des coups possible

### Modifications

* Ajout des PionView avec cast dynamique pour prendre en compte les nouveaux type de pion

## [Version 0.2](https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/2)

### Ajouts

* Tests unitaires interface ( non testé sur le runner )
 ( a7238d72dfed6fa7ba2448d3f70fa45358297cbd / 
  32a3893f76c698d9a8a19ae198de29b84145bb0b)
* Tests unitaires [CaseView](src/test/fr/univ_lyon1/info/m1/pom2019/view/CaseViewTest.java)
: 5d09951e3509bb2a1ce1b1acf7f6ed45e49176cd

### Modifications

* Changement chargement contrôleur FXML: 35cfa2d6913ba7d07aa416345536a19b807c12ac

## [Version 0.1](https://forge.univ-lyon1.fr/p1601511/pom-2019/-/milestones/1)

### Ajouts

- gitignore / gitlab-ci.xml 29670ddaf07a41e7bd4a9809c5c7f70a291a89e7
- Mise en place JavaFx 6d2afbfc3dcdf9a41d731ce40032627adfd09c75
- Logger ( version basique ) 637f5d030eef72bedb341a6d38a977bca88db0fd
- Application responsive 59c745942313919fff6562f7a3fd0552828d2476
- Déplacement pièce d'un point **A** à un point **B** 9077502903700f4c49046c7098f2061acdba04e8
