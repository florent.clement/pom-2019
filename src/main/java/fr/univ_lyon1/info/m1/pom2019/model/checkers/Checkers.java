package fr.univ_lyon1.info.m1.pom2019.model.checkers;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.app.CheckersApp;
import fr.univ_lyon1.info.m1.pom2019.logger.LoggerApp;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.LearningPlayer;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Classe modele pour representer le damier.
 */
public class Checkers extends Service<List<Action>> {
    private Player player1;
    private Player player2;
    private Player current;

    private int[][] checkers;

    public Checkers() {
    }

    @Override
    protected Task<List<Action>> createTask() {
        return new Task<List<Action>>() {
            @Override
            protected List<Action> call() {

                try {

                    if (player1 instanceof LearningPlayer) {
                        LearningPlayer player = (LearningPlayer)player1;
                        initLearningPlayer(player);
                    }

                    if (player2 instanceof LearningPlayer) {
                        LearningPlayer player = (LearningPlayer)player2;
                        initLearningPlayer(player);
                    }

                    long time = 0;

                    while (nextPlayer()) {
                        try {
                            if (time != 0) {
                                Thread.sleep(time);
                            }
                        } catch (InterruptedException ex) {
                            return new ArrayList<>();
                        }
                        long start = System.currentTimeMillis();
                        List<Action> actionList = current.doAction();
                        long end = System.currentTimeMillis();

                        time = Math.max(0, 600 - end + start);

                        for (Action action : actionList) {

                            // We remove the pawns
                            if (action.getCapturedPieces().size() > 0) {
                                opponent(current).removePawn(action.getCapturedPieces());
                            }

                            Coord arr = action.getMovements().get(action.getMovements().size() - 1);
                            Coord dep = action.getStartingPos();

                            action.getPiece().setPosition(arr);

                            updateCheckers(dep, 0);
                            updateCheckers(arr, action.getPiece().getId());
                            updateCheckers(action.getCapturedPieces());

                            if (action.isConvert()) {
                                System.out.println("Converting at: " + action.getLastMovements());
                                AbstractPawn abstractPawn = current.convert(action);
                                if (abstractPawn != null) {
                                    updateCheckers(arr, abstractPawn.getId());
                                }
                            }
                        }
                        updateValue(actionList);
                    }
                } catch (ExecutionException | InterruptedException e) {

                    e.printStackTrace();

                    Platform.runLater(() -> LoggerApp.error(e.getMessage()));

                    return new ArrayList<>();
                }

                String message = "Le joueur "
                        + (player1.equals(current) ? "2" : "1")
                        + " gagne la partie";

                if (player1 instanceof LearningPlayer) {
                    persistMemory((LearningPlayer) player1);
                }

                if (player2 instanceof LearningPlayer) {
                    persistMemory((LearningPlayer) player2);
                }

                Platform.runLater(() -> LoggerApp.success(message));

                return new ArrayList<>();
            }

            @Override
            protected void succeeded() {
                reset();
            }

            @Override
            protected void cancelled() {
                reset();
            }

            @Override
            protected void failed() {
                reset();
            }

            @Override
            protected void updateValue(List<Action> value) {
                
                if (value.size() != 0) {

                    Platform.runLater(() -> {
                        for (Action action : value) {

                            // We remove the pawns
                            for (Coord coord : action.getCapturedPieces()) {
                                CheckersApp.removePiece(coord.getX(),coord.getY());
                            }

                            Coord arr = action.getMovements().get(action.getMovements().size() - 1);
                            Coord dep = action.getStartingPos();
                            boolean transform = action.isConvert();

                            CheckersApp.echange(dep.getX(), dep.getY(), arr.getX(), arr.getY(),
                                    action.getPiece().getId() > 0 ? Color.BLUE : Color.RED
                            );

                            if (transform) {
                                CheckersApp.changeToDame(arr.getX(),
                                        arr.getY(), action.getPiece().getId());
                            }
                        }
                    });
                }
            }
        };
    }

    /**
     * Initialisation method for checkers game.
     * @param abstractMatrixBuilder matrix builder for creating game board.
     */
    public void init(AbstractMatrixBuilder abstractMatrixBuilder) {
        this.checkers = abstractMatrixBuilder.build();
    }

    /**
     * Change de joueur.
     * @return renvoie VRAI s'il peut jouer FAUX sinon.
     */
    private boolean nextPlayer() {
        if (current != null) {
            this.current = current == player1 ? player2 : player1;
        } else {
            this.current = player1;
        }

        return this.current.canPlay();
    }

    private Player opponent(Player player) {
        return player == player1 ? player2 : player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public int[][] getCheckers() {
        return checkers;
    }

    private void updateCheckers(Coord coord, int id) {
        this.checkers[coord.getX()][coord.getY()] = id;
    }

    private void updateCheckers(List<Coord> lCoord) {
        for (Coord coord : lCoord) {
            updateCheckers(coord, 0);
        }
    }

    private void initLearningPlayer(LearningPlayer player) {
        if (player != null) {
            player.initMemory();
            player.setPersistence(true);
        }
    }

    private void persistMemory(LearningPlayer player) {
        if (player != null) {
            player.saveMemory();
        }
    }
}
