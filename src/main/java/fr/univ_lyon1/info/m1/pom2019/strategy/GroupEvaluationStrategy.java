package fr.univ_lyon1.info.m1.pom2019.strategy;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.event.Listeners;
import fr.univ_lyon1.info.m1.pom2019.event.PossibleActionsEvent;
import fr.univ_lyon1.info.m1.pom2019.event.ProtectorsEvent;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.globals.Tools;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.IaPlayer;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.thread.StrategyListenersRunner;
import fr.univ_lyon1.info.m1.pom2019.vote.NaiveVote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;


/**
 * Reverse filtering Naive strategy.
 * First filter actions by pawn.
 * Then make groups.
 * Then vote for action.
 */
public class GroupEvaluationStrategy<G extends Group> extends Naive2Strategy<G> {

    /**
     * Protector pawn list by coordinate.
     */
    protected Set<Coord> protectors = new HashSet<>();

    /**
     * GroupEvaluationStrategy default constructor.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     * @param opponent     the opponent
     */
    public GroupEvaluationStrategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        super(abstractPawn, player, opponent);

        this.memoryListAction = new HashMap<>();
    }

    @Override
    public void preStepFunctions(List<BiConsumer<StrategyListenersRunner<G, NaiveVote>,
            List<Listeners<G, NaiveVote>>>> biFunctions) {
        biFunctions.add(this::movementAnnouncementsStep);
        biFunctions.add(this::filterProtectors);
        biFunctions.add(this::filterMeaningless);
        biFunctions.add(this::groupJoinStep);
        biFunctions.add(this::existingGroupAnnouncementsStep);
        biFunctions.add(this::choiceGroupStep);
        biFunctions.add(this::naiveVoteStep);
        biFunctions.add(this::naive2MoveApplicationStep);
    }

    @Override
    protected void movementAnnouncementsStep(
            StrategyListenersRunner<G, NaiveVote> src,
            List<Listeners<G, NaiveVote>> list) {

        IaPlayer iaPlayer = this.getIaPlayer();
        int[][] checkers = Tools.cloneCheckers(iaPlayer.getCheckers());

        List<Action> actionList = this.getPawn().getDplPossible(checkers);

        PossibleActionsEvent<G, NaiveVote> possibleActionsEvent
                = new PossibleActionsEvent<>(this, checkers, actionList);

        src.sendEventAllListeners(possibleActionsEvent, list);

        Set<Coord> protectorsList = this.getPawn().getProtectors(checkers);
        protectors.addAll(protectorsList);
        src.sendEventAllListeners(
                new ProtectorsEvent<>(this, protectorsList, checkers), list);

        this.memoryListAction = new HashMap<>();
        this.memoryListAction.put(this, actionList);
        for (Listeners<G, NaiveVote> lst : list) {
            this.memoryListAction.put(lst, new ArrayList<>());
        }
    }

    /**
     * Method to filter protectors from memoryListAction.
     *
     * @param src  StrategyListenersRunner
     * @param list List of other listeners
     */
    protected void filterProtectors(
            StrategyListenersRunner<G, NaiveVote> src,
            List<Listeners<G, NaiveVote>> list) {

        List<Listeners<G, NaiveVote>> useless = this.memoryListAction.entrySet().stream()
                .filter(entry ->
                        this.protectors.contains(entry.getKey().getPawn().getPosition())
                                && entry.getValue().stream().noneMatch(action -> action.isConvert()
                                || action.getCapturedPieces().size() > 2))
                .map(Map.Entry::getKey).collect(Collectors.toList());

        for (Listeners<G, NaiveVote> listeners : useless) {
            this.memoryListAction.get(listeners).clear();
        }
    }

    /**
     * Method to filter meaningless actions from memoryListAction.
     *
     * @param src  StrategyListenersRunner
     * @param list List of other listeners
     */
    protected void filterMeaningless(
            StrategyListenersRunner<G, NaiveVote> src,
            List<Listeners<G, NaiveVote>> list) {

        List<Listeners<G, NaiveVote>> meaningless = this.memoryListAction.entrySet().stream()
                .filter(entry ->
                        entry.getValue().stream().allMatch(action ->
                                action.getCapturedPieces().isEmpty()
                                        && !action.isConvert()))
                .map(Map.Entry::getKey).collect(Collectors.toList());

        if (this.memoryListAction.size() - meaningless.size() > Constants.GROUP_SIZE) {
            for (Listeners<G, NaiveVote> listeners : meaningless) {
                this.memoryListAction.get(listeners).clear();
            }
        }
    }

    @Override
    public void setProtectorsEvent(ProtectorsEvent<G, NaiveVote> protectorsEvent,
                                   StrategyListenersRunner<G, NaiveVote> strategyListenersRunner,
                                   List<Listeners<G, NaiveVote>> list) {
        this.protectors.addAll(protectorsEvent.getProtectors());
    }
}
