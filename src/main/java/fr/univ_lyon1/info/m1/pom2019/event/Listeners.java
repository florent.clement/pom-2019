package fr.univ_lyon1.info.m1.pom2019.event;


import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.IaPlayer;
import fr.univ_lyon1.info.m1.pom2019.thread.StrategyListenersRunner;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

import java.util.List;
import java.util.function.BiConsumer;

/**
 * Class Listeners.
 */
public interface Listeners<G extends Group, V extends Vote> {

    void preHandle(StrategyListenersRunner<G, V> src, List<Listeners<G, V>> list);

    void moveEvent(MoveEvent<G, V> event,
                   StrategyListenersRunner<G, V> src, List<Listeners<G, V>> list);

    void possibleActionsEvent(
            PossibleActionsEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list
    );

    void blockEvent(
            BlockEvent<G, V> event,
            StrategyListenersRunner<G, V> strategyListenersRunner,
            List<Listeners<G, V>> list
    );

    void updateCheckersEvent(
            UpdateCheckersEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list
    );

    void requestJoinGroupEvent(
            RequestJoinGroupEvent<G, V> event,
            StrategyListenersRunner<G, V> strategyListenersRunner,
            List<Listeners<G, V>> list);

    void requestLeaveGroupEvent(
            RequestLeaveGroupEvent<G, V> event,
            StrategyListenersRunner<G, V> strategyListenersRunner,
            List<Listeners<G, V>> list);

    void existingGroupEvent(
            ExistingGroupEvent<G, V> event,
            StrategyListenersRunner<G, V> strategyListenersRunner,
            List<Listeners<G, V>> list);

    void voteEvent(
            VoteEvent<G, V> event,
            StrategyListenersRunner<G, V> strategyListenersRunner,
            List<Listeners<G, V>> list);

    void setProtectorsEvent(ProtectorsEvent<G, V> event,
                            StrategyListenersRunner<G, V> strategyListenersRunner,
                            List<Listeners<G, V>> list);

    void authorizeVoteEvent(AuthorizeVoteEvent<G, V> event,
                            StrategyListenersRunner<G, V> strategyListenersRunner,
                            List<Listeners<G, V>> list);

    void learningFlagEvent(LearningFlagEvent<G, V> event,
                           StrategyListenersRunner<G, V> strategyListenersRunner,
                           List<Listeners<G, V>> list);

    AbstractPawn getPawn();

    IaPlayer getIaPlayer();

    int[][] getCheckers();

    void preStepFunctions(List<BiConsumer<StrategyListenersRunner<G, V>,
            List<Listeners<G, V>>>> list);

    boolean finishedSending();

    void incrementsSending();

    void decrementsSending();
}