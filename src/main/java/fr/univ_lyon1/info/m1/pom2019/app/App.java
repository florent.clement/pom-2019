package fr.univ_lyon1.info.m1.pom2019.app;

public class App {

    /**
     * Main function.
     *
     * @param args Args
     */
    public static void main(String[] args) {
        SceneApp.run(args);
    }
}
