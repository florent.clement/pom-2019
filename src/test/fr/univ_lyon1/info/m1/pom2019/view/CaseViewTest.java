package fr.univ_lyon1.info.m1.pom2019.view;

import javafx.scene.paint.Color;
import org.junit.Assert;
import org.junit.Test;

public class CaseViewTest {


    @Test
    public void getColor() {
        CaseView caseView1 = new CaseView(0,0);
        CaseView caseView2 = new CaseView(1,0);

        Assert.assertEquals(caseView1.getColor(), Color.LIGHTGRAY);
        Assert.assertEquals(caseView2.getColor(), Color.WHITE);
    }

    @Test
    public void getX() {
        CaseView caseView = new CaseView(3,0);

        Assert.assertEquals(caseView.getX(), 3);
    }

    @Test
    public void getY() {
        CaseView caseView = new CaseView(0,4);

        Assert.assertEquals(caseView.getY(), 4);
    }

    @Test
    public void removePieceView() {
        CaseView caseView = new CaseView(0,0);

        caseView.setPieceView(new PawnView(0, caseView));
        Assert.assertTrue(caseView.containsAPiece());

        caseView.removePieceView();
        Assert.assertFalse(caseView.containsAPiece());
    }

    @Test
    public void containsAPiece() {
        CaseView caseView = new CaseView(0,0);
        Assert.assertFalse(caseView.containsAPiece());

        caseView.setPieceView(new PawnView(0, caseView));
        Assert.assertTrue(caseView.containsAPiece());
    }
}