package fr.univ_lyon1.info.m1.pom2019.model.group;

import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;

import java.util.List;


/**
 * The type Group.
 */
public abstract class FilterGroup extends Group {

    private final List<AbstractPawn> ownerPawnList;
    private final List<AbstractPawn> opponentPawnList;


    /**
     * Instantiates a new Filter group.
     *
     * @param ownerPawnList    the owner pawn list
     * @param opponentPawnList the opponent pawn list
     */
    protected FilterGroup(List<AbstractPawn> ownerPawnList,
                          List<AbstractPawn> opponentPawnList) {
        this.ownerPawnList = ownerPawnList;
        this.opponentPawnList = opponentPawnList;
    }

    /**
     * Gets opponent pawn list.
     *
     * @return the opponent pawn list
     */
    public List<AbstractPawn> getOpponentPawnList() {
        return opponentPawnList;
    }

    /**
     * Gets owner pawn list.
     *
     * @return the owner pawn list
     */
    public List<AbstractPawn> getOwnerPawnList() {
        return ownerPawnList;
    }
}
