package fr.univ_lyon1.info.m1.pom2019.learning;

import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.action.CoordBuilder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * Builder class for Location.
 */
public class LocationBuilder {

    /**
     * Method for building Location from String.
     * @param str Input string to build Location.
     * @return Location if build successful, null otherwise.
     */
    public static Location buildFromString(String str) {
        Location location = null;
        Coord originCoord = null;
        int strategyId = -1;
        int originId = -1;
        int grade = -1;
        Map<Coord, Integer> map = new LinkedHashMap<>();

        if (str != null && !str.isEmpty()) {

            String clean = str.replace("\n", "").trim();
            if (!clean.isEmpty() && clean.startsWith("[") && clean.endsWith("]")) {

                String noBrackets =  clean.replace("[", "")
                        .replace("]", "");
                if (!noBrackets.isEmpty()) {

                    String[] split = noBrackets.split(";", 0);
                    List<String> arguments = Arrays.asList(split);
                    if (arguments.size() == 5) {

                        if (arguments.get(0).startsWith("strategyId")) {

                            String[] sCoord = arguments.get(0).split(":", 0);
                            List<String> strategyList = Arrays.asList(sCoord);
                            if (strategyList.size() == 2) {

                                try {
                                    strategyId = Integer.parseInt(strategyList.get(1));
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                    return null;
                                }
                            }
                        }

                        if (arguments.get(1).startsWith("originCoord")) {

                            String[] sCoord = arguments.get(1).split(":", 0);
                            List<String> coordList = Arrays.asList(sCoord);
                            if (coordList.size() == 2) {

                                originCoord = CoordBuilder.buildFromString(coordList.get(1));
                            }
                        }

                        if (arguments.get(2).startsWith("originId")) {

                            String[] sId = arguments.get(2).split(":", 0);
                            List<String> idList = Arrays.asList(sId);
                            if (idList.size() == 2) {

                                try {
                                    originId = Integer.parseInt(idList.get(1));
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                    return null;
                                }
                            }
                        }

                        if (arguments.get(3).startsWith("grade")) {

                            String[] sGrade = arguments.get(3).split(":", 0);
                            List<String> gradeList = Arrays.asList(sGrade);
                            if (gradeList.size() == 2) {

                                try {
                                    grade = Integer.parseInt(gradeList.get(1));
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                    return null;
                                }
                            }
                        }

                        if (arguments.get(4).startsWith("surroundings")) {

                            String[] sMap = arguments.get(4).split(":", 0);
                            List<String> mapList = Arrays.asList(sMap);
                            if (mapList.size() == 2) {

                                if (mapList.get(1).startsWith("{")
                                        && mapList.get(1).endsWith("}")) {

                                    String noAccolades = mapList.get(1).replace("{", "")
                                            .replace("}", "");
                                    if (!noAccolades.isEmpty()) {

                                        String[] sEntries = noAccolades.split("&", 0);
                                        List<String> entriesList = Arrays.asList(sEntries);
                                        if (!entriesList.isEmpty()) {

                                            for (String string : entriesList) {

                                                String[] sEntry = string.split("=", 0);
                                                List<String> entry = Arrays.asList(sEntry);
                                                if (entry.size() == 2) {

                                                    Coord key = CoordBuilder
                                                            .buildFromString(entry.get(0));
                                                    int value = 404;

                                                    try {
                                                        value = Integer.parseInt(entry.get(1));
                                                    } catch (NumberFormatException e) {
                                                        e.printStackTrace();
                                                        return null;
                                                    }

                                                    boolean checkValue = value == 1 || value == 2
                                                            || value == -1 || value == -2;

                                                    if (key != null && checkValue) {
                                                        map.put(key, value);
                                                    }
                                                }
                                            }

                                            if (entriesList.size() != map.size()) {
                                                map.clear();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        boolean checkStrategy = strategyId >= 0;
        boolean checkCoord = originCoord != null;
        boolean checkId = originId == 1 || originId == 2 || originId == -1 || originId == -2;
        boolean checkGrade = grade > 0;
        boolean checkMap = !map.isEmpty();

        if (checkCoord && checkId && checkGrade && checkMap) {
            location = new Location(strategyId, originCoord, originId, grade);
            location.setSurroundings(map);
        }

        return location;
    }

    /**
     * Method for building Location from String.
     * @param path Path to Location file.
     * @return Location if build successful, null otherwise.
     */
    public static Location buildFromPath(Path path) {
        Location location = null;

        if (path != null) {

            List<String> input;
            try {
                input = Files.readAllLines(path, StandardCharsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            if (!input.isEmpty()) {
                StringBuilder stringBuilder = new StringBuilder();
                for (String string : input) {
                    stringBuilder.append(string);
                }
                location = LocationBuilder
                        .buildFromString(stringBuilder.toString());
            }
        }

        return location;
    }
}
