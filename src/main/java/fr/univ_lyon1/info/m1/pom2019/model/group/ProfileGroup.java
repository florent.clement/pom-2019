package fr.univ_lyon1.info.m1.pom2019.model.group;


import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.Profile;
import fr.univ_lyon1.info.m1.pom2019.evaluation.PointEvaluation;

import scala.Tuple2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The type Group.
 */
public class ProfileGroup extends LimitGroup {

    /**
     * The Types.
     */
    Map<Profile.Type, Integer> required;

    /**
     * The Types.
     */
    Map<Profile.Type, Integer> current;

    /**
     * Instantiates a new Profile group.
     *
     * @param maxSize       the max size
     * @param evaluate      the evaluate
     * @param board         the board
     * @param limitRequest  limitRequest
     * @param types         the types
     */
    @SafeVarargs
    public ProfileGroup(List<AbstractPawn> ownerPawnList, List<AbstractPawn> opponentPawnList,
                        int maxSize, PointEvaluation evaluate, int[][] board, int limitRequest,
                        Tuple2<Profile.Type, Integer>... types) {

        super(ownerPawnList, opponentPawnList, maxSize, evaluate, board, limitRequest);
        this.required = new HashMap<>();
        for (Tuple2<Profile.Type, Integer> type : types) {
            this.required.put(type._1, type._2);
        }
        this.current = new HashMap<>();
    }

    @Override
    protected void callbackAddAbstractPawn(AbstractPawn abstractPawn) {
        Profile.Type type = abstractPawn.getProfile(this.getEvaluate(), this.getBoard(),
                this.getOwnerPawnList(), this.getOpponentPawnList());

        if (this.current.containsKey(type)) {
            this.current.put(type, this.current
                    .get(type) + 1);
        } else {
            this.current.put(type, 1);
        }
    }

    @Override
    protected void callbackRemoveAbstractPawn(AbstractPawn abstractPawn) {
        this.current.put(
                abstractPawn.getProfile(
                        this.getEvaluate(), this.getBoard(), this.getOwnerPawnList(),
                        this.getOpponentPawnList()),
                this.current.get(abstractPawn.getProfile(this.getEvaluate(), this.getBoard(),
                        this.getOwnerPawnList(), this.getOpponentPawnList())) - 1);
    }

    @Override
    protected AbstractPawn chooseAPawnToDelete() {


        for (Profile.Type type : this.required.keySet()) {

            if (this.current.containsKey(type)) {

                return this.getGroup().stream()
                        .filter(e -> e.getProfile(this.getEvaluate(), this.getBoard(),
                                this.getOwnerPawnList(), this.getOpponentPawnList()).equals(type))
                        .map(e -> new Tuple2<>(e, this.getEvaluate().evaluate(this.getBoard(), e,
                                this.getOwnerPawnList(), this.getOpponentPawnList())))
                        .sorted((t1, t2) -> -Integer.compare(t1._2, t2._2)).map(e -> e._1)
                        .findFirst().orElse(null);
            }
        }


        return super.chooseAPawnToDelete();
    }

    @Override
    public PointEvaluation getEvaluate() {
        return (PointEvaluation) super.getEvaluate();
    }
}
