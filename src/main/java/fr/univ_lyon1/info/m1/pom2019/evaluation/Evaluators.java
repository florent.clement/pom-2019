package fr.univ_lyon1.info.m1.pom2019.evaluation;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;

/**
 * The type Evaluators.
 */
public class Evaluators {

    /**
     * Evaluator method for possible immediate capture if abstractPawn does not move.
     *
     * @param board  Board to apply actions.
     * @param startX starting horizontal coordinate of pawn.
     * @param startY starting vertical coordinate of pawn.
     * @param id     the id
     * @return The number of opponent pieces capable of eating abstractPawn as points.
     */
    public static Integer evaluateImmediateCapture(int[][] board, int startX, int startY, int id) {
        boolean white = id > 0;
        int res = 0;

        for (int vecX = -1; vecX <= 1; vecX += 2) {
            for (int vecY = -1; vecY <= 1; vecY += 2) {

                if (startX - vecX < 0 || startX - vecX >= Constants.CHECKERS_SIZE
                    || startY - vecY < 0 || startY - vecY >= Constants.CHECKERS_SIZE
                    || board[startX - vecX][startY - vecY] != 0) {
                    continue;
                }

                int x = startX + vecX;
                int y = startY + vecY;
                int gap = 1;

                while (x < Constants.CHECKERS_SIZE && y < Constants.CHECKERS_SIZE
                        && x >= 0 && y >= 0 && board[x][y] == 0) {
                    x += vecX;
                    y += vecY;
                    ++gap;
                }

                boolean inBoard = x < Constants.CHECKERS_SIZE
                        && y < Constants.CHECKERS_SIZE && x >= 0 && y >= 0;

                boolean enemyPawn = inBoard
                        && ((white && board[x][y] == -1)
                            || (!white && board[x][y] == 1));

                boolean enemyQueen = inBoard
                        && ((white && board[x][y] == -2)
                            || (!white && board[x][y] == 2));

                boolean pawnCapture = enemyPawn && gap == 1;

                boolean queenCapture = enemyQueen && gap >= 1;

                if (pawnCapture || queenCapture) {
                    ++res;
                }
            }
        }

        return res;
    }
}
