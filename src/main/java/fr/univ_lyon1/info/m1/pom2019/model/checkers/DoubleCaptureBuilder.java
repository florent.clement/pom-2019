package fr.univ_lyon1.info.m1.pom2019.model.checkers;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;

/**
 * Pawn layout generator.
 */
public class DoubleCaptureBuilder extends AbstractMatrixBuilder {

    @Override
    public int[][] build() {
        int[][] result = this.initCheckers(0);

        result[Constants.CHECKERS_SIZE - 2][Constants.CHECKERS_SIZE - 2] = -1;
        result[Constants.CHECKERS_SIZE - 4][Constants.CHECKERS_SIZE - 4] = -1;
        result[Constants.CHECKERS_SIZE - 1][Constants.CHECKERS_SIZE - 1] = 1;

        return result;
    }

    @Override
    public String getName() {
        return "Taking two pawns";
    }
}
