package fr.univ_lyon1.info.m1.pom2019.model.pawn;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class PionTest {

    @Test
    public void testNoPossibilitiesUpperRow() {
        for (int color = 1; color >= -1; color -= 2) {
            for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                if ((x + 1) % 2 == 1) {
                    continue;
                }
                // Given
                int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                Pawn pion = null;
                if (color == 1) {
                    damier[x][9] = color;
                    pion = new Pawn(new Coord(x, 9), color);
                } else {
                    damier[x][0] = color;
                    pion = new Pawn(new Coord(x, 0), color);
                }

                // When
                List<Action> actions = pion.getDplPossible(damier);

                // Then
                Assert.assertTrue(actions.isEmpty());
            }
        }
    }

    @Test
    public void testOnePossibilitySideColumns() {
        for (int color = 1; color >= -1; color -= 2) {
            for (int x = 0; x < Constants.CHECKERS_SIZE; x +=9) {
                for (int y = 0; y < 9; ++y) {
                    if((color == 1 && (x + y) % 2 == 1)
                            || (color == -1 && (x + Constants.CHECKERS_SIZE - 1 - y) % 2 == 1)) {
                        continue;
                    }
                    // Given
                    int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    Pawn pion = null;
                    if (color == 1) {
                        damier[x][y] = color;
                        pion = new Pawn(new Coord(x, y), color);
                    } else {
                        damier[x][Constants.CHECKERS_SIZE - 1 - y] = color;
                        pion = new Pawn(new Coord(x,Constants.CHECKERS_SIZE - 1 - y), color);
                    }

                    // When
                    List<Action> actions = pion.getDplPossible(damier);

                    // Then
                    Assert.assertEquals(1,actions.size());
                    Assert.assertEquals(1, actions.get(0).getMovements().size());
                    Assert.assertEquals(0, actions.get(0).getCapturedPieces().size());
                    Coord coord = actions.get(0).getMovements().get(0);
                    if (x == 0) {
                        Assert.assertEquals(x + 1, coord.getX());
                    } else {
                        Assert.assertEquals(x - 1, coord.getX());
                    }
                    if (color == 1) {
                        Assert.assertEquals(y + 1, coord.getY());
                    } else {
                        Assert.assertEquals(8 - y, coord.getY());
                    }
                }
            }
        }
    }

    @Test
    public void testTwoPossibilitiesMiddleCases() {
        for (int color = 1; color >= -1; color -= 2) {
            for (int x = 1; x < 9; ++x) {
                for (int y = 0; y < 9; ++y) {
                    if((color == 1 && (x + y) % 2 == 1)
                            || (color == -1 && (x + Constants.CHECKERS_SIZE - 1 - y) % 2 == 1)) {
                        continue;
                    }
                    // Given
                    int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    damier[x][y] = color;
                    Pawn pion = null;
                    if (color == 1) {
                        damier[x][y] = color;
                        pion = new Pawn(new Coord(x, y), color);
                    } else {
                        damier[x][Constants.CHECKERS_SIZE - 1 - y] = color;
                        pion = new Pawn(new Coord(x,Constants.CHECKERS_SIZE - 1 - y), color);
                    }

                    // When
                    List<Action> actions = pion.getDplPossible(damier);

                    // Then
                    Assert.assertEquals(2,actions.size());
                    Assert.assertEquals(1, actions.get(0).getMovements().size());
                    Assert.assertEquals(0, actions.get(0).getCapturedPieces().size());
                    Assert.assertEquals(1, actions.get(1).getMovements().size());
                    Assert.assertEquals(0, actions.get(1).getCapturedPieces().size());
                    Coord coord0 = actions.get(0).getMovements().get(0);
                    Coord coord1 = actions.get(1).getMovements().get(0);
                    Assert.assertEquals(x - 1, coord0.getX());
                    Assert.assertEquals(x + 1, coord1.getX());
                    if (color == 1) {
                        Assert.assertEquals(y + 1, coord0.getY());
                        Assert.assertEquals(y + 1, coord1.getY());
                    } else {
                        Assert.assertEquals(8 - y, coord0.getY());
                        Assert.assertEquals(8 - y, coord1.getY());
                    }
                }
            }
        }
    }

    @Test
    public void testBlockedPossibilities() {
        for (int color2 = 1; color2 >= -1; color2 -= 2) {
            for (int color = 1; color >= -1; color -= 2) {
                for (int y = 0; y < 9; ++y) {
                    for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                        if((color == 1 && (x + y) % 2 == 1)
                                || (color == -1 && (x + Constants.CHECKERS_SIZE - 1 - y) % 2 == 1)) {
                            continue;
                        }
                        // Given
                        int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                        damier[x][y] = color;
                        Pawn pion = null;
                        if (color == 1) {
                            damier[x][y] = color;
                            pion = new Pawn(new Coord(x, y), color);
                            for (int xs = 0; xs < Constants.CHECKERS_SIZE; ++xs) {
                                for (int ys = y + 1; ys < Constants.CHECKERS_SIZE; ++ys) {
                                    if((xs + ys) % 2 == 1) {
                                        continue;
                                    }
                                    damier[xs][ys] = color2;
                                }
                            }
                        } else {
                            damier[x][Constants.CHECKERS_SIZE - 1 - y] = color;
                            pion = new Pawn(new Coord(x,Constants.CHECKERS_SIZE - 1 - y), color);
                            for (int xs = 0; xs < Constants.CHECKERS_SIZE; ++xs) {
                                for (int ys = 0; ys < Constants.CHECKERS_SIZE - 1 - y; ++ys) {
                                    if((xs + ys) % 2 == 1) {
                                        continue;
                                    }
                                    damier[xs][ys] = color2;
                                }
                            }
                        }

                        // When
                        List<Action> actions = pion.getDplPossible(damier);

                        // Then
                        Assert.assertTrue(actions.isEmpty());
                    }
                }
            }
        }
    }

    @Test
    public void testCantEatSameTeam() {
        for (int color = 1; color >= -1; color -= 2) {
            for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                    if((x + y) % 2 == 1) {
                        continue;
                    }
                    // Given
                    int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    damier[x][y] = color;
                    if (x - 1 >= 0) {
                        if (y - 1 >= 0) {
                            damier[x - 1][y - 1] = color;
                        }
                        if (y + 1 < Constants.CHECKERS_SIZE) {
                            damier[x - 1][y + 1] = color;
                        }
                    }
                    if (x + 1 < Constants.CHECKERS_SIZE) {
                        if (y - 1 >= 0) {
                            damier[x + 1][y - 1] = color;
                        }
                        if (y + 1 < Constants.CHECKERS_SIZE) {
                            damier[x + 1][y + 1] = color;
                        }
                    }
                    Pawn pion = new Pawn(new Coord(x, y), color);

                    // When
                    List<Action> actions = pion.getDplPossible(damier);

                    // Then
                    Assert.assertTrue(actions.isEmpty());
                }
            }
        }
    }

    @Test
    public void testEatAllOtherTeam() {
        for (int color = 1; color >= -1; color -= 2) {
            for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                    if((x + y) % 2 == 1) {
                        continue;
                    }
                    // Given
                    int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    damier[x][y] = color;
                    int color2 = color > 0 ? color - 2 : color + 2;
                    int possibilities = 0;
                    LinkedList<Coord> possibleMovements = new LinkedList<>();
                    LinkedList<Coord> possibleCaptures = new LinkedList<>();
                    if (x - 1 >= 0) {
                        if (y - 1 >= 0) {
                            damier[x - 1][y - 1] = color2;
                            if (x - 2 >= 0 && y - 2 >= 0) {
                                ++possibilities;
                                possibleMovements.add(new Coord(x - 2, y - 2));
                                possibleCaptures.add(new Coord(x - 1, y - 1));
                            }
                        }
                        if (y + 1 < Constants.CHECKERS_SIZE) {
                            damier[x - 1][y + 1] = color2;
                            if (x - 2 >= 0 && y + 2 < Constants.CHECKERS_SIZE) {
                                ++possibilities;
                                possibleMovements.add(new Coord(x - 2, y + 2));
                                possibleCaptures.add(new Coord(x - 1, y + 1));
                            }
                        }
                    }
                    if (x + 1 < Constants.CHECKERS_SIZE) {
                        if (y - 1 >= 0) {
                            damier[x + 1][y - 1] = color2;
                            if (x + 2 < Constants.CHECKERS_SIZE && y - 2 >= 0) {
                                ++possibilities;
                                possibleMovements.add(new Coord(x + 2, y - 2));
                                possibleCaptures.add(new Coord(x + 1, y - 1));
                            }
                        }
                        if (y + 1 < Constants.CHECKERS_SIZE) {
                            damier[x + 1][y + 1] = color2;
                            if (x + 2 < Constants.CHECKERS_SIZE && y + 2 < Constants.CHECKERS_SIZE) {
                                ++possibilities;
                                possibleMovements.add(new Coord(x + 2, y + 2));
                                possibleCaptures.add(new Coord(x + 1, y + 1));
                            }
                        }
                    }

                    Pawn pion = new Pawn(new Coord(x, y), color);

                    // When
                    List<Action> actions = pion.getDplPossible(damier);

                    // Then
                    Assert.assertEquals(possibilities, actions.size());
                    for (int i = 0; i < possibilities; ++i) {
                        Assert.assertEquals(1, actions.get(i).getMovements().size());
                        Assert.assertEquals(1, actions.get(i).getCapturedPieces().size());
                        Assert.assertTrue(possibleMovements.contains(actions.get(i).getMovements().get(0)));
                        Assert.assertTrue(possibleCaptures.contains(actions.get(i).getCapturedPieces().get(0)));
                    }
                }
            }
        }
    }

    @Test
    public void testChainDiagonalEating() {
        for (int color = 1; color >= -1; color -= 2) {
            for (int x = 0; x < Constants.CHECKERS_SIZE; x += 4) {
                // Given
                int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                damier[x][x] = color;
                // Block possible movement when x = 8
                damier[7][9] = color;
                damier[9][7] = color;
                int color2 = color > 0 ? color - 2 : color + 2;
                for (int a = 1; a < Constants.CHECKERS_SIZE; a += 2) {
                    damier[a][a] = color2;
                    if (Constants.CHECKERS_SIZE - 1 - a - 1 >= 0) {
                        damier[Constants.CHECKERS_SIZE - 1 - a - 1][a] = color2;
                    }

                }
                Pawn pion = new Pawn(new Coord(x, x), color);

                // When
                List<Action> actions = pion.getDplPossible(damier);

                // Then
                int expected;
                if (color == 1) {
                    expected = 8;
                } else if (x == 0) {
                    expected = 8;
                } else {
                    expected = 8;
                }
                Assert.assertEquals(expected, actions.size());
            }
        }
    }

    @Test
    public void canConvert() {
        for (int color = 1; color >= -1; color -= 2) {
            for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                for (int y = 1; y < Constants.CHECKERS_SIZE - 1; y += 7) {
                    if ((color == 1 && y == 1) || (color == -1 && y == 8)) {
                        continue;
                    }
                    // Given
                    int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    damier[x][y] = color;
                    Pawn pion = new Pawn(new Coord(x, y), color);
                    int possibilities;
                    if (x > 0 && x < Constants.CHECKERS_SIZE - 1) {
                        possibilities = 2;
                    } else {
                        possibilities = 1;
                    }

                    // When
                    List<Action> list = pion.getDplPossible(damier);

                    // Then
                    Assert.assertFalse(list.isEmpty());
                    Assert.assertEquals(possibilities, list.size());
                    int count = 0;
                    for (Action action : list) {
                        if (action.isConvert()) {
                            ++count;
                        }
                    }
                    Assert.assertEquals(possibilities, count);
                }
            }
        }
    }

    @Test
    public void cantConvert() {
        for (int color = 1; color >= -1; color -= 2) {
            for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                    if ((color == 1 && y == 8) || (color == -1 && y == 1)) {
                        continue;
                    }
                    // Given
                    int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    damier[x][y] = color;
                    Pawn pion = new Pawn(new Coord(x, y), color);
                    int possibilities;
                    if (x > 0 && x < Constants.CHECKERS_SIZE - 1) {
                        possibilities = 2;
                    } else {
                        possibilities = 1;
                    }
                    if ((color == 1 && y == Constants.CHECKERS_SIZE - 1) || (color == -1 && y == 0)) {
                        possibilities = 0;
                    }

                    // When
                    List<Action> list = pion.getDplPossible(damier);

                    // Then
                    Assert.assertEquals(possibilities, list.size());
                    int count = 0;
                    for (Action action : list) {
                        if (action.isConvert()) {
                            ++count;
                        }
                    }
                    Assert.assertEquals(0, count);
                }
            }
        }
    }

    @Test
    public void chainConvertCorrectness() {
        for (int color = 1; color >= -1; color -= 2) {
            // Given
            int x;
            int y;
            if (color == 1) {
                x = 5;
                y = 7;
            } else {
                x = 4;
                y = 2;
            }
            int color2 = color > 0 ? color - 2 : color + 2;
            int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
            damier[x][y] = color;
            damier[x + 1][y + color] = color2;
            damier[x + 3][y + color] = color2;
            Pawn pion = new Pawn(new Coord(x, y), color);

            // When
            List<Action> actions = pion.getDplPossible(damier);

            // Then
            Assert.assertFalse(actions.isEmpty());
            Assert.assertEquals(3, actions.size());
            int count = 0;
            for (Action action : actions) {
                if (action.isConvert()) {
                    ++count;
                }
            }
            Assert.assertEquals(1, count);
            Action last = actions.get(actions.size() - 1);
            Assert.assertFalse(last.isConvert());
        }
    }

    @Test
    public void notBlocked() {
        for (int color = 1; color >= -1; color -= 2) {
            for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                    // Given
                    int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    damier[x][y] = color;
                    Pawn pion = new Pawn(new Coord(x, y), color);

                    // When
                    List<Coord> coords = pion.getBlockCoord(damier);

                    // Then
                    Assert.assertTrue(coords.isEmpty());
                }
            }
        }
    }

    @Test
    public void blockedByOpposingTeam() {
        for (int color = 1; color >= -1; color -= 2) {
            int color2 = color > 0 ? color - 2 : color + 2;
            int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
            for (int a = 0; a < Constants.CHECKERS_SIZE; ++a) {
                for (int b = 0; b < Constants.CHECKERS_SIZE; ++b) {
                    damier[a][b] = color2;
                }
            }
            for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                    // Given
                    Pawn pion = new Pawn(new Coord(x, y), color);

                    // When
                    List<Coord> coords = pion.getBlockCoord(damier);

                    // Then
                    Assert.assertTrue(coords.isEmpty());
                }
            }
        }
    }

    @Test
    public void blockedBySameTeam() {
        for (int color = 1; color >= -1; color -= 2) {
            int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
            for (int a = 0; a < Constants.CHECKERS_SIZE; ++a) {
                for (int b = 0; b < Constants.CHECKERS_SIZE; ++b) {
                    damier[a][b] = color;
                }
            }
            for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                    // Given
                    Pawn pion = new Pawn(new Coord(x, y), color);

                    // When
                    List<Coord> coords = pion.getBlockCoord(damier);

                    // Then
                    int possibilities;
                    Coord coord1 = null;
                    Coord coord2 = null;
                    if (color == 1) {
                        if (y < Constants.CHECKERS_SIZE - 1) {
                            if (x == 0 || x == Constants.CHECKERS_SIZE - 1) {
                                possibilities = 1;
                                int x1 = x == 0 ? 1 : Constants.CHECKERS_SIZE - 2;
                                coord1 = new Coord(x1, y + 1);
                            } else {
                                possibilities = 2;
                                coord1 = new Coord(x - 1, y + 1);
                                coord2 = new Coord(x + 1, y + 1);
                            }
                        } else {
                            possibilities = 0;
                        }
                    } else {
                        if (y > 0) {
                            if (x == 0 || x == Constants.CHECKERS_SIZE - 1) {
                                possibilities = 1;
                                int x1 = x == 0 ? 1 : Constants.CHECKERS_SIZE - 2;
                                coord1 = new Coord(x1, y - 1);
                            } else {
                                possibilities = 2;
                                coord1 = new Coord(x - 1, y - 1);
                                coord2 = new Coord(x + 1, y - 1);
                            }
                        } else {
                            possibilities = 0;
                        }
                    }
                    Assert.assertEquals(possibilities, coords.size());
                    if (coord1 != null) {
                        Assert.assertTrue(coords.contains(coord1));
                    }
                    if (coord2 != null) {
                        Assert.assertTrue(coords.contains(coord2));
                    }
                }
            }
        }
    }
}