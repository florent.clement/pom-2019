package fr.univ_lyon1.info.m1.pom2019.thread;

import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;


public class MonitorTest {

    @Test
    public void add() {
        Monitor<Thread> monitor = new Monitor<>();
        Thread thread = new Thread();

        Assert.assertEquals(monitor.countThread(), 0);
        monitor.add(thread);
        Assert.assertEquals(monitor.countThread(), 1);
    }

    @Test
    public void addAll() {
        Monitor<Thread> monitor = new Monitor<>();
        List<Thread> list = Arrays.asList(
                new Thread(),
                new Thread()
        );

        Assert.assertEquals(monitor.countThread(), 0);
        monitor.addAll(list);
        Assert.assertEquals(monitor.countThread(), 2);
    }

    @Test
    public void remove() {
        Monitor<Thread> monitor = new Monitor<>();
        Thread thread = new Thread();
        monitor.add(thread);

        Assert.assertEquals(monitor.countThread(), 1);
        monitor.remove(new Thread());
        Assert.assertEquals(monitor.countThread(), 1);
        monitor.remove(thread);
        Assert.assertEquals(monitor.countThread(), 0);
    }

    @Test
    public void removeAll() {
        Monitor<Thread> monitor = new Monitor<>();
        List<Thread> list = Arrays.asList(
                new Thread(),
                new Thread()
        );

        monitor.addAll(list);
        Assert.assertEquals(monitor.countThread(), 2);
        monitor.removeAll(list);
        Assert.assertEquals(monitor.countThread(), 0);
    }

    @Test
    public void testNotifyWait() {

        Monitor<Runnable> monitor = new Monitor<>();

        Runnable runnable1 = new Runnable() {
            @Override
            public void run() {
                try {
                    monitor.wait(this);
                } catch (Exception e) {
                    e.printStackTrace();
                    Assert.fail();
                }
            }
        };

        Runnable runnable2 = () -> monitor.notify(runnable1, null);

        monitor.add(runnable1);
        monitor.add(runnable2);

        try {
            monitor.run();
        } catch (InterruptedException e) {
            Assert.fail();
        }
    }
}