# POM-2019

**Sujet**: Modélisation distribué de jeux stratégique: cas du damiers

**Responsable**: Samir AKNIN

## Description

L’idée principale du sujet de modélisation distribuée d’un jeu stratégique est tout
d’abord de concevoir un système où dans un monde défini, avec des règles établies, il
existe un ensemble d’entités autonomes appartenant à deux camps différents. Ces
entités communiquent entre elles et agissent pour atteindre un but défini. Nous
souhaitons, via ce système, observer les stratégies de groupes qu’utilisent les entités
appartenant à un même camp afin de triompher sur leurs adversaires.
Pour modéliser le système nous avons repris le jeu de dames dans lequel sont
définis des pions et des dames qui correspondent aux entités. Chaque équipe cherche à
gagner la partie du jeu suivant les règles préfixées.


## Pré-requis

* java JDK 11.0.5
* Maven


## Installation du projet

Placez vous à la racine du projet puis exécutez les commandes suivante:

### Installation

```shell script
mvn install
```

### Ouverture de l’application

```shell script
mvn exec:java
```

### Tests unitaires

```shell script
mvn test
```

## Documentation

[Voir documentation projet](documentation)




