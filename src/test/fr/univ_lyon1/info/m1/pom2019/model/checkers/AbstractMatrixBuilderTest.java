package fr.univ_lyon1.info.m1.pom2019.model.checkers;

import org.junit.Assert;
import org.junit.Test;

public class AbstractMatrixBuilderTest {

    @Test
    public void getSubClass() {
        Assert.assertNotEquals(AbstractMatrixBuilder.getSubClass().size(), 0);
    }

    @Test
    public void mapTypeBuilder() {
        Assert.assertNotEquals(AbstractMatrixBuilder.mapTypeBuilder().size(), 0);
    }
}