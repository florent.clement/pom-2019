package fr.univ_lyon1.info.m1.pom2019.action;

import java.util.Objects;

/**
 * Coord class.
 */
public class Coord implements Cloneable {

    private int x;
    private int y;

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Coord)) {
            return false;
        }
        Coord coord = (Coord) o;
        return getX() == coord.getX()
                && getY() == coord.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }

    /**
     * Cloning method for Coord object.
     *
     * @return cloned Coord.
     */
    public Coord clone() {
        Coord clone = null;
        try {
            clone = (Coord) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }
}
