package fr.univ_lyon1.info.m1.pom2019.learning;

import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;

public class LocationPersistenceTest {

    @BeforeClass
    public static void beforeClass() throws Exception {
        Path directory = Paths.get(Constants.LEARNING_DIRECTORY);
        if (Files.exists(directory) && Files.isDirectory(directory)
                && Files.isReadable(directory)) {

            Pattern pattern =
                    Pattern.compile("^.*0_\\d-\\d_(-2|-1|1|2)_\\d{1,2}_\\d{1,2}\\.txt$");

            try {

                Files.newDirectoryStream(directory, path -> path.toFile().isFile()
                        && pattern.matcher(path.toString()).matches())
                        .forEach(path -> {
                                    try {
                                        System.out.println("cleaning learning tests");
                                        Files.delete(path);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                        );

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void saveSingleLocation() {
        // Given
        String test = "[\n" +
                "strategyId:0;\n" +
                "originCoord:(1,2);\n" +
                "originId:-1;\n" +
                "grade:1;\n" +
                "surroundings:{\n" +
                "(2,1)=-1" +
                "\n&\n" +
                "(1,2)=-2" +
                "\n}\n" +
                "]\n";
        Location location = LocationBuilder.buildFromString(test);
        Assert.assertNotNull(location);
        Assert.assertFalse(location.getSurroundings().isEmpty());
        Assert.assertEquals(test, location.toString());

        // When
        String filename = LocationPersistence.generateName(location);
        Assert.assertTrue(LocationPersistence.saveLocation(location));
        Path path = Paths.get(filename);
        Assert.assertTrue(Files.exists(path));

        // Then
        Location res = LocationBuilder.buildFromPath(path);
        Assert.assertNotNull(res);
        Assert.assertFalse(res.getSurroundings().isEmpty());
        Assert.assertEquals(location.toString(), res.toString());
    }

    @Test
    public void saveListLocation() {
        // Given
        Random random = new Random();

        Set<Location> locationHashSet = new HashSet<>();
        for (int type = 1; type <= 2; ++type) {
            for (int color = -1; color <= 1; color += 2) {
                for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                    for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                        if((x + y) % 2 == 1) {
                            continue;
                        }
                        int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                        board[x][y] = type * color;

                        for (int locX = x - Constants.LOCATION_RADIUS;
                             locX <= x + Constants.LOCATION_RADIUS; ++locX) {
                            for (int locY = y - Constants.LOCATION_RADIUS;
                                 locY <= y + Constants.LOCATION_RADIUS; ++locY) {
                                if((locX + locY) % 2 == 1 || locX < 0 || locY < 0
                                        || locX >= Constants.CHECKERS_SIZE
                                        || locY>= Constants.CHECKERS_SIZE
                                        || locX == x || locY == y) {
                                    continue;
                                }
                                if (random.nextInt(100) % 2 == 0) {
                                    int randType = random.nextInt(100) % 2 == 0 ? 1 : 2;
                                    int randColor = random.nextInt(100) % 2 == 0 ? -1 : 1;
                                    board[locX][locY] = randType * randColor;
                                }
                            }
                        }

                        Location location = new Location(0, new Coord(x, y), type * color, 1);
                        location.generateSurroundings(board);
                        locationHashSet.add(location);
                    }
                }
            }
        }

        // When
        Assert.assertTrue(LocationPersistence.saveLocation(locationHashSet));
    }
}