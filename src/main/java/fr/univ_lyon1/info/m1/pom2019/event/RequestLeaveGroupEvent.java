package fr.univ_lyon1.info.m1.pom2019.event;


import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

/**
 * Class RequestLeaveGroupEvent.
 */
public class RequestLeaveGroupEvent<G extends Group, V extends Vote> extends Event<G, V> {

    private final G group;
    private final int[][] currentCheckers;

    /**
     * Constructor of a request event to leave a group.
     *
     * @param src             Event source
     * @param currentCheckers current currentCheckers
     */
    public RequestLeaveGroupEvent(Listeners<G, V> src, int[][] currentCheckers, G group) {
        super(src);
        this.currentCheckers = currentCheckers;
        this.group = group;
        this.group.setOwner(src.getPawn());
        this.group.addAbstractPawn(this.group.getOwner());
    }

    @Override
    public Type typeEvent() {
        return Type.REQUEST_LEAVE_GROUP;
    }

    @Override
    public int[][] getCurrentCheckers() {
        return this.currentCheckers;
    }

    /**
     * Gets group.
     *
     * @return the group
     */
    public G getGroup() {
        return group;
    }
}