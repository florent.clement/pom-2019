package fr.univ_lyon1.info.m1.pom2019.evaluation;

import fr.univ_lyon1.info.m1.pom2019.model.checkers.AbstractMatrixBuilder;
import fr.univ_lyon1.info.m1.pom2019.model.checkers.CaptureBuilder;
import fr.univ_lyon1.info.m1.pom2019.model.checkers.ClassicBuilder;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.NaivePlayer;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import scala.Tuple4;
import scala.Tuple5;

import java.util.List;

import static org.junit.Assert.*;

public class DepthEvaluationTest {

    Player player1;
    Player player2;
    int[][] checkers;
    DepthEvaluation depthEvaluation;


    @Before
    public void init() {
        AbstractMatrixBuilder abstractMatrixBuilder = new ClassicBuilder();
        this.checkers = abstractMatrixBuilder.build();

        this.player1 = new NaivePlayer(checkers, true, null);
        this.player2 = new NaivePlayer(checkers, false, player1);
        player1.setOpponent(player2);


        this.depthEvaluation = new MinMaxEvaluation(new NaiveEvaluation());
    }


    @Test
    public void getAllPossibleOpponentBoards() {

        List<Tuple5<int[][], AbstractPawn, Boolean,
                List<AbstractPawn>, List<AbstractPawn>>> result =
                depthEvaluation.getAllPossibleBoardsOpponent(
                        checkers, player1.getPieces().get(18),
                        player1.getPieces(),
                        player2.getPieces()
                );
        Assert.assertNotEquals(result.size(), 0);
    }

    @Test
    public void evaluateDepth1() {

        this.depthEvaluation.evaluate(
                checkers,
                this.player1.getPieces().get(0),
                this.player1.getPieces(),
                this.player2.getPieces()
        );
    }

    @Test
    public void getAllPossibleOwnerBoards() {


        List<Tuple4<int[][], AbstractPawn, List<AbstractPawn>, List<AbstractPawn>>> result =

                depthEvaluation.getAllPossibleBoards(
                        checkers, player1.getPieces().get(15),
                        player1.getPieces(),
                        player2.getPieces()
                );

        Assert.assertNotEquals(result.size(), 0);
        Assert.assertEquals(result.size(), 36);
    }

    @Test
    public void getAllPossibleOwnerBoardsWin() {

        this.checkers = new CaptureBuilder().build();

        this.player1 = new NaivePlayer(checkers, true, null);
        this.player2 = new NaivePlayer(checkers, false, player1);
        player1.setOpponent(player2);

        List<Tuple4<int[][], AbstractPawn, List<AbstractPawn>, List<AbstractPawn>>> result =

                depthEvaluation.getAllPossibleBoards(
                        checkers, player1.getPieces().get(0),
                        player1.getPieces(),
                        player2.getPieces()
                );

        Assert.assertEquals(result.size(), 1);
    }
}