package fr.univ_lyon1.info.m1.pom2019.app;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.model.checkers.AbstractMatrixBuilder;
import fr.univ_lyon1.info.m1.pom2019.model.checkers.ClassicBuilder;
import fr.univ_lyon1.info.m1.pom2019.model.player.NaivePlayer;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * The type Console app.
 */
public class ConsoleApp {

    /**
     * Main class for console version testing.
     *
     * @param args main arguments.
     */
    public static void main(String[] args) {
        AbstractMatrixBuilder builder = new ClassicBuilder();
        int[][] checkers = builder.build();
        Player player1 = new NaivePlayer(checkers, true, null);
        Player player2 = new NaivePlayer(checkers, false, player1);
        player1.setOpponent(player2);
        Player current = player1;
        String strPlayer = "Player 1";
        String strWinner = "";
        StringBuilder stringBuilder = new StringBuilder();

        int moves = 0;
        int turns = 0;

        int player1Captures = 0;
        int player2Captures = 0;

        boolean player1CanPlay = true;
        boolean player2CanPlay = true;
        boolean winner = false;

        stringBuilder.append("\nInitial Board:\n\n");
        printCheckers(checkers, stringBuilder);

        try {

            while ((player1CanPlay || player2CanPlay) && !winner) {

                List<Action> actionList = current.doAction();

                if (current == player1) {
                    player1CanPlay = !actionList.isEmpty();
                } else {
                    player2CanPlay = !actionList.isEmpty();
                }

                String msg1 = "Player 1 has: " + player1.getPieces().size() + " pieces.\n"
                        + "Player 2 has: " + player2.getPieces().size() + " pieces.\n\n";
                stringBuilder.append(msg1);

                if (!actionList.isEmpty()) {
                    for (Action action : actionList) {

                        // We remove the pawns
                        if (!action.getCapturedPieces().isEmpty()) {
                            if (current == player1) {
                                player2.removePawn(action.getCapturedPieces());
                                player1Captures += action.getCapturedPieces().size();
                            } else {
                                player1.removePawn(action.getCapturedPieces());
                                player2Captures += action.getCapturedPieces().size();
                            }
                        }

                        Coord arr = action.getMovements().get(action.getMovements().size() - 1);
                        Coord dep = action.getStartingPos();

                        action.getPiece().setPosition(arr);

                        updateCheckers(checkers, dep, 0);
                        updateCheckers(checkers, arr, action.getPiece().getId());
                        updateCheckers(checkers, action.getCapturedPieces());

                        ++moves;

                        String msg2 = strPlayer + " moved a piece from: "
                                + dep.getX() + ", " + dep.getY() + " to: "
                                + arr.getX() + ", " + arr.getY() + ", and captured: "
                                + action.getCapturedPieces().size() + " pieces.\n\n";
                        stringBuilder.append(msg2);

                        if (!action.getCapturedPieces().isEmpty()) {
                            stringBuilder.append("Captured pieces list: \n");
                            for (Coord coord : action.getCapturedPieces()) {
                                String c = "x: " + coord.getX() + ", y: " + coord.getY() + "\n";
                                stringBuilder.append(c);
                            }
                            stringBuilder.append("\n");
                        }

                    }
                }
                printCheckers(checkers, stringBuilder);

                if (current == player1) {
                    current = player2;
                    strPlayer = "Player 2";
                    if (player2.getPieces().isEmpty()) {
                        winner = true;
                        strWinner = "Player 1";
                    }
                } else {
                    current = player1;
                    strPlayer = "Player 1";
                    if (player1.getPieces().isEmpty()) {
                        winner = true;
                        strWinner = "Player 2";
                    }
                }

                ++turns;
            }
        } catch (ExecutionException | InterruptedException e) {

            e.printStackTrace();
        }

        String msg3 = "Player 1 is left with: " + player1.getPieces().size()
                + " pieces, and captured: " + player1Captures + " pieces.\n"
                + "Player 2 is left with: " + player2.getPieces().size()
                + " pieces, and captured: " + player2Captures + " pieces.\n\n";
        stringBuilder.append(msg3);

        if (winner) {
            String winMsg = "The winner is: " + strWinner
                    + " after " + turns + " turns and " + moves + " moves.\n";
            stringBuilder.append(winMsg);
        }

        System.out.println(stringBuilder);

    }

    private static void updateCheckers(int[][] checkers, Coord coord, int id) {
        checkers[coord.getX()][coord.getY()] = id;
    }

    private static void updateCheckers(int[][] checkers, List<Coord> lCoord) {
        for (Coord coord : lCoord) {
            updateCheckers(checkers, coord, 0);
        }
    }

    /**
     * Print checkers.
     *
     * @param checkers      the checkers
     * @param stringBuilder the string builder
     */
    public static void printCheckers(int[][] checkers, StringBuilder stringBuilder) {
        int whites = 0;
        int blacks = 0;
        int pieces = 0;

        stringBuilder.append("{");
        for (int b = 0; b < 10; ++b) {
            stringBuilder.append("{");
            for (int a = 0; a < 10; ++a) {
                String m;
                String comma = a == 9 ? "" : ",";
                m = checkers[b][a] + comma;
                if (checkers[b][a] > 0) {
                    ++whites;
                    ++pieces;
                } else if (checkers[b][a] < 0) {
                    ++blacks;
                    ++pieces;
                }
                stringBuilder.append(m);
            }
            String m;
            String comma = b == 9 ? "" : ",\n";
            m = "}" + comma;
            stringBuilder.append(m);
        }
        stringBuilder.append("}\n\n");

        String msg = "There are: " + whites + " white pieces in the board.\n"
                + "There are: " + blacks + " black pieces in the board.\n"
                + "There is a total of: " + pieces + " pieces in the board.\n\n";
        stringBuilder.append(msg);
    }
}
