package fr.univ_lyon1.info.m1.pom2019.strategy;

import fr.univ_lyon1.info.m1.pom2019.event.AuthorizeVoteEvent;
import fr.univ_lyon1.info.m1.pom2019.event.Listeners;
import fr.univ_lyon1.info.m1.pom2019.thread.StrategyListenersRunner;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;


/**
 * The type Naive strategy.
 */
public class NaiveStrategy<G extends Group, V extends Vote> extends GroupMangerStrategy<G, V> {

    /**
     * Instantiates a new Naive strategy.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     * @param opponent     the opponent
     */
    public NaiveStrategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        super(abstractPawn, player, opponent);
    }

    @Override
    public void preStepFunctions(
            List<BiConsumer<StrategyListenersRunner<G, V>,
            List<Listeners<G, V>>>> biFunctions) {

        biFunctions.add(this::movementAnnouncementsStep);
        biFunctions.add(this::groupJoinStep);
        biFunctions.add(this::existingGroupAnnouncementsStep);
        biFunctions.add(this::naiveMoveApplicationStep);
    }

    @Override
    protected void choiceGroupStep(
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {

        if (this.getGroup() != null) {

            if (this.getGroup().getOwner() == this.getPawn()) {

                AuthorizeVoteEvent<G, V> authorizeVoteEvent =
                        new AuthorizeVoteEvent<>(
                                this,
                                this.getCheckers(),
                                this.getGroup(),
                                (int) this.getGroup().getCreatedAt()
                        );

                src.sendEventAllListeners(authorizeVoteEvent,
                        new ArrayList<>(this.getExistingGroup().keySet()));
            }
        }
    }

    @Override
    public void authorizeVoteEvent(
            AuthorizeVoteEvent<G, V> event,
            StrategyListenersRunner<G, V> strategyListenersRunner,
            List<Listeners<G, V>> list) {

        if (this.getGroup() != null) {

            if (this.getGroup().getOwner() == this.getPawn() && this.isEnableGroup()) {

                if (this.getGroup().getCreatedAt() < event.getGroup().getCreatedAt()) {
                    this.setEnableGroup(false);
                }
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected G createGroup() {
        return (G)new Group();
    }
}