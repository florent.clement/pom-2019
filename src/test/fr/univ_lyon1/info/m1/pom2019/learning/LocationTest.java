package fr.univ_lyon1.info.m1.pom2019.learning;

import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class LocationTest {

    @Test
    public void generateLocationTest() {
        // Given
        Random random = new Random();

        for (int type = 1; type <= 2; ++type) {
            for (int color = -1; color <= 1; color += 2) {
                for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                    for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                        if((x + y) % 2 == 1) {
                            continue;
                        }
                        int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                        Map<Coord, Integer> map = new LinkedHashMap<>();
                        board[x][y] = type * color;
                        map.put(new Coord(x, y), type * color);

                        for (int locX = x - Constants.LOCATION_RADIUS;
                             locX <= x + Constants.LOCATION_RADIUS; ++locX) {
                            for (int locY = y - Constants.LOCATION_RADIUS;
                                 locY <= y + Constants.LOCATION_RADIUS; ++locY) {
                                if((locX + locY) % 2 == 1 || locX < 0 || locY < 0
                                        || locX >= Constants.CHECKERS_SIZE
                                        || locY>= Constants.CHECKERS_SIZE
                                        || locX == x || locY == y) {
                                    continue;
                                }
                                if (random.nextInt(100) % 2 == 0) {
                                    int randType = random.nextInt(100) % 2 == 0 ? 1 : 2;
                                    int randColor = random.nextInt(100) % 2 == 0 ? -1 : 1;
                                    board[locX][locY] = randType * randColor;
                                    map.put(new Coord(locX, locY), randType * randColor);
                                }
                            }
                        }

                        // When
                        Location location = new Location(0, new Coord(x, y), type * color, 1);
                        Map<Coord, Integer> locMap = location.generateSurroundings(board);

                        // Then
                        Assert.assertEquals(map.size(), locMap.size());
                        for (Map.Entry<Coord, Integer> entry : map.entrySet()) {
                            Assert.assertTrue(locMap.containsKey(entry.getKey()));
                            Assert.assertEquals(entry.getValue(), locMap.get(entry.getKey()));
                        }
                    }
                }
            }
        }
    }
}