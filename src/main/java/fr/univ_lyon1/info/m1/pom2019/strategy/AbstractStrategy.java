package fr.univ_lyon1.info.m1.pom2019.strategy;


import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.event.BlockEvent;
import fr.univ_lyon1.info.m1.pom2019.event.ExistingGroupEvent;
import fr.univ_lyon1.info.m1.pom2019.event.LearningFlagEvent;
import fr.univ_lyon1.info.m1.pom2019.event.Listeners;
import fr.univ_lyon1.info.m1.pom2019.event.MoveEvent;
import fr.univ_lyon1.info.m1.pom2019.event.PossibleActionsEvent;
import fr.univ_lyon1.info.m1.pom2019.event.ProtectorsEvent;
import fr.univ_lyon1.info.m1.pom2019.event.RequestJoinGroupEvent;
import fr.univ_lyon1.info.m1.pom2019.event.RequestLeaveGroupEvent;
import fr.univ_lyon1.info.m1.pom2019.event.UpdateCheckersEvent;
import fr.univ_lyon1.info.m1.pom2019.event.VoteEvent;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.thread.StrategyListenersRunner;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

import java.util.List;


/**
 * The type Abstract strategy.
 */
public abstract class AbstractStrategy<G extends Group, V extends Vote> implements Listeners<G, V> {

    private final AbstractPawn abstractPawn;
    private final Player player;
    private final Player opponent;
    private int ratioSendReceive;


    /**
     * Instantiates a new Abstract strategy.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     * @param opponent     the opponent
     */
    public AbstractStrategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        this.abstractPawn = abstractPawn;
        this.player = player;
        this.ratioSendReceive = 0;
        this.opponent = opponent;
    }

    @Override
    public void preHandle(StrategyListenersRunner<G, V> src, List<Listeners<G, V>> list) {
        System.out.println("preHandle is calling");
    }

    @Override
    public void moveEvent(
            MoveEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {
        System.out.println("MoveEvent is calling");
    }

    @Override
    public void requestJoinGroupEvent(
            RequestJoinGroupEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {
        System.out.println("RequestGroupEvent is calling");
    }

    @Override
    public void requestLeaveGroupEvent(
            RequestLeaveGroupEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {
        System.out.println("RequestLeaveGroupEvent is calling");
    }

    @Override
    public void existingGroupEvent(
            ExistingGroupEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {
        System.out.println("ExistingGroupEvent event is calling");
    }


    @Override
    public void possibleActionsEvent(
            PossibleActionsEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {
        System.out.println("PossibleActionsEvent is calling");
    }

    @Override
    public void blockEvent(
            BlockEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {
        System.out.println("Block is calling");
    }

    @Override
    public void updateCheckersEvent(
            UpdateCheckersEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list
    ) {
        System.out.println("UpdateCheckersEvent is calling");
    }

    @Override
    public void voteEvent(
            VoteEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list
    ) {
        System.out.println("VoteEvent is calling");
    }

    @Override
    public void setProtectorsEvent(ProtectorsEvent<G, V> protectorsEvent,
                                   StrategyListenersRunner<G, V> strategyListenersRunner,
                                   List<Listeners<G, V>> list) {
        System.out.println("ProtectorsEvent is calling");
    }

    @Override
    public void learningFlagEvent(LearningFlagEvent<G, V> event,
                                  StrategyListenersRunner<G, V> strategyListenersRunner,
                                  List<Listeners<G, V>> list) {
        System.out.println("LearningFlagEvent is calling");
    }

    @Override
    public final AbstractPawn getPawn() {
        return this.abstractPawn;
    }

    @Override
    public final synchronized void incrementsSending() {
        this.ratioSendReceive++;
    }

    @Override
    public final synchronized void decrementsSending() {
        this.ratioSendReceive--;
    }

    @Override
    public boolean finishedSending() {
        return this.ratioSendReceive == 0;
    }

    /**
     * Gets player.
     *
     * @return the player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Return a pawn to a listener list.
     *
     * @param listeners list of listeners
     * @param coord     coord
     * @return Pawn listener by position
     */
    protected Listeners<G, V> getListenerByPosition(
            List<Listeners<G, V>> listeners,
            Coord coord) {
        return listeners.stream().filter(l -> l.getPawn().getPosition().equals(coord))
                .findAny().orElse(null);
    }

    /**
     * Gets owner pawn.
     *
     * @return the owner pawn
     */
    protected List<AbstractPawn> getOwnerPawn() {
        return this.player.getPieces();
    }

    /**
     * Gets opponent pawn.
     *
     * @return the opponent pawn
     */
    protected List<AbstractPawn> getOpponentPawn() {
        return this.opponent.getPieces();
    }
}
