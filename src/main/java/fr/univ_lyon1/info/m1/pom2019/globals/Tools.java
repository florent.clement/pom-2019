package fr.univ_lyon1.info.m1.pom2019.globals;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.app.ConsoleApp;
import fr.univ_lyon1.info.m1.pom2019.learning.Location;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Tools.
 */
public class Tools {

    /**
     * Checker copy function.
     *
     * @param originalCheckers checker to copy.
     * @return reference to new checker containing same values as original checker.
     */
    public static int[][] cloneCheckers(int[][] originalCheckers) {
        int[][] copyChecker
                = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];

        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {
            System.arraycopy(originalCheckers[x], 0, copyChecker[x], 0, Constants.CHECKERS_SIZE);
        }
        return copyChecker;
    }

    /**
     * Checkers comparator.
     *
     * @param src1 src1
     * @param src2 src2
     * @return true or false
     */
    public static boolean compareCheckers(int[][] src1, int[][] src2) {
        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {
            for (int y = 0; y < Constants.CHECKERS_SIZE; y++) {
                if (src1[x][y] != src2[x][y]) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Apply an action on a copied board.
     *
     * @param checkers Board
     * @param action   Action
     * @return New board
     */
    public static int[][] applyActionOnTempBoard(int[][] checkers, Action action) {
        int[][] temp = cloneCheckers(checkers);

        if (action != null) {
            Coord coord = action.getStartingPos();

            temp[coord.getX()][coord.getY()] = 0;
            for (Coord pos : action.getCapturedPieces()) {
                temp[pos.getX()][pos.getY()] = 0;
            }
            for (Coord pos : action.getMovements()) {
                temp[pos.getX()][pos.getY()] = 0;
            }
            Coord lastMovement = action.getMovements()
                    .get(action.getMovements().size() - 1);
            temp[lastMovement.getX()][lastMovement.getY()] =
                    action.getPiece().getId();
        }
        return temp;
    }

    /**
     * Apply an action on a copied board.
     *
     * @param checkers Board
     * @param actions  Actions list
     * @return New board
     */
    public static int[][] applyActionOnTempBoard(int[][] checkers, List<Action> actions) {
        int[][] temp = cloneCheckers(checkers);

        for (Action action : actions) {
            temp = applyActionOnTempBoard(temp, action);
        }
        return temp;
    }

    /**
     * Apply an action on a copied board.
     *
     * @param checkers     Board
     * @param actions      Actions list
     * @param abstractPawn Pawn src
     * @return New board and action owner tuple.
     */
    public static Tuple2<int[][], AbstractPawn> applyActionBoardPawnTuple(
            int[][] checkers, List<Action> actions, AbstractPawn abstractPawn) {

        Optional<Action> actionMe = actions.stream()
                .filter(e -> e.getPiece() == abstractPawn).findFirst();


        int[][] temp = applyActionOnTempBoard(checkers, actions);
        return actionMe.map(action -> new Tuple2<>(
                temp, action.getPiece())).orElseGet(() -> new Tuple2<>(temp, abstractPawn));


    }

    /**
     * Conflict actions.
     *
     * @param action1 action to compare
     * @param action2 action to compare
     * @return true / false
     */
    public static boolean conflictAction(Action action1, Action action2) {

        if (action1.getPiece() == action2.getPiece()) {
            return true;
        }

        if ((action1.getLastMovements().getY() == action2.getLastMovements().getY())
                && (action1.getLastMovements().getX() == action2.getLastMovements().getX())) {
            return true;
        }

        return action1.getCapturedPieces().stream()
                .anyMatch(e -> action2.getCapturedPieces().contains(e));
    }

    /**
     * List of possible actions.
     *
     * @param current current action list
     * @param list    action list
     * @return Action list
     */
    public static List<Action> possibleActions(List<Action> current, List<Action> list) {
        return list.stream()
                .filter(e -> current
                        .stream().noneMatch(i -> conflictAction(e,i))
        ).collect(Collectors.toList());
    }

    /**
     * Recur search.
     *
     * @param actions current actions list
     * @param list    actions list
     * @param maxMove max move
     * @param decal   decal list
     * @return actions list
     */
    public static List<List<Action>> recursiveSearchPossibleAction(
            List<Action> actions, List<Action>  list, int maxMove, int decal) {

        List<List<Action>> result = new ArrayList<>();
        if (maxMove == 1) {

            List<Action> resultRecur = possibleActions(actions, list);

            for (int i = decal; i < resultRecur.size(); i++) {
                Action action = resultRecur.get(i);
                List<Action> tmpCopy = new ArrayList<>(actions);
                tmpCopy.add(action);
                result.add(tmpCopy);
            }

        } else {

            for (int i = decal; i < list.size(); i++) {
                List<Action> itList = new ArrayList<>(actions);
                Action action = list.get(i);
                itList.add(action);
                result.add(new ArrayList<>(itList));


                result
                        .addAll(recursiveSearchPossibleAction(
                                itList, list, maxMove - 1, decal + 1)
                );

            }

        }
        return result;

    }

    /**
     * Rules constraints.
     *
     * @param actions actions list
     * @return actions list
     */
    public static List<List<Action>> filterMove(List<List<Action>> actions) {

        boolean capture = actions.stream().anyMatch(la -> la.stream()
                .anyMatch(a -> !a.getCapturedPieces().isEmpty() || a.isConvert()));

        if (capture) {
            return actions.stream().filter(la -> la.stream()
                    .anyMatch(a -> !a.getCapturedPieces().isEmpty() || a.isConvert()))
                    .collect(Collectors.toList());
        } else {
            return actions;
        }
    }

    /**
     * Filter board list.
     *
     * @param actions the actions
     * @param board   the board
     * @return the list
     */
    public static List<List<Action>> filterBoard(List<List<Action>> actions, int[][] board) {


        List<List<Action>> result = new ArrayList<>();
        List<String> listBoard = new ArrayList<>();


        for (List<Action> actionList : actions) {

            int[][] newBoard = Tools.applyActionOnTempBoard(board, actionList);
            StringBuilder newBoardStr = new StringBuilder();
            ConsoleApp.printCheckers(newBoard, newBoardStr);

            if (!listBoard.contains(newBoardStr.toString())) {
                result.add(actionList);
                listBoard.add(newBoardStr.toString());
            }
        }
        return result;
    }

    /**
     * Match actions.
     *
     * @param actionList actions list
     * @param maxMove    max move
     * @param board      the board
     * @return Actions list
     */
    public static List<List<Action>> matchActions(List<Action> actionList,
                                                  int maxMove, int[][] board) {

        List<List<Action>> lists = new ArrayList<>(recursiveSearchPossibleAction(
                new ArrayList<>(), actionList, maxMove, 0));

        return filterBoard(filterMove(lists), board);
    }

    /**
     * Tool to check if coord are of the same ratio i.e. in a pawn movable line.
     * @param first Coord to compare.
     * @param second Coord to compare.
     * @return true if coords are aligned.
     */
    public static boolean ratio(Coord first, Coord second) {
        // add 1 to each coord to avoid divisions by 0
        int firstX = first.getX();
        int firstY = first.getY();
        ++firstX;
        ++firstY;
        int secondX = second.getX();
        int secondY = second.getY();
        ++secondX;
        ++secondY;

        return firstX / secondX == firstY / secondY;
    }
    
}
