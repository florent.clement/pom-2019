package fr.univ_lyon1.info.m1.pom2019.strategy;

import fr.univ_lyon1.info.m1.pom2019.event.AuthorizeVoteEvent;
import fr.univ_lyon1.info.m1.pom2019.event.Listeners;
import fr.univ_lyon1.info.m1.pom2019.event.RequestJoinGroupEvent;
import fr.univ_lyon1.info.m1.pom2019.event.RequestLeaveGroupEvent;
import fr.univ_lyon1.info.m1.pom2019.model.group.LimitGroup;
import fr.univ_lyon1.info.m1.pom2019.thread.StrategyListenersRunner;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.evaluation.Evaluate;
import fr.univ_lyon1.info.m1.pom2019.evaluation.NaiveEvaluation;
import fr.univ_lyon1.info.m1.pom2019.vote.NaiveVote;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Group limit strategy.
 */
public class GroupLimitStrategy<T extends Evaluate, V extends Vote>
        extends AbstractVoteStrategy<LimitGroup, T, V> {

    static int MAX_SIZE = 5;
    Evaluate evaluate;

    /**
     * Instantiates a new Naive 2 strategy.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     */
    public GroupLimitStrategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        super(abstractPawn, player, opponent);
        this.evaluate = new NaiveEvaluation();
    }

    @Override
    public int[][] getCheckers() {
        return super.getCheckers();
    }

    @Override
    protected void choiceGroupStep(
            StrategyListenersRunner<LimitGroup, V> src,
            List<Listeners<LimitGroup, V>> list) {

        if (this.getGroup() != null) {

            if (this.getGroup().getOwner() == this.getPawn()) {

                AuthorizeVoteEvent<LimitGroup, V> authorizeVoteEvent =
                        new AuthorizeVoteEvent<>(
                                this,
                                this.getCheckers(),
                                this.getGroup(),
                                this.getGroup().getWeight()
                        );

                src.sendEventAllListeners(authorizeVoteEvent,
                        new ArrayList<>(this.getExistingGroup().keySet()));
            }
        }
    }

    @Override
    public void requestJoinGroupEvent(
            RequestJoinGroupEvent<LimitGroup, V> event,
            StrategyListenersRunner<LimitGroup, V> src,
            List<Listeners<LimitGroup, V>> list) {

        AbstractPawn rPawn = null;

        if (this.getGroup() == null) {
            this.setGroup(event.getGroup());
            rPawn = this.getGroup().addAbstractPawnWithLimit(this.getPawn());
        } else if (event.getGroup().getCreatedAt() < this.getGroup().getCreatedAt()) {
            this.getGroup().removeAbstractPawnWithLimit(this.getPawn());
            this.setGroup(event.getGroup());
            rPawn = this.getGroup().addAbstractPawnWithLimit(this.getPawn());
        }
        if (rPawn != null) {
            src.sendEvent(
                    src.getListenerByWithPawn(rPawn),
                    new RequestLeaveGroupEvent<>(this, this.getCheckers(), this.getGroup())
            );
        }
    }

    @Override
    public void requestLeaveGroupEvent(
            RequestLeaveGroupEvent<LimitGroup, V> event,
            StrategyListenersRunner<LimitGroup, V> src,
            List<Listeners<LimitGroup, V>> list) {

        if (event.getGroup() == this.getGroup()) {
            this.setGroup(null);
        }
        // we form only one group
    }

    @Override
    public void authorizeVoteEvent(
            AuthorizeVoteEvent<LimitGroup, V> event,
            StrategyListenersRunner<LimitGroup, V> strategyListenersRunner,
            List<Listeners<LimitGroup, V>> list) {

        if (this.getGroup() != null) {

            if (this.getGroup().getOwner() == this.getPawn() && this.isEnableGroup()) {

                if (this.getGroup().getWeight() <= event.getWeight()) {

                    if (this.getGroup().getWeight() < event.getWeight()) {
                        this.setEnableGroup(false);
                    } else if (this.getGroup().getCreatedAt() < event.getGroup().getCreatedAt()) {
                        this.setEnableGroup(false);
                    }
                }
            }
        }
    }

    @Override
    protected LimitGroup createGroup() {
        return new LimitGroup(this.getOwnerPawn(), this.getOpponentPawn(),
                MAX_SIZE, evaluate, this.getCheckers(), 15);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected V createVote() {
        return (V) new NaiveVote();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected T createEvaluate() {
        return (T) new NaiveEvaluation();
    }

    @Override
    protected Integer customEvaluationCalc(T evaluate, int[][] board, AbstractPawn abstractPawn) {
        return evaluate.evaluate(board, abstractPawn, this.getOwnerPawn(), this.getOpponentPawn());
    }
}