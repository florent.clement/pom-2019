package fr.univ_lyon1.info.m1.pom2019.action;

import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;

import java.util.LinkedList;
import java.util.List;

/**
 * Class defining actions.
 * Each action has a constant target piece.
 */
public class Action implements Cloneable {

    private List<Coord> capturedPieces = new LinkedList<>();
    private List<Coord> movements = new LinkedList<>();
    private Coord startingPos = null;
    private boolean convert = false;
    private Class<? extends AbstractPawn> convertClass = null;
    private AbstractPawn piece;

    public Action(AbstractPawn p) {
        this.piece = p;
    }

    public void setPiece(AbstractPawn piece) {
        this.piece = piece;
    }

    public List<Coord> getCapturedPieces() {
        return capturedPieces;
    }

    public void setCapturedPieces(List<Coord> capturedPieces) {
        this.capturedPieces = capturedPieces;
    }

    public List<Coord> getMovements() {
        return movements;
    }

    public Coord getLastMovements() {
        return this.getMovements().get(this.getMovements().size() - 1);
    }

    public void setMovements(List<Coord> movements) {
        this.movements = movements;
    }

    public Coord getStartingPos() {
        return startingPos;
    }

    public void setStartingPos(Coord startingPos) {
        this.startingPos = startingPos;
    }

    public boolean isConvert() {
        return convert;
    }

    public void setConvert(boolean convert) {
        this.convert = convert;
    }

    public Class<? extends AbstractPawn> getConvertClass() {
        return convertClass;
    }

    public void setConvertClass(Class<? extends AbstractPawn> convertClass) {
        this.convertClass = convertClass;
    }

    public AbstractPawn getPiece() {
        return piece;
    }

    /**
     * Cloning method for Action object.
     * Defaults convert as false.
     *
     * @return cloned action.
     */
    public Action clone() {
        Action clone = null;
        try {
            clone = (Action) super.clone();
            clone.setPiece(this.piece);
            clone.setCapturedPieces(new LinkedList<>(this.capturedPieces));
            clone.setMovements(new LinkedList<>(this.movements));
            clone.setStartingPos(this.startingPos.clone());
            clone.setConvert(this.convert);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return clone;
    }
}
