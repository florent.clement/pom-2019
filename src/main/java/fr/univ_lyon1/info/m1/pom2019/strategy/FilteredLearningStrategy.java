package fr.univ_lyon1.info.m1.pom2019.strategy;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.evaluation.Evaluate;
import fr.univ_lyon1.info.m1.pom2019.event.BlockEvent;
import fr.univ_lyon1.info.m1.pom2019.event.LearningFlagEvent;
import fr.univ_lyon1.info.m1.pom2019.event.Listeners;
import fr.univ_lyon1.info.m1.pom2019.event.PossibleActionsEvent;
import fr.univ_lyon1.info.m1.pom2019.event.VoteEvent;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.globals.Tools;
import fr.univ_lyon1.info.m1.pom2019.learning.Location;
import fr.univ_lyon1.info.m1.pom2019.model.group.LimitGroup;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.IaPlayer;
import fr.univ_lyon1.info.m1.pom2019.model.player.LearningPlayer;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.thread.StrategyListenersRunner;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * Strategy using learning filters before group and vote steps.
 */
public class FilteredLearningStrategy<E extends Evaluate, V extends Vote>
        extends GroupLimitStrategy<E, V> {

    /**
     * List of tuples of Listeners and flags for learning filtering.
     */
    protected final Map<Listeners<LimitGroup, V>, Integer> flaggedListeners = new HashMap<>();

    /**
     * Reference to LearningPlayer locationSet.
     */
    protected final Set<Location> locationSet;

    private final int strategyId;

    /**
     * FilteredLearningStrategy default constructor.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     * @param opponent     the opponent
     */
    public FilteredLearningStrategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        super(abstractPawn, player, opponent);

        if (player instanceof LearningPlayer) {
            LearningPlayer learningPlayer = (LearningPlayer) player;
            locationSet = learningPlayer.getMemory();
            strategyId = learningPlayer.getStrategyId();
        } else {
            locationSet = null;
            strategyId = -1;
        }
    }

    @Override
    public void preStepFunctions(List<BiConsumer<StrategyListenersRunner<LimitGroup, V>,
            List<Listeners<LimitGroup, V>>>> biFunctions) {
        biFunctions.add(this::movementAnnouncementsStep);
        biFunctions.add(this::filterMeaningless);
        biFunctions.add(this::groupJoinStep);
        biFunctions.add(this::existingGroupAnnouncementsStep);
        biFunctions.add(this::choiceGroupStep);
        biFunctions.add(this::naiveVoteStep);
        biFunctions.add(this::naive2MoveApplicationStep);
    }

    @Override
    protected void movementAnnouncementsStep(StrategyListenersRunner<LimitGroup, V> src,
                                             List<Listeners<LimitGroup, V>> list) {
        IaPlayer iaPlayer = this.getIaPlayer();
        int[][] checkers = Tools.cloneCheckers(iaPlayer.getCheckers());

        List<Action> actionList = this.getPawn().getDplPossible(checkers);

        PossibleActionsEvent<LimitGroup, V> possibleActionsEvent
                = new PossibleActionsEvent<>(this, checkers, actionList);

        src.sendEventAllListeners(possibleActionsEvent, list);

        if (actionList.size() == 0) {

            List<Coord> listCoord = this.getPawn().getBlockCoord(checkers);

            for (Coord coord : listCoord) {

                Listeners<LimitGroup, V> listener = getListenerByPosition(list, coord);
                src.sendEvent(listener, new BlockEvent<>(this, checkers));

            }
        }

        this.memoryListAction = new HashMap<>();
        this.memoryListAction.put(this, actionList);
        for (Listeners<LimitGroup, V> lst : list) {
            this.memoryListAction.put(lst, new ArrayList<>());
        }

        if (locationSet != null) {
            Coord originCoord = this.getPawn().getPosition();
            boolean isPawn = this.getPawn().typePawnId() == 1;
            boolean isInLocation;
            boolean isNearLocation;
            int nearLocationValue = 0;
            int inLocationValue = 0;

            synchronized (locationSet) {
                try {
                    for (Location location : locationSet) {
                        if (this.getPawn().getId() == location.getOriginId()) {
                            Coord locationCoord = location.getOriginCoord();
                            isInLocation = originCoord == locationCoord;

                            inLocationValue = isInLocation
                                    ? Math.max(inLocationValue,
                                            location.getGrade()) : inLocationValue;

                            if (isPawn) {
                                isNearLocation =
                                        Math.abs(originCoord.getX() - locationCoord.getX()) == 1
                                                && locationCoord.getY() - originCoord.getY() == 1;
                            } else {
                                isNearLocation = Tools.ratio(locationCoord, originCoord);
                            }

                            nearLocationValue = isNearLocation
                                    ? Math.max(nearLocationValue,
                                            location.getGrade()) : nearLocationValue;

                            String sid = isPawn ? "Pawn" : "Queen";
                            if (isInLocation) {
                                System.out.println("FilteredLearningStrategy: "
                                        + sid + " In Location At " + originCoord);
                            }
                            if (isNearLocation) {
                                System.out.println("FilteredLearningStrategy: "
                                        + sid + " Near Location At " + originCoord);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (nearLocationValue > 0 || inLocationValue > 0) {
                int flag = nearLocationValue > inLocationValue ? 1 : 2;
                src.sendEvent(this,
                        new LearningFlagEvent<>(this, flag, checkers));
                this.flaggedListeners.put(this, flag);
            }
        } else {
            System.out.println(
                    "FilteredLearningStrategy: movementAnnouncementStep Null LocationSet");
        }
    }

    /**
     * Method to filter meaningless actions from memoryListAction.
     *
     * @param src  StrategyListenersRunner
     * @param list List of other listeners
     */
    protected void filterMeaningless(
            StrategyListenersRunner<LimitGroup, V> src,
            List<Listeners<LimitGroup, V>> list) {

        List<Listeners<LimitGroup, V>> meaningless = this.memoryListAction.entrySet().stream()
                .filter(entry ->
                        entry.getValue().stream().allMatch(action ->
                                action.getCapturedPieces().size() < 3
                                        && !action.isConvert())
                                && !flaggedListeners.containsKey(entry.getKey()))
                .map(Map.Entry::getKey).collect(Collectors.toList());

        if (flaggedListeners.size() > Constants.GROUP_SIZE) {
            for (Listeners<LimitGroup, V> listeners : meaningless) {
                this.memoryListAction.get(listeners).clear();
            }
        }
    }

    @Override
    public void voteEvent(VoteEvent<LimitGroup, V> event,
                          StrategyListenersRunner<LimitGroup, V> src,
                          List<Listeners<LimitGroup, V>> list) {

        if (this.getGroup() == event.getGroup()) {
            Vote vote = event.getVote();
            List<List<Action>> actionList = vote.getActions();
            E evaluate = this.createEvaluate();

            for (List<Action> action : actionList) {
                int[][] checkers = this.getIaPlayer().getCheckers();
                Tuple2<int[][], AbstractPawn> tuple2 =
                        Tools.applyActionBoardPawnTuple(checkers, action, this.getPawn());
                int[][] newCheckers = tuple2._1();
                AbstractPawn newPawn = tuple2._2();
                int evaluation = this.customEvaluationCalc(evaluate, newCheckers, newPawn);

                if (locationSet != null) {
                    if (evaluation >= Constants.MINIMUM_GRADE) {

                        Location location = new Location(strategyId,
                                newPawn.getPosition(), newPawn.getId(), evaluation);
                        location.generateSurroundings(newCheckers);

                        synchronized (locationSet) {
                            try {
                                System.out.println("FilteredLearningStrategy: Adding Location At "
                                        + location.getOriginCoord());
                                locationSet.add(location);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    System.out.println("FilteredLearningStrategy: Vote Event Null LocationSet");
                }

                vote.addAnswer(action, evaluation);
            }
        }
    }

    @Override
    public void learningFlagEvent(LearningFlagEvent<LimitGroup, V> event,
                                  StrategyListenersRunner<LimitGroup,
                                          V> strategyListenersRunner,
                                  List<Listeners<LimitGroup, V>> list) {

        if (this.flaggedListeners.containsKey(event.getSource())) {
            if (!this.flaggedListeners.get(event.getSource()).equals(event.getFlag())) {
                this.flaggedListeners.remove(event.getSource());
                this.flaggedListeners.put(event.getSource(), event.getFlag());
            }
        } else {
            this.flaggedListeners.put(event.getSource(), event.getFlag());
        }
    }
}
