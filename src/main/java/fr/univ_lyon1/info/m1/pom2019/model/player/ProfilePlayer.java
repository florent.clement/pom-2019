package fr.univ_lyon1.info.m1.pom2019.model.player;

import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.strategy.AbstractStrategy;
import fr.univ_lyon1.info.m1.pom2019.strategy.ProfileStrategy;


public class ProfilePlayer extends IaPlayer {

    public ProfilePlayer(int[][] checkers, boolean whiteColor, Player opponent) {
        super(checkers,whiteColor, opponent);
    }

    @Override
    public AbstractStrategy<?, ?> getStrategy(
            AbstractPawn abstractPawn,
            Player player, Player opponent) {
        return new ProfileStrategy<>(
                abstractPawn,
                player,
                opponent
        );
    }

    @Override
    public String getNameIa() {
        return "Naive player using profiles pawn";
    }
}
