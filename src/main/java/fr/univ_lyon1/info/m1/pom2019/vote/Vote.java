package fr.univ_lyon1.info.m1.pom2019.vote;

import fr.univ_lyon1.info.m1.pom2019.action.Action;

import java.util.List;

/**
 * The interface Vote.
 */
public interface Vote {

    /**
     * Start.
     *
     * @param action the action
     * @param board  the board
     */
    void start(List<Action> action, int[][] board);

    /**
     * Add answer.
     *
     * @param action the action
     * @param value  the value
     */
    void addAnswer(List<Action> action, Integer value);

    /**
     * Convert integer.
     *
     * @param action the action
     * @return the integer
     */
    Integer convert(List<Action> action);

    /**
     * Finish list.
     *
     * @return the list
     */
    List<Action> finish();

    /**
     * Gets actions.
     *
     * @return the actions
     */
    List<List<Action>> getActions();
}
