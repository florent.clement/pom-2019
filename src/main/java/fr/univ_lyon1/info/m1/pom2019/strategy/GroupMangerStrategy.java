package fr.univ_lyon1.info.m1.pom2019.strategy;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.event.RequestJoinGroupEvent;
import fr.univ_lyon1.info.m1.pom2019.event.Listeners;
import fr.univ_lyon1.info.m1.pom2019.event.PossibleActionsEvent;
import fr.univ_lyon1.info.m1.pom2019.event.BlockEvent;
import fr.univ_lyon1.info.m1.pom2019.event.ExistingGroupEvent;
import fr.univ_lyon1.info.m1.pom2019.exception.ConflictMoveException;
import fr.univ_lyon1.info.m1.pom2019.globals.Tools;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.IaPlayer;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.thread.StrategyListenersRunner;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The type Group manger strategy.
 *
 * @param <G> the type parameter
 * @param <V> the type parameter
 */
public abstract class GroupMangerStrategy<G extends Group, V extends Vote>
        extends AbstractStrategy<G, V> {

    /**
     * The Memory list action.
     */
    protected Map<Listeners<G, V>, List<Action>> memoryListAction;


    /**
     * The Existing group.
     */
    protected Map<Listeners<G,V>, G> existingGroup;

    /**
     * The Group.
     */
    private G group;


    private boolean enableGroup;

    /**
     * Instantiates a new Abstract strategy.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     * @param opponent     the opponent
     */
    public GroupMangerStrategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        super(abstractPawn, player, opponent);
    }

    /**
     * First step: announcement of possible moves.
     *
     * @param src  StrategyListenersRunner
     * @param list List of other listeners
     */
    protected void movementAnnouncementsStep(
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {


        IaPlayer iaPlayer = this.getIaPlayer();
        int[][] checkers = Tools.cloneCheckers(iaPlayer.getCheckers());

        List<Action> actionList = this.getPawn().getDplPossible(checkers);

        PossibleActionsEvent<G, V> possibleActionsEvent
                = new PossibleActionsEvent<>(this, checkers, actionList);

        src.sendEventAllListeners(possibleActionsEvent, list);

        if (actionList.size() == 0) {

            List<Coord> listCoord = this.getPawn().getBlockCoord(checkers);

            for (Coord coord : listCoord) {

                Listeners<G, V> listener = getListenerByPosition(list, coord);
                src.sendEvent(listener, new BlockEvent<>(this, checkers));

            }
        }

        this.memoryListAction = new HashMap<>();
        this.memoryListAction.put(this, actionList);
        for (Listeners<G, V> lst : list) {
            this.memoryListAction.put(lst, new ArrayList<>());
        }
    }

    @Override
    public final IaPlayer getIaPlayer() {
        return (IaPlayer) this.getPlayer();
    }

    @Override
    public int[][] getCheckers() {
        return this.getIaPlayer().getNewCheckers();
    }

    /**
     * Second step: Send group requests.
     *
     * @param src  StrategyListenersRunner
     * @param list List of other listeners
     */
    protected void groupJoinStep(StrategyListenersRunner<G, V> src, List<Listeners<G, V>> list) {

        int[][] checkers = Tools.cloneCheckers(this.getIaPlayer().getCheckers());

        if (!this.memoryListAction.get(this).isEmpty()) {

            RequestJoinGroupEvent<G, V> requestJoinGroupEvent =
                    new RequestJoinGroupEvent<>(this, checkers, this.createGroup());

            this.setGroup(requestJoinGroupEvent.getGroup());

            for (Listeners<G, V> listener : list) {

                if (!this.memoryListAction.get(listener).isEmpty() && listener != this) {
                    src.sendEvent(listener, requestJoinGroupEvent);
                }
            }
        }
    }

    /**
     * Existing group announcements step.
     *
     * @param src  the src
     * @param list the list
     */
    protected void existingGroupAnnouncementsStep(
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {

        if (this.group != null) {


            if (this.group.getOwner() == this.getPawn()) {

                this.enableGroup = true;
                this.existingGroup = new HashMap<>();
                ExistingGroupEvent<G, V> existingGroupEvent = new ExistingGroupEvent<>(
                        this,
                        this.getCheckers(),
                        this.group
                );

                src.sendEventAllListeners(existingGroupEvent, list);
            }
        }
    }

    /**
     * Choice group step.
     *
     * @param src  the src
     * @param list the list
     */
    protected abstract void choiceGroupStep(
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list);

    /**
     * Here, the reasoning is naive, we choose one or
     * more possible moves and we offer it to others.
     *
     * @param src       StrategyListenersRunner
     * @param listeners List of other listeners
     */
    protected void naiveMoveApplicationStep(
            StrategyListenersRunner<G, V> src, List<Listeners<G, V>> listeners) {

        if (this.getGroup() != null) {

            if (this.getGroup().getOwner() == this.getPawn() && this.enableGroup) {

                List<List<Action>> list = new ArrayList<>();

                for (Listeners<G, V> lst : listeners) {
                    if (this.getGroup().contains(lst.getPawn())) {
                        list.add(this.memoryListAction.get(lst));
                    }
                }
                list.add(this.memoryListAction.get(this));

                int[][] checkers = Tools.cloneCheckers(this.getIaPlayer().getNewCheckers());

                if (!list.isEmpty()) {
                    List<Action> actionListRandom = list.get(
                            (int) (Math.random() * list.size()));
                    Action actionRandom = actionListRandom.get(
                            (int) (Math.random() * actionListRandom.size()));

                    try {
                        this.getIaPlayer().notifyAction(checkers, actionRandom);
                    } catch (ConflictMoveException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void possibleActionsEvent(
            PossibleActionsEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {
        this.memoryListAction.put(event.getSource(), event.getListAction());
    }

    @Override
    public void requestJoinGroupEvent(
            RequestJoinGroupEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {

        // We take only the most recent

        if (this.getGroup() == null) {
            this.setGroup(event.getGroup());
            this.getGroup().addAbstractPawn(this.getPawn());
        } else if (event.getGroup().getCreatedAt() < this.getGroup().getCreatedAt()) {
            this.getGroup().removeAbstractPawn(this.getPawn());
            this.setGroup(event.getGroup());
            this.getGroup().addAbstractPawn(this.getPawn());
        }
    }

    @Override
    public void existingGroupEvent(
            ExistingGroupEvent<G, V> event,
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> list) {

        if (this.group != null) {

            if (this.group.getOwner() == this.getPawn()
                    && event.getGroup().getOwner() != this.getPawn()) {

                this.existingGroup.put(event.getSource(), event.getGroup());
            }
        }
    }

    /**
     * Create group.
     *
     * @return the group
     */
    protected abstract G createGroup();

    /**
     * Gets group.
     *
     * @return the group
     */
    protected final G getGroup() {
        return this.group;
    }

    /**
     * Sets group.
     *
     * @param group the group
     */
    public void setGroup(G group) {
        this.group = group;
    }

    /**
     * Gets existing group.
     *
     * @return the existing group
     */
    public Map<Listeners<G, V>, G> getExistingGroup() {
        return existingGroup;
    }

    /**
     * Gets memory list action.
     *
     * @param listeners the listeners
     * @return the memory list action
     */
    public List<Action> getMemoryListAction(
            Listeners<G, V> listeners) {
        return memoryListAction.get(listeners);
    }

    public void setEnableGroup(boolean enableGroup) {
        this.enableGroup = enableGroup;
    }

    public boolean isEnableGroup() {
        return enableGroup;
    }
}