package fr.univ_lyon1.info.m1.pom2019.learning;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Set;

/**
 * Persistence class to save Locations.
 */
public class LocationPersistence {

    /**
     * Filename generator based on Location.
     * @param location Location to generate name.
     * @return filename in format SRATEDY_COORD_ID_GRADE_SURROUNDINGS-SIZE.
     */
    public static String generateName(Location location) {
        if (location != null) {
            return Constants.LEARNING_DIRECTORY + "/"
                    + location.getStrategyId() + "_"
                    + location.getOriginCoord().getX() + "-"
                    + location.getOriginCoord().getY() + "_"
                    + location.getOriginId() + "_"
                    + location.getGrade() + "_"
                    + location.getSurroundings().size() + ".txt";
        } else {
            return "";
        }
    }

    /**
     * Method to ensure Learning Directory existence.
     * @return true if directory exists or was successfully created.
     */
    private static boolean existsOrCreateDirectory() {
        Path path = Paths.get(Constants.LEARNING_DIRECTORY);
        if (Files.exists(path) && Files.isDirectory(path)
                && Files.isReadable(path)) {
            return true;
        } else {
            try {
                Path res = Files.createDirectory(path);
                return res.compareTo(path) == 0;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    /**
     * Method to persist single location.
     * @param location Location to persist.
     * @return True if location was successfully saved, false otherwise.
     */
    public static boolean saveLocation(Location location) {
        if (location != null) {
            String filename = generateName(location);
            if (!filename.isEmpty()) {
                if (existsOrCreateDirectory()) {
                    Path path = Paths.get(filename);
                    InputStream in = new ByteArrayInputStream(
                            location.toString().getBytes(StandardCharsets.UTF_8));
                    try {
                        System.out.println("try to persist " + path.toString());
                        long res = Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
                        return res > 0L;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Method to persist list of location.
     * @param locations List of Location to persist.
     * @return True if all locations were successfully saved, false otherwise.
     */
    public static boolean saveLocation(Set<Location> locations) {
        if (locations != null && !locations.isEmpty()) {
            boolean success = true;
            for (Location location : locations) {
                success = success && saveLocation(location);
            }
            return success;
        }
        return false;
    }
}
