package fr.univ_lyon1.info.m1.pom2019.evaluation;

import fr.univ_lyon1.info.m1.pom2019.model.checkers.AbstractMatrixBuilder;
import fr.univ_lyon1.info.m1.pom2019.model.checkers.CaptureBuilder;
import fr.univ_lyon1.info.m1.pom2019.model.checkers.ClassicBuilder;
import fr.univ_lyon1.info.m1.pom2019.model.player.NaivePlayer;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PointEvaluationTest {

    AbstractMatrixBuilder abstractMatrixBuilder;
    int[][] checkers;

    Player player1;
    Player player2;

    PointEvaluation pointEvaluation;

    @Before
    public void initPlayer() {
        this.abstractMatrixBuilder = new CaptureBuilder();
        this.checkers = this.abstractMatrixBuilder.build();

        this.player1 = new NaivePlayer(checkers, true, null);
        this.player2 = new NaivePlayer(checkers, false, player1);
        player1.setOpponent(this.player2);

        this.pointEvaluation = new PointEvaluation();
    }

    @Test
    public void protectEvaluate() {
        Assert.assertEquals(
                (Integer) 0,
                this.pointEvaluation.protectEvaluate(
                        this.checkers,
                        this.player1.getPieces().get(0),
                        player1.getPieces(),
                        this.player1.getPieces().get(0).getPosition()));
    }

    @Test
    public void offensiveEvaluate() {
        Assert.assertEquals(
                (Integer) 1,
                this.pointEvaluation.offensiveEvaluate(
                        this.checkers,
                        this.player1.getPieces().get(0)));
    }

    @Test
    public void riskEvaluate() {
        Assert.assertEquals(
                (Integer) 0,
                this.pointEvaluation.riskEvaluate(
                        this.checkers,
                        this.player1.getPieces().get(0),
                        this.player2.getPieces()));
    }

    @Test
    public void conversionEvaluate() {
    }

    @Test
    public void controlEvaluate() {
    }

    @Test
    public void evaluate() {
        this.abstractMatrixBuilder = new ClassicBuilder();
        this.checkers = this.abstractMatrixBuilder.build();

        this.player1 = new NaivePlayer(checkers, true, null);
        this.player2 = new NaivePlayer(checkers, false, player1);
        player1.setOpponent(this.player2);



        Integer value = this.pointEvaluation.evaluate(checkers,player1.getPieces().get(0),
                player1.getPieces(),
                player2.getPieces());

        Assert.assertNotEquals(value, null);
    }
}