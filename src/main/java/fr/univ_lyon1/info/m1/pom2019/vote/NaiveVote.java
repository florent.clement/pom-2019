package fr.univ_lyon1.info.m1.pom2019.vote;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.globals.Tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Comparator;

/**
 * The type Naive vote.
 */
public class NaiveVote implements Vote {

    /**
     * The Map.
     */
    Map<List<Action>, List<Integer>> map;
    /**
     * The Actions.
     */
    List<List<Action>> actions;

    /**
     * Start.
     *
     * @param actions the actions
     * @param board   the board
     */
    @Override
    public void start(List<Action> actions, int[][] board) {
        this.actions = Tools.matchActions(actions, Constants.MAX_MOVE, board);
        this.map = new HashMap<>();
        for (List<Action> action : this.actions) {
            map.put(action, new ArrayList<>());
        }
    }

    @Override
    public void addAnswer(List<Action> action, Integer value) {
        synchronized (this) {
            this.map.get(action).add(value);
        }
    }

    @Override
    public Integer convert(List<Action> actions) {
        int sum = 0;
        for (Integer v : map.get(actions)) {
            sum += v;
        }
        return sum;
    }

    @Override
    public List<Action> finish() {
        return this.map.keySet().stream().max(Comparator.comparingInt(this::convert)).orElse(null);
    }

    @Override
    public List<List<Action>> getActions() {
        return actions;
    }
}
