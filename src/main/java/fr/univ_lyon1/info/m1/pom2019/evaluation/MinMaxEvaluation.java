package fr.univ_lyon1.info.m1.pom2019.evaluation;

import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import scala.Tuple4;
import scala.Tuple5;

import java.util.Comparator;
import java.util.List;

/**
 * The type Min max evaluation.
 */
public class MinMaxEvaluation extends DepthEvaluation {

    /**
     * Instantiates a new Min max evaluation.
     *
     * @param evaluate the evaluate
     */
    public MinMaxEvaluation(Evaluate evaluate) {
        super(evaluate);
    }

    @Override
    Tuple5<int[][], AbstractPawn, Boolean, List<AbstractPawn>, List<AbstractPawn>> maxMove(
            List<Tuple5<int[][], AbstractPawn, Boolean,
                    List<AbstractPawn>, List<AbstractPawn>>> listMoves,
            AbstractPawn abstractPawn,
            List<AbstractPawn> owner, List<AbstractPawn> opponent) {

        return listMoves.stream().max((t1, t2) -> {
            if (t1._2() != t2._2()) {
                return Boolean.compare(t2._3(), t1._3());
            } else {
                return Integer.compare(
                        getEvaluate().evaluate(t1._1(), abstractPawn, owner, opponent),
                        getEvaluate().evaluate(t2._1(), abstractPawn, owner, opponent)
                );
            }
        }).orElse(null);
    }

    @Override
    Tuple4<int[][], AbstractPawn, List<AbstractPawn>, List<AbstractPawn>> minMove(
            List<Tuple4<int[][], AbstractPawn, List<AbstractPawn>, List<AbstractPawn>>> listMoves,
            AbstractPawn abstractPawn, List<AbstractPawn> owner, List<AbstractPawn> opponent) {

        return listMoves.stream().min(Comparator.comparingInt(t -> getEvaluate()
                .evaluate(t._1(), abstractPawn, owner, opponent))).orElse(null);
    }
}
