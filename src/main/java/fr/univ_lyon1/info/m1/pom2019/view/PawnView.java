package fr.univ_lyon1.info.m1.pom2019.view;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;

/**
 * View class for displaying pawns.
 */
public class PawnView extends PieceView {

    /**
     * Each pawn will be represented by a sphere.
     */
    private final Sphere pawn;

    /**
     * Color determines the color of the pawn.
     * @param color 0 for white, else will be a black piece.
     */
    public PawnView(int color, CaseView parent) {
        super(color,parent);
        this.pawn = new Sphere(Math.min(parent.getWidth(),parent.getHeight()) * 0.4);
        PhongMaterial material = new PhongMaterial();
        if (color == 0) {
            material.setDiffuseColor(Color.WHITE);
            material.setSpecularColor(Color.BLACK);
        } else {
            int black = 60;
            material.setDiffuseColor(Color.rgb(black,black,black));
            material.setSpecularColor(Color.WHITE);
        }
        this.pawn.setMaterial(material);
    }

    @Override
    public Node getNode() {
        return this.pawn;
    }

    @Override
    public void resize(CaseView parent) {
        this.parent = parent;
        this.pawn.setRadius(Math.min(parent.getWidth(),parent.getHeight()) * 0.4);
    }

    @Override
    public int getId() {
        return 1; // Simple pawn category.
    }
}
