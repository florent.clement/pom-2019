package fr.univ_lyon1.info.m1.pom2019.event;


import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

import java.util.List;

/**
 * Class MoveEvent.
 */
public class PossibleActionsEvent<G extends Group, V extends Vote> extends Event<G, V> {

    private final List<Action> listAction;
    private final int[][] currentCheckers;

    /**
     * Possible action event builder.
     *
     * @param src             Source of the event
     * @param currentCheckers Checkers source
     * @param listAction      List of related actions
     */
    public PossibleActionsEvent(
            Listeners<G, V> src,
            int[][] currentCheckers,
            List<Action> listAction) {
        super(src);
        this.currentCheckers = currentCheckers;
        this.listAction = listAction;
    }

    @Override
    public Type typeEvent() {
        return Type.POSSIBLE_ACTION;
    }

    @Override
    public int[][] getCurrentCheckers() {
        return this.currentCheckers;
    }

    /**
     * Gets list action.
     *
     * @return the list action
     */
    public List<Action> getListAction() {
        return listAction;
    }
}