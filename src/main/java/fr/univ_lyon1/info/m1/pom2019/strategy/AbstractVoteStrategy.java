package fr.univ_lyon1.info.m1.pom2019.strategy;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.evaluation.Evaluate;
import fr.univ_lyon1.info.m1.pom2019.event.Listeners;
import fr.univ_lyon1.info.m1.pom2019.event.VoteEvent;
import fr.univ_lyon1.info.m1.pom2019.exception.ConflictMoveException;
import fr.univ_lyon1.info.m1.pom2019.globals.Tools;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.thread.StrategyListenersRunner;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;


/**
 * The type Abstract vote strategy.
 *
 * @param <T> the type parameter
 * @param <V> the type parameter
 */
public abstract class AbstractVoteStrategy<G extends Group, T extends Evaluate,
        V extends Vote> extends GroupMangerStrategy<G, V> {

    /**
     * The Vote.
     */
    V vote;

    /**
     * Instantiates a new Naive strategy.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     * @param opponent     the opponent
     */
    public AbstractVoteStrategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        super(abstractPawn, player, opponent);
    }

    @Override
    public int[][] getCheckers() {
        return this.getIaPlayer().getNewCheckers();
    }

    @Override
    public void preStepFunctions(
            List<BiConsumer<StrategyListenersRunner<G, V>, List<Listeners<G, V>>>> biFunctions) {

        biFunctions.add(this::movementAnnouncementsStep);
        biFunctions.add(this::groupJoinStep);
        biFunctions.add(this::existingGroupAnnouncementsStep);
        biFunctions.add(this::choiceGroupStep);
        biFunctions.add(this::naiveVoteStep);
        biFunctions.add(this::naive2MoveApplicationStep);
    }

    /**
     * Naive vote step.
     *
     * @param src       the src
     * @param listeners the listeners
     */
    protected final void naiveVoteStep(
            StrategyListenersRunner<G, V> src,
            List<Listeners<G, V>> listeners) {

        if (this.getGroup() != null) {

            if (this.getGroup().getOwner() == this.getPawn() && this.isEnableGroup()) {

                List<Action> list = new ArrayList<>();

                for (Listeners<G, V> lst : listeners) {
                    if (this.getGroup().contains(lst.getPawn())) {
                        list.addAll(this.getMemoryListAction(lst));
                    }
                }
                list.addAll(this.getMemoryListAction(this));

                int[][] checkers = Tools.cloneCheckers(this.getIaPlayer().getNewCheckers());

                // On lance les votes
                this.vote = this.createVote();
                this.vote.start(list, this.getCheckers());

                src.sendEventAllListeners(new VoteEvent<>(this, checkers, vote, this.getGroup()));
            }
        }
    }

    @Override
    public void voteEvent(
            VoteEvent<G, V> event, StrategyListenersRunner<G, V> src, List<Listeners<G, V>> list) {

        if (this.getGroup() == event.getGroup()) {
            Vote vote = event.getVote();
            List<List<Action>> actionList = vote.getActions();
            T evaluate = this.createEvaluate();

            for (List<Action> action : actionList) {
                int[][] checkers = this.getIaPlayer().getCheckers();
                Tuple2<int[][], AbstractPawn> tuple2 =
                        Tools.applyActionBoardPawnTuple(checkers, action, this.getPawn());
                int[][] newCheckers = tuple2._1();
                AbstractPawn newPawn = tuple2._2();
                vote.addAnswer(action, this.customEvaluationCalc(evaluate, newCheckers, newPawn));
            }
        }
    }

    /**
     * Fourth step.
     *
     * @param strategyListenersRunner the strategy listeners runner
     * @param listeners               the listeners
     */
    protected final void naive2MoveApplicationStep(
            StrategyListenersRunner<G, V> strategyListenersRunner,
            List<Listeners<G, V>> listeners) {

        if (this.getGroup() != null) {

            if (this.getGroup().getOwner() == this.getPawn() && this.isEnableGroup()) {


                List<Action> list = this.vote.finish();

                for (Action action : list) {

                    int[][] checkers = this.getIaPlayer().getNewCheckers();
                    try {
                        this.getIaPlayer().notifyAction(checkers, action);
                    } catch (ConflictMoveException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    /**
     * Create vote vote.
     *
     * @return the vote
     */
    protected abstract V createVote();

    /**
     * Create evaluate evaluate.
     *
     * @return the evaluate
     */
    protected abstract T createEvaluate();

    /**
     * Custom evaluation calc integer.
     *
     * @param evaluate     the evaluate
     * @param board        the board
     * @param abstractPawn the abstract pawn
     * @return the integer
     */
    protected abstract Integer customEvaluationCalc(
            T evaluate, int[][] board, AbstractPawn abstractPawn);
}