package fr.univ_lyon1.info.m1.pom2019.app;

import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import org.junit.Assert;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import static org.junit.Assert.*;

public class SceneAppTest extends ApplicationTest {

    private static final String NAME_APP = "pom-2019";
    private static final String LOCATION_FXML = "/ui.fxml";
    private FXMLLoader loader = new FXMLLoader();

    private boolean failLoad = false;
    private Stage stage;

    /**
     * Will be called with {@code @Before} semantics, i. e. before each test method.
     */
    @Override
    public void start(Stage stage) {

    }

    /**
     * Configure la scène en fonction des options pré-définis.
     * @param stage scène de l'application
     */
    private void setup(Stage stage) {
        stage.setTitle(NAME_APP);
    }

    @Test
    public void failLoad() {
        Assert.assertFalse(this.failLoad);
    }

    @Test
    public void title() {
        //Assert.assertEquals(NAME_APP,this.stage.getTitle());
    }
}