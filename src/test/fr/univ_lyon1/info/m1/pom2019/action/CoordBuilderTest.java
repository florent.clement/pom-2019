package fr.univ_lyon1.info.m1.pom2019.action;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CoordBuilderTest {

    @Test
    public void buildFromString() {
        // Given
        Coord original = new Coord(1, 2);
        String testString = "(1,2)";
        Assert.assertEquals(original.toString(), testString);

        // When
        Coord test = CoordBuilder.buildFromString(testString);

        // Then
        Assert.assertEquals(test, original);

        // Error nullity tests
        Assert.assertNull(CoordBuilder.buildFromString(""));
        //Assert.assertNull(CoordBuilder.buildFromString("aaaa"));
        Assert.assertNull(CoordBuilder.buildFromString("1,2"));
        //Assert.assertNull(CoordBuilder.buildFromString("(a,b)"));
        Assert.assertNull(CoordBuilder.buildFromString("(-1,2)"));
        Assert.assertNull(CoordBuilder.buildFromString("(-1,-2)"));
        Assert.assertNull(CoordBuilder.buildFromString("(1,-2)"));
        Assert.assertNull(CoordBuilder.buildFromString("(220,30)"));
    }
}