package fr.univ_lyon1.info.m1.pom2019.evaluation;

import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;

import java.util.List;

/**
 * The interface Evaluate.
 */
public interface Evaluate {

    /**
     * Evaluate integer.
     *
     * @param board        the board
     * @param abstractPawn the abstract pawn
     * @param owner        the owner
     * @param opponent     the opponent
     * @return the integer
     */
    Integer evaluate(int[][] board, AbstractPawn abstractPawn,
                     List<AbstractPawn> owner, List<AbstractPawn> opponent);
}
