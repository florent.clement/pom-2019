package fr.univ_lyon1.info.m1.pom2019.model.checkers;

/**
 * Pawn layout generator.
 */
public class BugBuilder extends AbstractMatrixBuilder {

    @Override
    public int[][] build() {


        return new int[][]{
                {0, 0, 1, 0, 1, 0, -1, 0, 0, 0},
                {0, 1, 0, 1, 0, -1, 0, -1, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, -1, 0},
                {0, 1, 0, 0, 0, -1, 0, -1, 0, -1},
                {1, 0, 1, 0, 0, 0, -1, 0, -1, 0},
                {0, 1, 0, 1, 0, -1, 0, -1, 0, -1},
                {1, 0, 1, 0, 0, 0, -1, 0, -1, 0},
                {0, 1, 0, 1, 0, 0, 0, -1, 0, -1},
                {1, 0, 1, 0, 0, 0, -1, 0, -1, 0},
                {0, 1, 0, 1, 0, 0, 0, -1, 0, -1}};
    }

    @Override
    public String getName() {
        return "Configuration with a bug";
    }
}
