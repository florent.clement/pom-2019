package fr.univ_lyon1.info.m1.pom2019.event;


import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

/**
 * Class ExistingGroupEvent.
 */
public class ExistingGroupEvent<G extends Group, V extends Vote> extends Event<G, V> {

    private final G group;
    private final int[][] currentCheckers;

    /**
     * Existing group event constructor.
     *
     * @param src             Source of the event
     * @param currentCheckers Checkers source
     */
    public ExistingGroupEvent(Listeners<G, V> src, int[][] currentCheckers, G group) {
        super(src);
        this.currentCheckers = currentCheckers;
        this.group = group;
    }

    @Override
    public Type typeEvent() {
        return Type.EXISTING_GROUP;
    }

    @Override
    public int[][] getCurrentCheckers() {
        return this.currentCheckers;
    }

    public G getGroup() {
        return group;
    }
}