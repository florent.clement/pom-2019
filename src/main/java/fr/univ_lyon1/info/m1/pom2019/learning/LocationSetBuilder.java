package fr.univ_lyon1.info.m1.pom2019.learning;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Builder class for recursive Location building.
 */
public class LocationSetBuilder {

    /**
     * Method for recursive Location building corresponding to strategy from directory.
     * @param strategy strategy id to load
     * @return List of Location if build successful null otherwise.
     */
    public static Set<Location> buildWithStrategy(int strategy) {
        Set<Location> locations = new HashSet<>();

        Path directory = Paths.get(Constants.LEARNING_DIRECTORY);
        if (Files.exists(directory) && Files.isDirectory(directory)
                && Files.isReadable(directory)) {

            Pattern pattern =
                    Pattern.compile("^.*" + strategy
                            + "_\\d{1,2}-\\d_(-2|-1|1|2)_\\d{1,2}_\\d{1,2}\\.txt$");

            try {
                Files.newDirectoryStream(directory, path -> path.toFile().isFile()
                        && pattern.matcher(path.toString()).matches())
                        .forEach(path -> {
                            System.out.println("try to build " + path.toString());
                            Location loc = LocationBuilder.buildFromPath(path);
                            if (loc != null) {
                                locations.add(loc);
                            }
                        }
                    );
            } catch (IOException e) {
                return null;
            }
        }

        return locations;
    }

}
