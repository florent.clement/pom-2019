package fr.univ_lyon1.info.m1.pom2019.event;

import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

/**
 * Class MoveEvent.
 */
public class VoteEvent<G extends Group, V extends Vote> extends Event<G, V> {

    private final V vote;
    private final int[][] currentCheckers;
    private final G group;

    /**
     * Voting event constructor.
     *
     * @param src             Event source
     * @param currentCheckers current currentCheckers
     * @param vote            Vote source
     * @param group           the group
     */
    public VoteEvent(Listeners<G, V> src, int[][] currentCheckers, V vote, G group) {
        super(src);
        this.currentCheckers = currentCheckers;
        this.vote = vote;
        this.group = group;
    }

    @Override
    public Type typeEvent() {
        return Type.VOTE;
    }

    @Override
    public int[][] getCurrentCheckers() {
        return this.currentCheckers;
    }

    /**
     * Gets vote.
     *
     * @return the vote
     */
    public V getVote() {
        return vote;
    }

    /**
     * Gets group.
     *
     * @return the group
     */
    public G getGroup() {
        return group;
    }
}