package fr.univ_lyon1.info.m1.pom2019.app;

import javafx.scene.Scene;
import javafx.stage.Stage;
import org.datafx.controller.flow.Flow;
import org.datafx.controller.flow.FlowHandler;
import org.datafx.controller.flow.FlowView;
import org.junit.Assert;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import static org.junit.Assert.*;

public class AppControllerTest extends ApplicationTest {

    private AppController appController;

    @Test
    public void testGridPane() {
        Assert.assertNotNull(this.appController.getGridPane());
    }

    @Test
    public void testLoggerText() {
        Assert.assertNotNull(this.appController.getLoggerText());
    }

    @Test
    public void testConsoleBackground() {
        Assert.assertNotNull(this.appController.getConsoleBackground());
    }

    @Test
    public void initChoiceBox() {
        Assert.assertNotEquals(this.appController.getChoiceBox().getItems().size(),0);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Flow flow = new Flow(AppController.class);
        FlowHandler handler = flow.createHandler();
        stage.setScene(new Scene(handler.start()));
        FlowView view = handler.getCurrentView();
        this.appController = (AppController) view.getViewContext().getController();
    }
}