package fr.univ_lyon1.info.m1.pom2019.thread;

import fr.univ_lyon1.info.m1.pom2019.globals.Tools;
import fr.univ_lyon1.info.m1.pom2019.event.BlockEvent;
import fr.univ_lyon1.info.m1.pom2019.event.Event;
import fr.univ_lyon1.info.m1.pom2019.event.Listeners;
import fr.univ_lyon1.info.m1.pom2019.event.RequestJoinGroupEvent;
import fr.univ_lyon1.info.m1.pom2019.event.RequestLeaveGroupEvent;
import fr.univ_lyon1.info.m1.pom2019.event.ProtectorsEvent;
import fr.univ_lyon1.info.m1.pom2019.event.AuthorizeVoteEvent;
import fr.univ_lyon1.info.m1.pom2019.event.VoteEvent;
import fr.univ_lyon1.info.m1.pom2019.event.UpdateCheckersEvent;
import fr.univ_lyon1.info.m1.pom2019.event.MoveEvent;
import fr.univ_lyon1.info.m1.pom2019.event.PossibleActionsEvent;
import fr.univ_lyon1.info.m1.pom2019.event.ExistingGroupEvent;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * Implementation of naive class strategy.
 */
public class StrategyListenersRunner<G extends Group, V extends Vote> implements Runnable {

    private final Monitor<StrategyListenersRunner<? extends G, ? extends V>> monitor;
    private final Listeners<G, V> listener;
    private Event<G, V> currentEvent;

    @SuppressWarnings("unchecked")
    private StrategyListenersRunner(
            Monitor<StrategyListenersRunner<? extends G, ? extends V>> monitor,
            Listeners<? extends G, ? extends V> listeners) {

        this.listener = (Listeners<G, V>) listeners;
        this.monitor = monitor;
    }

    /**
     * Builder Monitor.
     *
     * @param listeners listeners
     * @return monitor monitor
     */
    public static <G extends Group, V extends Vote> Monitor<StrategyListenersRunner
            <? extends G, ? extends V>> build(
                List<Listeners<? extends G, ? extends V>> listeners) {

        Monitor<StrategyListenersRunner<? extends G, ? extends V>> monitor = new Monitor<>();
        for (Listeners<? extends G, ? extends V> listener : listeners) {
            StrategyListenersRunner<? extends G, ? extends V> strategyListenersRunner
                    = new StrategyListenersRunner<>(monitor, listener);
            monitor.add(strategyListenersRunner);
        }
        return monitor;
    }

    /**
     * Return list StrategyListenersRunner without src.
     *
     * @param listenersRunners list runner
     * @param filter           src
     * @return list listeners
     */
    private static <G extends Group, V extends Vote> List<StrategyListenersRunner
            <? extends G, ? extends V>> filterList(
            List<StrategyListenersRunner<? extends G, ? extends V>> listenersRunners,
            StrategyListenersRunner<? extends G, ? extends V> filter) {

        return listenersRunners.stream().filter(runner -> !runner.equals(filter))
                .collect(Collectors.toList());
    }

    /**
     * Return list listeners.
     *
     * @param listenersRunners list runner
     * @return list listeners
     */
    @SuppressWarnings("unchecked")
    private static <G extends Group, V extends Vote> List<Listeners<G, V>> listListeners(
            List<StrategyListenersRunner
                    <? extends G, ? extends V>> listenersRunners) {
        return listenersRunners.stream().map(runner -> (Listeners<G, V>) runner.listener)
                .collect(Collectors.toList());
    }

    private static boolean checkersIsCorrect(Listeners<?, ?> listener, Event<?, ?> event) {
        return Tools.compareCheckers(
                listener.getCheckers(),
                event.getCurrentCheckers());
    }

    private StrategyListenersRunner
            <? extends G, ? extends V> getByListener(Listeners<G, V> listener) {
        return this.monitor.getListRunnable().stream()
                .filter(runner -> runner.listener == listener)
                .findFirst().orElse(null);
    }

    /**
     * Sending an event.
     *
     * @param lst   Destination
     * @param event Event to send
     */
    @SuppressWarnings("unchecked")
    public void sendEvent(Listeners<G, V> lst, Event<G, V> event) {
        StrategyListenersRunner<G, V> rcv =
                (StrategyListenersRunner<G, V>) this.getByListener(lst);

        synchronized (rcv) {

            if (checkersIsCorrect(lst, event)) {
                this.listener.incrementsSending();
                monitor.notify(rcv, () -> rcv.currentEvent = event);
            }
        }
    }

    /**
     * Send an event to all other Listeners.
     *
     * @param event Event to send
     */
    public void sendEventAllListeners(Event<G, V> event) {
        this.sendEventAllListeners(
                event,
                listListeners(filterList(this.monitor.getListRunnable(), this)));
    }

    /**
     * Sends an event to a list of Listeners.
     *
     * @param event     Event to send
     * @param listeners List of listeners
     */
    public void sendEventAllListeners(Event<G, V> event, List<Listeners<G, V>> listeners) {
        listeners.forEach(lst -> sendEvent(lst, event));
    }

    @Override
    public void run() {

        List<Listeners<G, V>> list = listListeners(
                filterList(this.monitor.getListRunnable(), this));

        this.listener.preHandle(this, list);

        List<BiConsumer<StrategyListenersRunner<G, V>,
                List<Listeners<G, V>>>> functions = new ArrayList<>();
        this.listener.preStepFunctions(functions);

        for (BiConsumer<StrategyListenersRunner<G, V>,
                List<Listeners<G, V>>> function : functions) {

            try {
                function.accept(this, list);
            } catch (Exception e) {
                e.printStackTrace();
            }

            boolean isAllDone = false;
            while (!isAllDone) {
                try {
                    monitor.wait(this);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (currentEvent != null) {
                    this.receiveEvent(currentEvent);
                    this.currentEvent = null;
                } else {
                    isAllDone = true;
                }
            }
        }
    }

    /**
     * Reception of an event.
     *
     * @param event Event
     */
    private void receiveEvent(Event<G, V> event) {

        if (checkersIsCorrect(this.listener, event)) {

            List<Listeners<G, V>> list = listListeners(
                    filterList(this.monitor.getListRunnable(), this));

            switch (event.typeEvent()) {
                case MOVE:
                    this.listener.moveEvent((MoveEvent<G, V>) event, this, list);
                    break;
                case UPDATE_CHECKERS:
                    this.listener.updateCheckersEvent(
                            (UpdateCheckersEvent<G, V>) event, this, list);
                    break;
                case BLOCK:
                    this.listener.blockEvent((BlockEvent<G, V>) event, this, list);
                    break;
                case POSSIBLE_ACTION:
                    this.listener.possibleActionsEvent(
                            (PossibleActionsEvent<G, V>) event, this, list);
                    break;
                case EXISTING_GROUP:
                    this.listener.existingGroupEvent(
                            (ExistingGroupEvent<G, V>) event, this, list);
                    break;
                case REQUEST_JOIN_GROUP:
                    this.listener.requestJoinGroupEvent(
                            (RequestJoinGroupEvent<G, V>) event, this, list);
                    break;
                case REQUEST_LEAVE_GROUP:
                    this.listener.requestLeaveGroupEvent(
                            (RequestLeaveGroupEvent<G, V>) event, this, list);
                    break;
                case AUTHORIZE_VOTE_GROUP:
                    this.listener.authorizeVoteEvent(
                            (AuthorizeVoteEvent<G, V>) event, this, list);
                    break;
                case VOTE:
                    this.listener.voteEvent((VoteEvent<G, V>) event, this, list);
                    break;
                case PROTECTORS:
                    this.listener.setProtectorsEvent(
                            (ProtectorsEvent<G, V>) event, this, list);
                    break;
                default:
                    break;
            }

            event.getSource().decrementsSending();
        }
    }

    /**
     * Gets listener by with pawn.
     *
     * @param abstractPawn the abstract pawn
     * @return the listener by with pawn
     */
    public Listeners<G, V> getListenerByWithPawn(AbstractPawn abstractPawn) {
        return listListeners(this.monitor.getListRunnable()).stream()
                .filter(e -> e.getPawn() == abstractPawn)
                .findFirst().orElse(null);
    }
}