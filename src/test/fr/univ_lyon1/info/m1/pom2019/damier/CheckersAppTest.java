package fr.univ_lyon1.info.m1.pom2019.damier;

import fr.univ_lyon1.info.m1.pom2019.app.AppController;
import fr.univ_lyon1.info.m1.pom2019.app.CheckersApp;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.logger.LoggerApp;
import fr.univ_lyon1.info.m1.pom2019.view.CaseView;
import fr.univ_lyon1.info.m1.pom2019.view.PieceView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.TextFlow;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CheckersAppTest {


    private GridPane gridPane;

    private int[][] damier;

    private AppController appController;

    @Before
    public void init() {
        this.gridPane = new GridPane();
        damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
        CheckersApp.init(gridPane);
        LoggerApp.init(new TextFlow());

        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {
            for (int y = 0; y < Constants.CHECKERS_SIZE; y++) {
                damier[x][y] = 0;
            }
        }
    }

    @Test
    public void fill() {

        CheckersApp.fill(damier);

        Assert.assertEquals(CheckersApp.getCheckers().getChildren().size(),100);
    }

    @Test
    public void getCaseView() {

        Assert.assertEquals(
                CheckersApp.getCheckers().getChildren().get(9),
                CheckersApp.getCaseView(0,0));

        Assert.assertEquals(
                CheckersApp.getCheckers().getChildren().get(18),
                CheckersApp.getCaseView(1,1));
    }

    @Test
    public void echange() {

        damier[1][0] = 1;

        CheckersApp.fill(damier);
        CaseView caseViewDep = CheckersApp.getCaseView(1,0);
        Assert.assertNotNull(caseViewDep);

        PieceView pieceView = caseViewDep.getPieceView();
        Assert.assertNotNull(pieceView);

        CheckersApp.echange(1,0,0,0,Color.RED);
        CaseView caseViewArr = CheckersApp.getCaseView(0,0);
        Assert.assertNotNull(caseViewArr);

        Assert.assertEquals(pieceView,caseViewArr.getPieceView());
    }

    @Test
    public void removePiece() {

        damier[1][0] = 1;

        CheckersApp.fill(damier);

        CheckersApp.removePiece(1, 0);

        Assert.assertNotNull(CheckersApp.getCaseView(1, 0));
        Assert.assertFalse(CheckersApp.getCaseView(1, 0).containsAPiece());
    }
}