package fr.univ_lyon1.info.m1.pom2019.model.checkers;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.model.player.NaivePlayer;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ConversionBugBuilderTest {

    @Test
    public void build() {

        int[][] checkers = (new ConversionBugBuilder()).build();
        Player player1 = new NaivePlayer(checkers, true, null);
        Player player2 = new NaivePlayer(checkers, false, player1);
        player1.setOpponent(player2);

        List<Action> lActions = player1.getPieces().get(0).getDplPossible(checkers);

        Assert.assertEquals(lActions.size(), 3);
    }
}