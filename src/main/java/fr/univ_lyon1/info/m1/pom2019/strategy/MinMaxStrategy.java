package fr.univ_lyon1.info.m1.pom2019.strategy;

import fr.univ_lyon1.info.m1.pom2019.evaluation.MinMaxEvaluation;
import fr.univ_lyon1.info.m1.pom2019.evaluation.NaiveEvaluation;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.vote.MinMaxVote;


/**
 * The type Min max strategy.
 */
public class MinMaxStrategy<V extends MinMaxVote> extends GroupLimitStrategy<MinMaxEvaluation, V> {

    /**
     * Instantiates a new Min max strategy.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     * @param opponent     the opponent
     */
    public MinMaxStrategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        super(abstractPawn, player, opponent);
    }

    @Override
    protected Integer customEvaluationCalc(
            MinMaxEvaluation evaluate, int[][] board, AbstractPawn abstractPawn) {

        return evaluate.evaluate(
                board,
                abstractPawn,
                this.getGroup().getOwnerPawnList(),
                this.getOpponentPawn(),
                2,
                this.getOwnerPawn().get(0).getId() > 0
        );
    }

    @Override
    protected MinMaxEvaluation createEvaluate() {
        return new MinMaxEvaluation(
                new NaiveEvaluation()
        );
    }

    @Override
    @SuppressWarnings("unchecked")
    protected V createVote() {
        return (V) new MinMaxVote();
    }
}