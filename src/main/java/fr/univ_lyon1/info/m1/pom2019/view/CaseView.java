package fr.univ_lyon1.info.m1.pom2019.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

/**
 * CaseView.
 */
public class CaseView extends StackPane {

    private final int x;

    private final int y;

    private Color color;

    private PieceView pieceView;


    /**
     * Instantiates a new Case view.
     *
     * @param x the x
     * @param y the y
     */
    public CaseView(int x, int y) {
        this.x = x;
        this.y = y;
        this.defineColor();
        this.setAlignment(Pos.CENTER);

        CaseView self = this;

        this.widthProperty().addListener((obsValue, oldWidth, newWidth) -> {
            if (pieceView != null) {
                pieceView.resize(self);
            }
        });

        this.heightProperty().addListener((obsValue, oldHeight, newHeight) -> {
            if (pieceView != null) {
                pieceView.resize(self);
            }
        });
    }

    private void defineColor() {
        if ((this.x + this.y) % 2 == 0) {
            this.color = Color.LIGHTGRAY;
        } else {
            this.color = Color.WHITE;
        }
        this.setBackground(new Background(
                new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY))
        );
    }


    /**
     * Gets color.
     *
     * @return the color
     */
    public Color getColor() {
        return color;
    }


    /**
     * Gets x.
     *
     * @return the x
     */
    public int getX() {
        return x;
    }


    /**
     * Gets y.
     *
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * Sets piece view.
     *
     * @param pieceView the piece view
     */
    public void setPieceView(PieceView pieceView) {
        this.pieceView = pieceView;
        this.getChildren().add(pieceView.getNode());
    }

    /**
     * Remove piece view.
     */
    public void removePieceView() {
        this.pieceView = null;
        this.getChildren().remove(0);
    }

    /**
     * Contains a piece boolean.
     *
     * @return the boolean
     */
    public boolean containsAPiece() {
        return this.pieceView != null;
    }

    /**
     * Gets piece view.
     *
     * @return the piece view
     */
    public PieceView getPieceView() {
        return this.pieceView;
    }
}
