package fr.univ_lyon1.info.m1.pom2019.strategy;

import fr.univ_lyon1.info.m1.pom2019.evaluation.Evaluate;
import fr.univ_lyon1.info.m1.pom2019.model.group.ProfileGroup;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.Profile;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.evaluation.PointEvaluation;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;
import scala.Tuple2;


/**
 * The type Group limit strategy.
 */
public class ProfileStrategy<T extends Evaluate, E extends Vote> extends GroupLimitStrategy<T, E> {


    /**
     * Instantiates a new Naive 2 strategy.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     * @param opponent     the opponent
     */
    public ProfileStrategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        super(abstractPawn, player, opponent);
    }

    @Override
    protected ProfileGroup createGroup() {
        return new ProfileGroup(
                this.getOwnerPawn(),
                this.getOpponentPawn(),
                5,
                new PointEvaluation(),
                this.getCheckers(),
                15,
                new Tuple2<>(Profile.Type.Protect, 2),
                new Tuple2<>(Profile.Type.Offensive, 2),
                new Tuple2<>(Profile.Type.Risk, 1)
        );
    }
}