package fr.univ_lyon1.info.m1.pom2019.strategy;

import fr.univ_lyon1.info.m1.pom2019.event.BlockEvent;
import fr.univ_lyon1.info.m1.pom2019.event.Listeners;
import fr.univ_lyon1.info.m1.pom2019.event.RequestJoinGroupEvent;
import fr.univ_lyon1.info.m1.pom2019.globals.Tools;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.thread.StrategyListenersRunner;
import fr.univ_lyon1.info.m1.pom2019.vote.NaiveVote;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;


/**
 * The type Pawn blocked strategy.
 */
public class PawnBlockedStrategy<G extends Group> extends Naive2Strategy<G> {

    List<Listeners<G, NaiveVote>> pawnBlocked = new ArrayList<>();

    @Override
    public void preStepFunctions(
            List<BiConsumer<StrategyListenersRunner<G, NaiveVote>,
                    List<Listeners<G, NaiveVote>>>> biFunctions) {

        biFunctions.add(this::movementAnnouncementsStep);
        biFunctions.add(this::groupJoinStep);
        biFunctions.add(this::groupPawnBlockedJoinStep);
        biFunctions.add(this::existingGroupAnnouncementsStep);
        biFunctions.add(this::choiceGroupStep);
        biFunctions.add(this::naiveVoteStep);
        biFunctions.add(this::naive2MoveApplicationStep);
    }

    /**
     * Instantiates a new Pawn blocked strategy.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     */
    public PawnBlockedStrategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        super(abstractPawn, player, opponent);
    }

    /**
     * Second step: Send group requests.
     *
     * @param src  StrategyListenersRunner
     * @param list List of other listeners
     */
    protected void groupPawnBlockedJoinStep(
            StrategyListenersRunner<G, NaiveVote> src,
            List<Listeners<G, NaiveVote>> list) {

        int[][] checkers = Tools.cloneCheckers(this.getIaPlayer().getCheckers());

        if (this.getGroup() != null) {

            if (this.getGroup().getOwner() == this.getPawn()) {

                RequestJoinGroupEvent<G, NaiveVote> requestJoinGroupEvent =
                        new RequestJoinGroupEvent<>(this, checkers, this.getGroup());

                for (Listeners<G, NaiveVote> listener : this.pawnBlocked) {
                    src.sendEvent(listener, requestJoinGroupEvent);
                }

            }
        }
    }

    @Override
    public void blockEvent(BlockEvent<G, NaiveVote> event,
                           StrategyListenersRunner<G, NaiveVote> src,
                           List<Listeners<G, NaiveVote>> list) {

        pawnBlocked.add(event.getSource());
    }
}