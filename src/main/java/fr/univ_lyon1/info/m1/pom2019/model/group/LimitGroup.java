package fr.univ_lyon1.info.m1.pom2019.model.group;

import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.evaluation.Evaluate;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Comparator;


/**
 * The type Limit group.
 */
public class LimitGroup extends FilterGroup {

    /**
     * The Max size.
     */
    int maxSize;

    /**
     * The Evaluate.
     */
    Evaluate evaluate;

    /**
     * The Weights.
     */
    Map<AbstractPawn, Integer> weights;

    /**
     * The Board.
     */
    private final int[][] board;

    private final int limitRequest;

    private int currentRequest = 0;

    /**
     * Instantiates a new Limit group.
     *
     * @param ownerPawnList    the owner pawn list
     * @param opponentPawnList the opponent pawn list
     * @param maxSize          the max size
     * @param evaluate         the evaluate
     * @param board            the board
     * @param limitRequest     the limit request
     */
    public LimitGroup(List<AbstractPawn> ownerPawnList, List<AbstractPawn> opponentPawnList,
                      int maxSize, Evaluate evaluate, int[][] board, int limitRequest) {

        super(ownerPawnList, opponentPawnList);
        this.maxSize = maxSize;
        this.evaluate = evaluate;
        this.weights = new HashMap<>();
        this.board = board;
        this.limitRequest = limitRequest;
    }

    @Deprecated
    @Override
    public void addAbstractPawn(AbstractPawn abstractPawn) {
        this.addAbstractPawnWithLimit(abstractPawn);
    }

    @Deprecated
    @Override
    public void addAbstractPawnNotSynchronized(AbstractPawn abstractPawn) {
        super.addAbstractPawnNotSynchronized(abstractPawn);
    }

    /**
     * Evaluate board integer.
     *
     * @param abstractPawn the abstract pawn
     * @return the integer
     */
    protected Integer evaluateBoard(AbstractPawn abstractPawn) {
        return evaluate.evaluate(this.board, abstractPawn,
                this.getOwnerPawnList(),this.getOpponentPawnList());
    }

    /**
     * Add a pawn to the group.
     *
     * @param abstractPawn AbstractPawn
     * @return the abstract pawn that has been removed
     */
    public final AbstractPawn addAbstractPawnWithLimit(AbstractPawn abstractPawn) {

        synchronized (this) {

            if (currentRequest < limitRequest) {

                this.currentRequest++;
                this.weights.put(abstractPawn, this.evaluateBoard(abstractPawn));

                if (this.getGroup().size() < maxSize) {
                    super.addAbstractPawnNotSynchronized(abstractPawn);
                    this.callbackAddAbstractPawn(abstractPawn);
                    return null;
                } else {
                    AbstractPawn mAbstractPawn = this.chooseAPawnToDelete();

                    // mAbstractPawn cannot be null

                    if (abstractPawn == mAbstractPawn) {
                        this.weights.remove(abstractPawn);
                        return null;
                    } else {
                        this.removeAbstractPawnWithLimit(mAbstractPawn);
                        super.addAbstractPawnNotSynchronized(abstractPawn);
                        this.callbackAddAbstractPawn(abstractPawn);
                        return mAbstractPawn;
                    }
                }
            } else {
                return null;
            }
        }
    }

    /**
     * Choose a pawn to delete abstract pawn.
     *
     * @return the abstract pawn
     */
    protected AbstractPawn chooseAPawnToDelete() {
        Integer minWeight = this.weights.values().stream().min(
                Comparator.comparingInt(Integer::intValue)).orElse(0);

        return weights.entrySet().stream()
                .filter(e -> e.getValue().equals(minWeight))
                .map(Map.Entry::getKey)
                .limit(1).findFirst().orElse(null);
    }

    /**
     * Remove a pawn from the group (and choose a new owner if it was him).
     *
     * @param abstractPawn AbstractPawn
     */
    public final void removeAbstractPawnWithLimit(AbstractPawn abstractPawn) {
        if (abstractPawn != null) {
            synchronized (this) {
                this.weights.remove(abstractPawn);
                super.removeAbstractPawnNotSynchronized(abstractPawn);
                this.callbackRemoveAbstractPawn(abstractPawn);
            }
        }
    }

    @Deprecated
    @Override
    public void removeAbstractPawn(AbstractPawn abstractPawn) {
        super.removeAbstractPawn(abstractPawn);
    }

    @Deprecated
    @Override
    public void removeAbstractPawnNotSynchronized(AbstractPawn abstractPawn) {
        super.removeAbstractPawnNotSynchronized(abstractPawn);
    }

    /**
     * Callback add abstract pawn.
     *
     * @param abstractPawn the abstract pawn
     */
    protected void callbackAddAbstractPawn(AbstractPawn abstractPawn) {

    }

    /**
     * Callback remove abstract pawn.
     *
     * @param abstractPawn the abstract pawn
     */
    protected void callbackRemoveAbstractPawn(AbstractPawn abstractPawn) {

    }

    /**
     * Gets weight.
     *
     * @return the weight
     */
    public int getWeight() {
        return this.weights.values().stream().reduce(0, Integer::sum);
    }

    /**
     * Gets evaluate.
     *
     * @return the evaluate
     */
    public Evaluate getEvaluate() {
        return evaluate;
    }

    /**
     * Get board int [ ] [ ].
     *
     * @return the int [ ] [ ]
     */
    public int[][] getBoard() {
        return board;
    }
}
