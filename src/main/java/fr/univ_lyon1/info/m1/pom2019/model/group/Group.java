package fr.univ_lyon1.info.m1.pom2019.model.group;

import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


/**
 * The type Group.
 */
public class Group {

    private static final AtomicLong LAST_TIME_MS = new AtomicLong();

    private AbstractPawn owner;
    private long createdAt;
    private final List<AbstractPawn> group;


    /**
     * Instantiates a new Group.
     */
    public Group() {
        this.group = new ArrayList<>();
        this.createdAt = uniqueCurrentTime();
    }

    /**
     * Instantiates a new Group.
     *
     * @param group the group
     */
    public Group(List<AbstractPawn> group) {
        this.group = group;
    }

    /**
     * Gets group.
     *
     * @return the group
     */
    public List<AbstractPawn> getGroup() {
        return group;
    }

    /**
     * Sets owner.
     *
     * @param owner the owner
     */
    public void setOwner(AbstractPawn owner) {
        this.owner = owner;
    }

    /**
     * Gets owner.
     *
     * @return the owner
     */
    public AbstractPawn getOwner() {
        return owner;
    }

    /**
     * Add a pawn to the group. If no pawn was the owner then it becomes.
     *
     * @param abstractPawn AbstractPawn
     */
    public void addAbstractPawn(AbstractPawn abstractPawn) {
        synchronized (this) {
            this.addAbstractPawnNotSynchronized(abstractPawn);
        }
    }

    /**
     * Add a pawn to the group. If no pawn was the owner then it becomes.
     *
     * @param abstractPawn AbstractPawn
     */
    protected void addAbstractPawnNotSynchronized(AbstractPawn abstractPawn) {
        this.group.add(abstractPawn);
        if (this.owner == null) {
            this.owner = abstractPawn;
        }
    }

    /**
     * Remove a pawn from the group (and choose a new owner if it was him).
     *
     * @param abstractPawn AbstractPawn
     */
    public void removeAbstractPawn(AbstractPawn abstractPawn) {
        synchronized (this) {
            this.removeAbstractPawnNotSynchronized(abstractPawn);
        }
    }

    /**
     * Remove a pawn from the group (and choose a new owner if it was him).
     *
     * @param abstractPawn AbstractPawn
     */
    public void removeAbstractPawnNotSynchronized(AbstractPawn abstractPawn) {
        this.group.remove(abstractPawn);
        if (this.owner == abstractPawn) {
            this.owner = group.isEmpty() ? null : group.get(0);
        }
    }


    /**
     * Gets created at.
     *
     * @return the created at
     */
    public long getCreatedAt() {
        return createdAt;
    }

    /**
     * Contains boolean.
     *
     * @param abstractPawn the abstract pawn
     * @return the boolean
     */
    public boolean contains(AbstractPawn abstractPawn) {
        return this.group.stream().anyMatch(abPawn -> abPawn == abstractPawn);
    }

    /**
     * Generates a unique time.
     * @return long
     */
    private static long uniqueCurrentTime() {
        long now = System.currentTimeMillis();
        while (true) {
            long lastTime = LAST_TIME_MS.get();
            if (lastTime >= now) {
                now = lastTime + 1;
            }
            if (LAST_TIME_MS.compareAndSet(lastTime, now)) {
                return now;
            }
        }
    }
}
