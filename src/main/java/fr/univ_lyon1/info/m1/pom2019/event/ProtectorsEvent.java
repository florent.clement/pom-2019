package fr.univ_lyon1.info.m1.pom2019.event;

import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

import java.util.Set;

/**
 * Event for declaring pawn protectors.
 */
public class ProtectorsEvent<G extends Group, V extends Vote> extends Event<G, V> {

    private final Set<Coord> protectors;
    private final int[][] currentCheckers;

    /**
     * ProtectorsEvent default constructor.
     * @param src Event source.
     * @param pSet Protectors Set as Coord.
     * @param checkers current checkers board.
     */
    public ProtectorsEvent(Listeners<G, V> src, Set<Coord> pSet, int[][] checkers) {
        super(src);
        this.currentCheckers = checkers;
        this.protectors = pSet;
    }

    public Set<Coord> getProtectors() {
        return protectors;
    }

    @Override
    public int[][] getCurrentCheckers() {
        return currentCheckers;
    }

    @Override
    public Type typeEvent() {
        return Type.PROTECTORS;
    }
}
