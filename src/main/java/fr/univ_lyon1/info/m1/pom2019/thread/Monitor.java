package fr.univ_lyon1.info.m1.pom2019.thread;


import fr.univ_lyon1.info.m1.pom2019.evaluation.DepthEvaluation;
import fr.univ_lyon1.info.m1.pom2019.logger.LoggerApp;

import java.util.Map;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

/**
 * Monitor d'exécution de thread.
 *
 * @param <T> the type parameter
 */
public class Monitor<T extends Runnable> {

    private final List<T> listRunnable = new ArrayList<>();
    private final Map<T,Stack<Runnable>> notifyQueue = new HashMap<>();
    private final Map<T,Thread> association = new HashMap<>();
    private boolean enableEvent;

    /**
     * Add.
     *
     * @param thread the thread
     */
    public void add(T thread) {
        listRunnable.add(thread);
        notifyQueue.put(thread, new Stack<>());
    }

    /**
     * Add all.
     *
     * @param collection the collection
     */
    public void addAll(Collection<T> collection) {
        collection.forEach(this::add);
    }

    /**
     * Remove.
     *
     * @param runnable the runnable
     */
    public void remove(T runnable) {
        listRunnable.remove(runnable);
        notifyQueue.remove(runnable);
    }

    /**
     * Remove all.
     *
     * @param collection the collection
     */
    public void removeAll(Collection<T> collection) {
        collection.forEach(this::remove);
    }

    /**
     * Count thread int.
     *
     * @return the int
     */
    public int countThread() {
        return this.listRunnable.size();
    }

    /**
     * Notifie un thread.
     *
     * @param runnable Thread
     * @param callback the callback
     */
    public void notify(T runnable, Runnable callback) {
        synchronized (notifyQueue) {
            notifyQueue.get(runnable).add(callback);
        }
    }

    /**
     * Met en attente un thread et le renotifie s'il est redemandé.
     *
     * @param runnable runnable
     * @throws InterruptedException Erreur
     */
    public void wait(T runnable) throws InterruptedException {

        synchronized (runnable) {
            runnable.wait();
        }
    }


    private void initModule() {
        DepthEvaluation.resetPreCalc();
    }


    /**
     * Exécute une liste de runnable.
     *
     * @throws InterruptedException Erreur d'exécution
     */
    public void run() throws InterruptedException {

        this.initModule();
        this.enableEvent = true;
        List<Thread> list = new ArrayList<>();
        for (T runnable : listRunnable) {
            Thread thread = new Thread(runnable);
            association.put(runnable, thread);
            list.add(thread);
        }

        // lancement du notifier
        Thread notifier = new Thread(() -> {
            while (this.enableEvent) {
                boolean wait = true;
                for (int i = 0; i < listRunnable.size(); i++) {
                    T listener = listRunnable.get(i);
                    final Thread thread = list.get(i);
                    if (thread.getState().equals(Thread.State.WAITING)) {
                        if (!notifyQueue.get(listener).isEmpty()) {
                            wait = false;
                            Runnable callback = notifyQueue.get(listener).pop();
                            if (callback != null) {
                                callback.run();
                            }
                            synchronized (listener) {
                                listener.notify();
                            }
                        }
                    } else {
                        wait = false;
                    }
                }
                if (wait) {
                    for (T listener : listRunnable) {
                        synchronized (listener) {
                            listener.notify();
                        }
                    }
                }

            }
        });

        for (Thread thread : list) {
            thread.start();
        }

        notifier.start();

        try {
            for (Thread thread : list) {
                thread.join();
            }
        } catch (InterruptedException ex) {
            LoggerApp.warn("Le jeu a bien été arrêté");
        }

        this.enableEvent = false;
    }

    /**
     * Gets list runnable.
     *
     * @return the list runnable
     */
    public List<T> getListRunnable() {
        return listRunnable;
    }
}