package fr.univ_lyon1.info.m1.pom2019.globals;

public class Constants {
    public static int GROUP_SIZE = 4;
    public static int CHECKERS_SIZE = 10;
    public static int MAX_MOVE = 2;
    public static int LOCATION_RADIUS = 2;
    public static final String LEARNING_DIRECTORY = "./learning";
    public static final int MINIMUM_GRADE = 5;
}
