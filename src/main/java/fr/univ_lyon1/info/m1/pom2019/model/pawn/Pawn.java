package fr.univ_lyon1.info.m1.pom2019.model.pawn;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.globals.Tools;
import fr.univ_lyon1.info.m1.pom2019.evaluation.PointEvaluation;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Basic Pawn class.
 */
public class Pawn extends AbstractPawn implements ConvertAble {

    /**
     * Instantiates a new Pawn.
     *
     * @param pos the pos
     * @param id  the id
     */
    public Pawn(Coord pos, int id) {
        super(pos, id);
    }

    private Action move(int[][] board, Pawn pawn, int x, int y) {
        Action res = null;
        boolean white = pawn.getId() > 0;

        if (x < Constants.CHECKERS_SIZE && y < Constants.CHECKERS_SIZE
                && x >= 0 && y >= 0 && board[x][y] == 0) {

            res = new Action(pawn);
            res.getMovements().add(new Coord(x, y));
            res.setStartingPos(pawn.getPosition().clone());

            if ((white && y == Constants.CHECKERS_SIZE - 1) || (!white && y == 0)) {
                res.setConvert(true);
                res.setConvertClass(Dame.class);
            }
        }

        return res;
    }

    private List<Action> capture(int[][] board, int moveX, int moveY,
                                 int captureX, int captureY, Action prevAction) {
        List<Action> res = new LinkedList<>();
        boolean white = this.getId() > 0;

        if (moveX < Constants.CHECKERS_SIZE && moveY < Constants.CHECKERS_SIZE
                && moveX >= 0 && moveY >= 0 && board[moveX][moveY] == 0) {

            if (AbstractPawn.canEat(board, captureX, captureY, white)) {

                Action action = AbstractPawn.eat(this,
                        prevAction, moveX, moveY, captureX, captureY);
                res.add(action);

                if ((white && moveY == Constants.CHECKERS_SIZE - 1) || (!white && moveY == 0)) {
                    Action converted = action.clone();
                    converted.setConvert(true);
                    converted.setConvertClass(Dame.class);
                    res.add(converted);
                    System.out.println("Possible Conversion after capture at: "
                            + converted.getLastMovements());
                }
            }
        }

        return res;
    }

    /**
     * Breathe First search of possible actions from current Pawn Coord.
     *
     * @return List of actions.
     */
    @Override
    public List<Action> getDplPossible(int[][] board) {
        LinkedList<Action> possible = new LinkedList<>();
        LinkedList<Action> visited = new LinkedList<>();
        int x = this.getPosition().getX();
        int y = this.getPosition().getY();
        final int vertical = y + this.getId();

        Action moveLeft = move(board, this, x - 1, vertical);
        if (moveLeft != null) {
            possible.add(moveLeft);
        }

        Action moveRight = move(board, this, x + 1, vertical);
        if (moveRight != null) {
            possible.add(moveRight);
        }

        Action prevAction = null;
        boolean hasAny = true;
        int[][] newBoard;
        while (hasAny) {

            if (prevAction != null) {
                newBoard = Tools.applyActionOnTempBoard(board, prevAction);
            } else {
                newBoard = board;
            }

            List<Action> captures =
                    this.capture(newBoard, x - 2, y + 2, x - 1, y + 1, prevAction);
            captures.addAll(this.capture(newBoard, x + 2, y + 2, x + 1, y + 1, prevAction));
            captures.addAll(this.capture(newBoard, x - 2, y - 2, x - 1, y - 1, prevAction));
            captures.addAll(this.capture(newBoard, x + 2, y - 2, x + 1, y - 1, prevAction));

            possible.addAll(captures);
            visited.addAll(captures);

            if (!visited.isEmpty()) {
                prevAction = visited.getFirst();
                visited.removeFirst();

                while (prevAction.isConvert() && !visited.isEmpty()) {
                    prevAction = visited.getFirst();
                    visited.removeFirst();
                }

                if (!prevAction.isConvert()) {
                    Coord update = prevAction.getMovements().get(
                            prevAction.getMovements().size() - 1);
                    x = update.getX();
                    y = update.getY();
                } else {
                    hasAny = false;
                }
            } else {
                hasAny = false;
            }
        }

        return possible.stream().filter(e -> possible.stream().noneMatch(
            f -> f.isConvert() && e.getLastMovements().equals(f.getLastMovements())
                        && e != f
                )
        ).collect(Collectors.toList());
    }

    @Override
    public List<Coord> getBlockCoord(int[][] board) {
        List<Coord> res = new LinkedList<>();
        final int vertical = this.getPosition().getY() + this.getId();
        final int left = this.getPosition().getX() - 1;
        final int right = this.getPosition().getX() + 1;
        boolean white = this.getId() > 0;

        if (AbstractPawn.isBlockedByTeam(board, left, vertical, white)) {
            res.add(new Coord(left, vertical));
        }

        if (AbstractPawn.isBlockedByTeam(board, right, vertical, white)) {
            res.add(new Coord(right, vertical));
        }

        return res;
    }

    @Override
    public int typePawnId() {
        return 1;
    }

    @Override
    public List<Class<? extends AbstractPawn>> getConversionList() {
        return Collections.singletonList(
                Dame.class
        );
    }

    @Override
    public boolean isConvertible(
            Class<? extends AbstractPawn> classConversion, int[][] board, Coord coord) {

        if (classConversion == Dame.class) {
            boolean white = this.getId() > 0;
            int y = coord.getY();
            return (white && y == Constants.CHECKERS_SIZE - 1) || (!white && y == 0);
        }

        return false;
    }

    @Override
    public AbstractPawn convert(Class<? extends AbstractPawn> classConversion) {
        if (classConversion == Dame.class) {
            return PionBuilder.build(this.getId() * 2, this.getPosition());
        }

        return null;
    }

    @Override
    public Type getProfile(PointEvaluation pointEvaluation, int[][] board,
                           List<AbstractPawn> owner, List<AbstractPawn> opponent) {
        return pointEvaluation.determineProfile(this, board, owner, opponent);
    }
}
