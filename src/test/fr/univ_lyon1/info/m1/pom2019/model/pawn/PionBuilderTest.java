package fr.univ_lyon1.info.m1.pom2019.model.pawn;

import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import org.junit.Assert;
import org.junit.Test;

public class PionBuilderTest {

    @Test
    public void build() {

        int[][] matrice = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];

        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {
            for (int y = 0; y < Constants.CHECKERS_SIZE; y++) {
                matrice[x][y] = 0;
            }
        }

        matrice[0][0] = 1;
        AbstractPawn abstractPawn = PionBuilder.build(1, new Coord(0,0));

        Assert.assertNotNull(abstractPawn);
        Assert.assertTrue(abstractPawn instanceof Pawn);
    }
}