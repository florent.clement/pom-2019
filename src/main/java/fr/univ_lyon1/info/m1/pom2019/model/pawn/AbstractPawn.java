package fr.univ_lyon1.info.m1.pom2019.model.pawn;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Abstract class for game pawns.
 */
public abstract class AbstractPawn implements Profile, Cloneable {

    private Coord position;
    private static int counter;
    private final int count;

    /**
     * 1 for white and -1 for black.
     */
    private int id;

    /**
     * Instantiates a new Abstract pawn.
     *
     * @param pos the pos
     * @param id  the id
     */
    AbstractPawn(Coord pos, int id) {
        this.position = pos;
        this.id = id;
        ++counter;
        this.count = counter;
    }

    /**
     * Gets position.
     *
     * @return the position
     */
    public Coord getPosition() {
        return position;
    }

    /**
     * Sets position.
     *
     * @param position the position
     */
    public void setPosition(Coord position) {
        this.position = position;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Can eat boolean.
     *
     * @param board the board
     * @param x     the x
     * @param y     the y
     * @param color the color
     * @return the boolean
     */
    protected static boolean canEat(int[][] board, int x, int y, boolean color) {
        return (x < Constants.CHECKERS_SIZE && y < Constants.CHECKERS_SIZE
                && x >= 0 && y >= 0 && board[x][y] != 0
                && ((color && board[x][y] + 1 <= 0)
                || (!color && board[x][y] - 1 >= 0)));
    }

    /**
     * Eat action.
     *
     * @param abstractPawn the abstract pawn
     * @param prevAction   the prev action
     * @param moveX        the move x
     * @param moveY        the move y
     * @param captureX     the capture x
     * @param captureY     the capture y
     * @return the action
     */
    protected static Action eat(AbstractPawn abstractPawn, Action prevAction,
                                int moveX, int moveY, int captureX, int captureY) {
        Action action;
        if (prevAction != null) {
            action = prevAction.clone();
        } else {
            action = new Action(abstractPawn);
            action.setStartingPos(abstractPawn.getPosition().clone());
        }

        action.getMovements().add(new Coord(moveX, moveY));
        action.getCapturedPieces().add(new Coord(captureX, captureY));
        return action;
    }

    /**
     * Is blocked by team boolean.
     *
     * @param board the board
     * @param x     the x
     * @param y     the y
     * @param color the color
     * @return the boolean
     */
    protected static boolean isBlockedByTeam(int[][] board, int x, int y, boolean color) {
        return (x < Constants.CHECKERS_SIZE && y < Constants.CHECKERS_SIZE
                && x >= 0 && y >= 0 && ((color && board[x][y] > 0)
                || (!color && board[x][y] < 0)));
    }

    /**
     * Get possible actions based on an alternative board.
     *
     * @param altCheckers alternative board.
     * @return possible actions.
     */
    public abstract List<Action> getDplPossible(int[][] altCheckers);

    /**
     * Returns blocking positions.
     *
     * @param altCheckers alternative board
     * @return blocking positions list
     */
    public abstract List<Coord> getBlockCoord(int[][] altCheckers);

    /**
     * Returns same team pawns blocking this pawn capture.
     * @param altChekers alternative board.
     * @return List of Coordinates of protector pawns
     */
    public Set<Coord> getProtectors(int[][] altChekers) {
        Set<Coord> protectors = new HashSet<>();
        boolean white = this.id > 0;
        int startX = this.getPosition().getX();
        int startY = this.getPosition().getY();

        for (int vecX = -1; vecX <= 1; vecX += 2) {
            for (int vecY = -1; vecY <= 1; vecY += 2) {

                int x = startX + vecX;
                int y = startY + vecY;
                int gap = 1;

                while (x < Constants.CHECKERS_SIZE && y < Constants.CHECKERS_SIZE
                        && x >= 0 && y >= 0 && altChekers[x][y] == 0) {
                    x += vecX;
                    y += vecY;
                    ++gap;
                }

                if (x < Constants.CHECKERS_SIZE
                        && y < Constants.CHECKERS_SIZE && x >= 0 && y >= 0) {

                    boolean enemyPawn = ((white && altChekers[x][y] == -1)
                            || (!white && altChekers[x][y] == 1));

                    boolean enemyQueen = ((white && altChekers[x][y] == -2)
                            || (!white && altChekers[x][y] == 2));

                    boolean pawnCapture = enemyPawn && gap == 1;

                    boolean queenCapture = enemyQueen && gap >= 1;

                    if (pawnCapture || queenCapture) {

                        int oppositeX = startX - vecX;
                        int oppositeY = startY - vecY;

                        boolean oppositeInBoard = oppositeX >= 0
                                && oppositeX < Constants.CHECKERS_SIZE
                                && oppositeY >= 0 && oppositeY < Constants.CHECKERS_SIZE;

                        boolean protector = oppositeInBoard
                                && ((white && altChekers[oppositeX][oppositeY] > 0)
                                || (!white && altChekers[oppositeX][oppositeY] < 0));

                        if (protector) {
                            protectors.add(new Coord(oppositeX, oppositeY));
                        }
                    }
                }
            }
        }

        return protectors;
    }

    /**
     * Id Type of the pawn for example 1 classic pawn and 2 for queen.
     *
     * @return pawn id
     */
    public abstract int typePawnId();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractPawn)) {
            return false;
        }
        AbstractPawn that = (AbstractPawn) o;
        return count == that.count
                && getId() == that.getId()
                && getPosition().equals(that.getPosition());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getPosition(), count, getId());
        return 31 * result;
    }

    @Override
    public AbstractPawn clone() {
        AbstractPawn clone = null;
        try {
            clone = (AbstractPawn) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

}
