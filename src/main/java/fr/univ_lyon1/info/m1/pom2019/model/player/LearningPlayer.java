package fr.univ_lyon1.info.m1.pom2019.model.player;

import fr.univ_lyon1.info.m1.pom2019.learning.Location;
import fr.univ_lyon1.info.m1.pom2019.learning.LocationPersistence;
import fr.univ_lyon1.info.m1.pom2019.learning.LocationSetBuilder;

import java.util.HashSet;
import java.util.Set;

/**
 * Learning type Players abstract Class.
 */
public abstract class LearningPlayer extends IaPlayer {

    protected Set<Location> locationSet = null;
    protected boolean persistence = false;

    public LearningPlayer(int[][] checkers, boolean whiteColor, Player opponent) {
        super(checkers, whiteColor, opponent);
    }

    /**
     * Memory initialisation method.
     * Call to LocationSetBuilder whit corresponding strategyId.
     */
    public void initMemory() {
        if (locationSet == null) {
            locationSet = LocationSetBuilder.buildWithStrategy(getStrategyId());
            if (locationSet == null) {
                System.out.println("LearningPlayer: Error building locationSet");
                locationSet = new HashSet<>();
            } else {
                System.out.println("LearningPlayer: Success building locationSet");
            }
        } else {
            System.out.println("LearningPlayer: locationSet already built");
        }
    }

    /**
     * Memory persistence method.
     * Call to LocationPersistence with corresponding locationSet.
     * @return true if save successful false otherwise.
     */
    public boolean saveMemory() {
        if (persistence) {
            persistence = false;
            if (getMemory() != null) {
                if (!getMemory().isEmpty()) {
                    boolean res = LocationPersistence.saveLocation(locationSet);
                    if (res) {
                        System.out.println("LearningPlayer: Successfully saved learning memory");
                    } else {
                        System.out.println("LearningPlayer: Error saving learning memory");
                    }
                    return res;
                } else {
                    System.out.println("LearningPlayer: No memory to save");
                }
            } else {
                System.out.println("LearningPlayer: Error Null memory to save");
            }
        }
        return false;
    }

    public void setPersistence(boolean value) {
        persistence = value;
    }

    public Set<Location> getMemory() {
        return locationSet;
    }

    public abstract int getStrategyId();
}
