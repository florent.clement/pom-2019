package fr.univ_lyon1.info.m1.pom2019.event;


import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

/**
 * Class MoveEvent.
 */
public class BlockEvent<G extends Group, V extends Vote> extends Event<G, V> {

    private final int[][] currentCheckers;

    /**
     * Blocking event constructor.
     *
     * @param src             Source of the event
     * @param currentCheckers Checkers source
     */
    public BlockEvent(Listeners<G, V> src, int[][] currentCheckers) {
        super(src);
        this.currentCheckers = currentCheckers;
    }

    @Override
    public Type typeEvent() {
        return Type.BLOCK;
    }

    @Override
    public int[][] getCurrentCheckers() {
        return this.currentCheckers;
    }
}