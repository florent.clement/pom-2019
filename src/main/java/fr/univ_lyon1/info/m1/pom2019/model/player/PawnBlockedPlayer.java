package fr.univ_lyon1.info.m1.pom2019.model.player;

import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.strategy.AbstractStrategy;
import fr.univ_lyon1.info.m1.pom2019.strategy.PawnBlockedStrategy;


/**
 * The type Pawn blocked player.
 */
public class PawnBlockedPlayer extends IaPlayer {


    /**
     * Instantiates a new Pawn blocked player.
     *
     * @param checkers   the checkers
     * @param whiteColor the white color
     * @param opponent   the opponent
     */
    public PawnBlockedPlayer(int[][] checkers, boolean whiteColor, Player opponent) {
        super(checkers,whiteColor, opponent);
    }

    @Override
    public AbstractStrategy<?, ?> getStrategy(
            AbstractPawn abstractPawn,
            Player player, Player opponent) {
        return new PawnBlockedStrategy<>(
                abstractPawn,
                player,
                opponent
        );
    }

    @Override
    public String getNameIa() {
        return "Naive Player with pawn blocked optimization";
    }
}
