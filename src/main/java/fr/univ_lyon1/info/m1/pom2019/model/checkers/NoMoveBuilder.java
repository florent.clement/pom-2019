package fr.univ_lyon1.info.m1.pom2019.model.checkers;

/**
 * Pawn layout generator.
 */
public class NoMoveBuilder extends AbstractMatrixBuilder {

    @Override
    public int[][] build() {
        int[][] result = this.initCheckers(0);

        result[1][9] = -1;
        result[0][8] = 1;

        return result;
    }

    @Override
    public String getName() {
        return "No movement possible";
    }
}
