package fr.univ_lyon1.info.m1.pom2019.view;

import org.junit.Assert;
import org.junit.Test;

public class PieceViewTest {

    @Test
    public void getSubClass() {
        Assert.assertNotEquals(PieceView.getSubClass().size(), 0);
    }

    @Test
    public void mapTypePawn() {
        Assert.assertNotEquals(PieceView.mapTypePawn().size(), 0);
    }
}