package fr.univ_lyon1.info.m1.pom2019.event;

import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

/**
 * Event to flag Listeners with learning states.
 * @param <G> Group class.
 * @param <V> Vote class.
 */
public class LearningFlagEvent<G extends Group, V extends Vote> extends Event<G, V> {

    private final int[][] currentCheckers;
    private final int flag;

    /**
     * Learning flag event default constructor.
     * @param src Event source.
     * @param flag List of flags assigned to Listener.
     * @param checkers current checkers board.
     */
    public LearningFlagEvent(Listeners<G, V> src,
                             int flag,
                             int[][] checkers) {
        super(src);
        this.currentCheckers = checkers;
        this.flag = flag;
    }

    @Override
    public Type typeEvent() {
        return Type.LEARNING_FLAG;
    }

    @Override
    public int[][] getCurrentCheckers() {
        return currentCheckers;
    }

    public int getFlag() {
        return flag;
    }
}
