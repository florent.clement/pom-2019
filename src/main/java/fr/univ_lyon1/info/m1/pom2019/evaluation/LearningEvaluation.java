package fr.univ_lyon1.info.m1.pom2019.evaluation;

import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.globals.Tools;
import fr.univ_lyon1.info.m1.pom2019.learning.Location;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;

import java.util.List;
import java.util.Set;

/**
 * Evaluator implementing Learning module.
 */
public class LearningEvaluation implements Evaluate {

    private final Set<Location> locationSet;

    private final Evaluate evaluator;

    private final int strategyId;

    /**
     * Default constructor for learning evaluator.
     * @param evaluator evaluator to grade locations.
     * @param locations locationSet from Player.
     * @param strategyId strategyId to categorize locations.
     */
    public LearningEvaluation(Evaluate evaluator, Set<Location> locations, int strategyId) {
        this.evaluator = evaluator;
        locationSet = locations;
        this.strategyId = strategyId;
    }

    public Set<Location> getLocationSet() {
        return locationSet;
    }

    public Evaluate getEvaluator() {
        return evaluator;
    }

    public int getStrategyId() {
        return strategyId;
    }

    /**
     * Synchronized function to addLocations to locationSet.
     * @param location location to add.
     */
    public void addLocation(Location location) {
        synchronized (locationSet) {
            try {
                locationSet.add(location);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Integer evaluate(int[][] board, AbstractPawn abstractPawn,
                            List<AbstractPawn> owner, List<AbstractPawn> opponent) {

        int evaluation = getEvaluator().evaluate(board, abstractPawn, owner, opponent);

        if (getLocationSet() != null) {

            Location location = new Location(getStrategyId(), abstractPawn.getPosition(),
                    abstractPawn.getId(), evaluation);
            location.generateSurroundings(board);

            boolean isPawn = abstractPawn.typePawnId() == 1;

            if (getLocationSet().contains(location)) {
                String sid = isPawn ? "Pawn" : "Queen";
                System.out.println("LearningEvaluation: " + sid + " In Location At"
                        + location.getOriginCoord());
                evaluation += 5;
            } else {
                if (evaluation < Constants.MINIMUM_GRADE) {
                    synchronized (locationSet) {
                        try {
                            if (isPawn) {
                                int x = abstractPawn.getPosition().getX();
                                int y = abstractPawn.getPosition().getY();
                                if (getLocationSet().stream().anyMatch(loc ->
                                        (Math.abs(loc.getOriginCoord().getX() - x) == 1)
                                                && (loc.getOriginCoord().getY() - y == 1)
                                                && loc.getOriginId() == abstractPawn.getId())) {
                                    evaluation += 2;
                                    System.out.println(
                                            "LearningEvaluation: Pawn Near Location At"
                                                    + location.getOriginCoord());
                                }
                            } else {
                                if (getLocationSet().stream().anyMatch(loc ->
                                        Tools.ratio(loc.getOriginCoord(),
                                                abstractPawn.getPosition())
                                                && loc.getOriginId() == abstractPawn.getId())) {
                                    evaluation += 2;
                                    System.out.println(
                                            "LearningEvaluation: Queen Near Location At"
                                                    + location.getOriginCoord());
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    System.out.println("LearningEvaluation: Adding Location At "
                            + location.getOriginCoord());
                    addLocation(location);
                }
            }
        } else {
            System.out.println("LearningEvaluation: Null LocationSet");
        }

        return evaluation;
    }
}
