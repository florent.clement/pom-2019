package fr.univ_lyon1.info.m1.pom2019.view;

import fr.univ_lyon1.info.m1.pom2019.logger.LoggerApp;
import javafx.scene.Node;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Interface for generic pawn rendering.
 */
public abstract class PieceView {

    /**
     * The Color.
     */
    protected int color;
    /**
     * The Parent.
     */
    protected CaseView parent;

    /**
     * Instantiates a new Piece view.
     *
     * @param color  the color
     * @param parent the parent
     */
    public PieceView(int color, CaseView parent) {
        this.parent = parent;
        this.color = color;
    }

    /**
     * GET VIEW NODE.
     *
     * @return the graphic node of the object implementing PieceDisplay.
     */
    abstract Node getNode();

    /**
     * Parent size update.
     *
     * @param parent Parent
     */
    abstract void resize(CaseView parent);


    /**
     * Returns the class of objects inheriting from PieceView.
     *
     * @return List de class
     */
    public static Set<Class<? extends PieceView>> getSubClass() {
        Reflections reflections = new Reflections("fr.univ_lyon1.info.m1.pom2019.view");

        return reflections.getSubTypesOf(PieceView.class);
    }

    /**
     * Returns the id of each type of pawn.
     *
     * @return Map map
     */
    public static Map<Integer, Class<? extends PieceView>> mapTypePawn() {

        Map<Integer, Class<? extends PieceView>> result = new HashMap<>();

        for (Class<? extends PieceView> classPieceView : getSubClass()) {
            try {

                PieceView pieceView =
                        classPieceView.getConstructor(int.class, CaseView.class)
                                .newInstance(0, new CaseView(0,0));

                result.put(pieceView.getId(),classPieceView);
            } catch (Exception e) {
                LoggerApp.error(e.getMessage());
            }
        }
        return result;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public abstract int getId();
}
