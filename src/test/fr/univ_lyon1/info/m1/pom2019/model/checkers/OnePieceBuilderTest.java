package fr.univ_lyon1.info.m1.pom2019.model.checkers;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import org.junit.Assert;
import org.junit.Test;

public class OnePieceBuilderTest {

    @Test
    public void build() {
        // On vérifie qu'il y a au moins un pion de chaque

        OnePieceBuilder onePieceBuilder = new OnePieceBuilder();
        int[][] matrice = onePieceBuilder.build();

        int pionBlanc = 0, pionNoir = 0;

        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {
            for (int y = 0; y < Constants.CHECKERS_SIZE; y++) {
                int id = matrice[x][y];

                if (id > 0) {
                    pionBlanc ++;
                } else if (id < 0) {
                    pionNoir ++;
                }
            }
        }

        Assert.assertEquals(pionBlanc,1);
        Assert.assertEquals(pionNoir,1);
    }

    @Test
    public void getName() {

        OnePieceBuilder onePieceBuilder = new OnePieceBuilder();

        Assert.assertNotNull(onePieceBuilder.getName());
    }
}