package fr.univ_lyon1.info.m1.pom2019.model.pawn;

import fr.univ_lyon1.info.m1.pom2019.evaluation.PointEvaluation;

import java.util.List;

/**
 * The interface Profile.
 */
public interface Profile {

    /**
     * Gets profile.
     *
     * @param pointEvaluation the point evaluation
     * @param board           the board
     * @param owner           the owner
     * @param opponent        the opponent
     * @return the profile
     */
    Type getProfile(PointEvaluation pointEvaluation, int[][] board,
                           List<AbstractPawn> owner, List<AbstractPawn> opponent);

    /**
     * The enum Type.
     */
    enum Type {
        /**
         * Protect type.
         */
        Protect,
        /**
         * Offensive type.
         */
        Offensive,
        /**
         * Risk type.
         */
        Risk,
        /**
         * Conversion type.
         */
        Conversion,
        /**
         * Control type.
         */
        Control,
    }
}
