package fr.univ_lyon1.info.m1.pom2019.learning;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import org.junit.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.regex.Pattern;

public class LocationSetBuilderTest {

    private int count = 0;

    private void addToCount() {
        ++count;
    }

    @Test
    public void buildFromString() {
        Path directory = Paths.get(Constants.LEARNING_DIRECTORY);
        Pattern pattern =
                Pattern.compile("^.*0_\\d-\\d_(-2|-1|1|2)_\\d{1,2}_\\d{1,2}\\.txt$");

        try {
            Files.newDirectoryStream(directory, path -> path.toFile().isFile()
                    && pattern.matcher(path.toString()).matches())
                    .forEach(path -> {
                            try {
                                if (Files.size(path) > 0L) {
                                    addToCount();
                                }
                            }catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    );
        } catch (IOException e) {
            e.printStackTrace();
        }

        Set<Location> locations = LocationSetBuilder.buildWithStrategy(0);
        Assert.assertNotNull(locations);
        Assert.assertEquals(count, locations.size());
        for (Location location : locations) {
            Assert.assertNotNull(location);
            Assert.assertFalse(location.getSurroundings().isEmpty());
        }
    }

    @AfterClass
    public static void afterClass() throws Exception {
        Path directory = Paths.get(Constants.LEARNING_DIRECTORY);
        if (Files.exists(directory) && Files.isDirectory(directory)
                && Files.isReadable(directory)) {

            Pattern pattern =
                    Pattern.compile("^.*0_\\d-\\d_(-2|-1|1|2)_\\d{1,2}_\\d{1,2}\\.txt$");

            try {

                Files.newDirectoryStream(directory, path -> path.toFile().isFile()
                        && pattern.matcher(path.toString()).matches())
                        .forEach(path -> {
                                    try {
                                        System.out.println("cleaning learning tests");
                                        Files.delete(path);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                        );

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}