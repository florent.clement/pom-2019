package fr.univ_lyon1.info.m1.pom2019.model.group;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;

import java.util.LinkedList;
import java.util.List;


/**
 * The type Group builder.
 */
public class GroupBuilder {

    /**
     * Combinatorial algorithm using iterator to navigate through List of AbstractPawn.
     *
     * @param res             resulting group combinations.
     * @param temp            temporary set to store a possible combination.
     * @param abstractPawnSet List of AbstractPawn.
     * @param iterator        Iterator used to navigate through List.
     */
    public static void makeCombination(List<Group> res, List<AbstractPawn> temp,
                                       List<AbstractPawn> abstractPawnSet, int iterator) {
        for (int i = 1; i < Constants.GROUP_SIZE; ++i) {
            if (temp.size() == i) {
                res.add(new Group(new LinkedList<>(temp)));
            }
        }
        if (temp.size() == Constants.GROUP_SIZE) {
            res.add(new Group(new LinkedList<>(temp)));
            return;
        }
        for (int i = iterator; i < abstractPawnSet.size(); ++i) {
            temp.add(abstractPawnSet.get(i));
            makeCombination(res, temp, abstractPawnSet, i + 1);
            temp.remove(abstractPawnSet.get(i));
        }
    }

    /**
     * Combinatorial build of List of Groups containing MAX_GROUP_SIZE AbstractPawn or less.
     *
     * @param pieces List of AbstractPawn corresponding to Player's Pawns at call.
     * @return List of possible Groups of Pawns.
     */
    public static List<Group> build(List<AbstractPawn> pieces) {
        List<Group> res = new LinkedList<>();
        List<AbstractPawn> temp = new LinkedList<>();
        makeCombination(res, temp, pieces, 0);
        return res;
    }
}
