package fr.univ_lyon1.info.m1.pom2019.model.player;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.ConvertAble;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.PionBuilder;
import fr.univ_lyon1.info.m1.pom2019.strategy.AbstractStrategy;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


/**
 * The type Player.
 */
public abstract class Player {

    private List<AbstractPawn> pieces;
    private final int[][] checkers;
    private final boolean whiteColor;
    private Player opponent;


    /**
     * Instantiates a new Player.
     *
     * @param checkers   the checkers
     * @param whiteColor the white color
     * @param opponent   the opponent
     */
    public Player(int[][] checkers, boolean whiteColor, Player opponent) {
        this.checkers = checkers;
        this.whiteColor = whiteColor;
        this.opponent = opponent;
        if (opponent != null) {
            this.pieces = loadPawns(checkers, whiteColor);
            this.handlePieceInit(pieces);
        }
    }

    /**
     * Do action list.
     *
     * @return the list
     * @throws ExecutionException   the execution exception
     * @throws InterruptedException the interrupted exception
     */
    public abstract List<Action> doAction() throws ExecutionException, InterruptedException;

    /**
     * Remove a pawn if it belongs to the list.
     *
     * @param list Pawn list to delete
     */
    public abstract void removePawn(List<Coord> list);

    /**
     * Get checkers int [ ] [ ].
     *
     * @return the int [ ] [ ]
     */
    public int[][] getCheckers() {
        return checkers;
    }

    /**
     * Gets pieces.
     *
     * @return the pieces
     */
    public List<AbstractPawn> getPieces() {
        return pieces;
    }

    /**
     * Method for changing an (Pawn)AbstractionPawn to Dame in pieces List.
     *
     * @param action for converting Pawn.
     * @return new Dame piece if change successful null otherwise.
     */
    public AbstractPawn convert(Action action) {
        if (action.isConvert() && action.getPiece() instanceof ConvertAble) {
            ConvertAble convertAble = (ConvertAble) action.getPiece();
            Class<? extends AbstractPawn> convertClass = action.getConvertClass();
            AbstractPawn converted = convertAble.convert(convertClass);
            if (converted != null) {
                this.pieces.remove(action.getPiece());
                this.pieces.add(converted);
                return converted;
            }
        }
        return null;
    }

    /**
     * Load pawns list.
     *
     * @param board      the board
     * @param whiteColor the white color
     * @return the list
     */
    public static List<AbstractPawn> loadPawns(int[][] board, boolean whiteColor) {
        List<AbstractPawn> pawns = new ArrayList<>();

        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {
            for (int y = 0; y < Constants.CHECKERS_SIZE; y++) {

                int id = board[x][y];
                if ((whiteColor && id > 0) || (!whiteColor && id < 0)) {

                    AbstractPawn abstractPawn = PionBuilder.build(id, new Coord(x,y));
                    pawns.add(abstractPawn);
                }
            }
        }
        return pawns;
    }

    /**
     * Handle piece init.
     *
     * @param pieces the pieces
     */
    public abstract void handlePieceInit(List<AbstractPawn> pieces);


    /**
     * Can play boolean.
     *
     * @return the boolean
     */
    public boolean canPlay() {
        return this.pieces.size() > 0 && canMove();
    }

    /**
     * Can move boolean.
     *
     * @return the boolean
     */
    abstract boolean canMove();

    /**
     * Gets strategy.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     * @param opponent     the opponent
     * @return the strategy
     */
    public abstract AbstractStrategy<?, ?> getStrategy(AbstractPawn abstractPawn,
                                                       Player player, Player opponent);

    /**
     * Gets opponent.
     *
     * @return the opponent
     */
    public Player getOpponent() {
        return opponent;
    }

    /**
     * Sets opponent.
     *
     * @param opponent the opponent
     */
    public void setOpponent(Player opponent) {
        this.opponent = opponent;
        this.pieces = loadPawns(checkers, whiteColor);
    }
}
