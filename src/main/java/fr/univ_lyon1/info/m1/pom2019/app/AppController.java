package fr.univ_lyon1.info.m1.pom2019.app;

import fr.univ_lyon1.info.m1.pom2019.logger.LoggerApp;
import fr.univ_lyon1.info.m1.pom2019.model.checkers.AbstractMatrixBuilder;
import fr.univ_lyon1.info.m1.pom2019.model.checkers.Checkers;
import fr.univ_lyon1.info.m1.pom2019.model.player.IaPlayer;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.TextFlow;
import org.datafx.controller.FXMLController;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * Console tab controller".
 */
@FXMLController(value = "/ui.fxml")
public class AppController {

    private final Checkers checkers;
    @FXML
    private ChoiceBox<String> choiceBox;
    @FXML
    private TextFlow loggerText;
    @FXML
    private GridPane gridPane;
    @FXML
    private ChoiceBox<String> choiceIaPlayer1;
    @FXML
    private ChoiceBox<String> choiceIaPlayer2;
    @FXML
    private StackPane consoleBackground;
    @FXML
    private Button buttonStart;
    @FXML
    private Button buttonStop;
    private Map<String, Class<? extends AbstractMatrixBuilder>> builderMap;
    private Map<String, Class<? extends IaPlayer>> iaMap;

    /**
     * Instantiates a new App controller.
     */
    public AppController() {
        this.checkers = new Checkers();
    }

    /**
     * After construction of the controller.
     */
    @PostConstruct
    public void init() {
        LoggerApp.init(loggerText);
        CheckersApp.init(gridPane);
        consoleBackground.setBackground(new Background(
                new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        this.initChoiceBox();
        this.initEvent();
        LoggerApp.success("finished loading");
    }

    /**
     * Initialization of the "pawn placement list" at the start of the game.
     */
    private void initChoiceBox() {

        this.builderMap = AbstractMatrixBuilder.mapTypeBuilder();

        for (String string : this.builderMap.keySet()) {
            choiceBox.getItems().add(string);
        }

        this.iaMap = IaPlayer.mapTypePlayer();

        for (String string : this.iaMap.keySet()) {
            choiceIaPlayer1.getItems().add(string);
            choiceIaPlayer2.getItems().add(string);
        }
    }

    private void initCheckers(AbstractMatrixBuilder abstractMatrixBuilder,
                              Class<? extends Player> player1,
                              Class<? extends Player> player2) {

        this.checkers.init(abstractMatrixBuilder);
        CheckersApp.fill(this.checkers.getCheckers());

        try {
            Player iaPlayer1 = player1
                    .getConstructor(int[][].class, boolean.class, Player.class)
                    .newInstance(this.checkers.getCheckers(), true, null);

            Player iaPlayer2 = player2
                    .getConstructor(int[][].class, boolean.class, Player.class)
                    .newInstance(this.checkers.getCheckers(), false, iaPlayer1);
            iaPlayer1.setOpponent(iaPlayer2);

            this.checkers.setPlayer1(iaPlayer1);
            this.checkers.setPlayer2(iaPlayer2);

            this.stopCheckers();
            checkers.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initEvent() {

        this.buttonStart.setOnMouseClicked(mouseEvent -> {

            if (!checkers.isRunning()) {

                int indexBuilder = choiceBox.getSelectionModel().getSelectedIndex();
                int indexPlayer1 = choiceIaPlayer1.getSelectionModel().getSelectedIndex();
                int indexPlayer2 = choiceIaPlayer2.getSelectionModel().getSelectedIndex();

                if (indexBuilder == -1 || indexPlayer1 == -1 || indexPlayer2 == -1) {
                    LoggerApp.message("No pawn maker selected");
                } else {
                    try {
                        AbstractMatrixBuilder abstractMatrixBuilder =
                                this.builderMap.get(choiceBox.getSelectionModel().getSelectedItem())
                                        .getConstructor().newInstance();

                        initCheckers(abstractMatrixBuilder,
                                this.iaMap.get(
                                        choiceIaPlayer1.getSelectionModel().getSelectedItem()),
                                this.iaMap.get(
                                        choiceIaPlayer2.getSelectionModel().getSelectedItem()));
                    } catch (Exception e) {
                        LoggerApp.error(e.getMessage());
                    }
                }
            }
        });

        this.buttonStop.setOnMouseClicked(mouseEvent -> {
            if (checkers.isRunning()) {
                checkers.cancel();
            }
            LoggerApp.clear();
        });
    }

    /**
     * Gets grid pane.
     *
     * @return the grid pane
     */
    public GridPane getGridPane() {
        return gridPane;
    }

    /**
     * Gets console background.
     *
     * @return the console background
     */
    public StackPane getConsoleBackground() {
        return consoleBackground;
    }

    /**
     * Gets logger text.
     *
     * @return the logger text
     */
    public TextFlow getLoggerText() {
        return loggerText;
    }

    /**
     * Gets choice box.
     *
     * @return the choice box
     */
    public ChoiceBox<String> getChoiceBox() {
        return choiceBox;
    }

    /**
     * Stop checkerboard service if it is running.
     */
    public void stopCheckers() {
        if (checkers.isRunning()) {
            checkers.cancel();
        }
        LoggerApp.clear();
    }
}
