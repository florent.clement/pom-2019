package fr.univ_lyon1.info.m1.pom2019.model.pawn;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class DameTest {

    @Test
    public void testCantEatSameTeam() {
        for (int color = 2; color >= -2; color -= 4) {
            for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                    if ((x + y) % 2 == 1) {
                        continue;
                    }

                    // Given
                    int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    board[x][y] = color;
                    if (x - 1 >= 0) {
                        if (y - 1 >= 0) {
                            board[x - 1][y - 1] = color;
                        }
                        if (y + 1 < Constants.CHECKERS_SIZE) {
                            board[x - 1][y + 1] = color;
                        }
                    }
                    if (x + 1 < Constants.CHECKERS_SIZE) {
                        if (y - 1 >= 0) {
                            board[x + 1][y - 1] = color;
                        }
                        if (y + 1 < Constants.CHECKERS_SIZE) {
                            board[x + 1][y + 1] = color;
                        }
                    }
                    Dame dame = new Dame(new Coord(x, y), color);

                    // When
                    List<Action> actions = dame.getDplPossible(board);

                    // Then
                    Assert.assertTrue(actions.isEmpty());
                }
            }
        }
    }

    @Test
    public void diagonalMove() {
        for (int color = 2; color >= -2; color -= 4) {
            for (int x = 0; x < Constants.CHECKERS_SIZE; x += 9) {
                for (int y = 0; y < Constants.CHECKERS_SIZE; y += 9) {
                    if ((x + y) % 2 == 1) {
                        continue;
                    }

                    // Given
                    int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    board[x][y] = color;
                    Dame dame = new Dame(new Coord(x, y), color);

                    // When
                    List<Action> actions = dame.getDplPossible(board);

                    // Then
                    Assert.assertEquals(9, actions.size());
                }
            }
        }
    }

    @Test
    public void crossMove() {
        for (int color = 2; color >= -2; color -= 4) {
            for (int pos = 4; pos <= 5; ++pos) {
                // Given
                int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                board[pos][pos] = color;
                Dame dame = new Dame(new Coord(pos, pos), color);

                // When
                List<Action> actions = dame.getDplPossible(board);

                // Then
                Assert.assertEquals(17, actions.size());
            }
        }
    }

    @Test
    public void distanceEat() {
        for (int color = 2; color >= -2; color -= 4) {
            for (int pos = 0; pos < Constants.CHECKERS_SIZE; pos += 9) {
                for (int capt = 1; capt < Constants.CHECKERS_SIZE - 1; ++capt) {
                    // Given
                    int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    board[pos][pos] = color;
                    board[capt][capt] = color > 0 ? color - 4 : color + 4;
                    Dame dame = new Dame(new Coord(pos, pos), color);

                    // When
                    List<Action> actions = dame.getDplPossible(board);

                    // Then
                    int count = 0;
                    Assert.assertFalse(actions.isEmpty());
                    for (Action action : actions) {
                        if (!action.getCapturedPieces().isEmpty()) {
                            ++count;
                            Assert.assertEquals(1, action.getCapturedPieces().size());
                            Coord captured = action.getCapturedPieces().get(0);
                            Assert.assertEquals(capt, captured.getX());
                            Assert.assertEquals(capt, captured.getY());
                        }
                    }
                    Assert.assertTrue(count > 0);
                    if (pos == 0) {
                        Assert.assertEquals(Constants.CHECKERS_SIZE - capt - 1, count);
                    } else {
                        Assert.assertEquals(capt, count);
                    }
                }
            }
        }
    }

    @Test
    public void crossEat() {
        for (int color = 2; color >= -2; color -= 4) {
            for (int pos = 4; pos <= 5; ++pos) {
                for (int dist = 1; dist < 4; ++dist) {
                    // Given
                    int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    board[pos][pos] = color;
                    int color2 = color > 0 ? color - 3 : color + 3;
                    board[pos + dist][pos + dist] = color2;
                    board[pos + dist][pos - dist] = color2;
                    board[pos - dist][pos + dist] = color2;
                    board[pos - dist][pos - dist] = color2;
                    Dame dame = new Dame(new Coord(pos, pos), color);

                    // When
                    List<Action> actions = dame.getDplPossible(board);

                    // Then
                    int count = 0;
                    Assert.assertFalse(actions.isEmpty());
                    for (Action action : actions) {
                        if (!action.getCapturedPieces().isEmpty()) {
                            ++count;
                            Assert.assertTrue(1 == action.getCapturedPieces().size()
                                    || 2 == action.getCapturedPieces().size());
                        }
                    }
                    Assert.assertTrue(count > 0);
                    int openDist = (4 - dist);
                    int possible = (openDist * 4 + 1) + openDist * openDist * 2
                            + openDist * (openDist + 1) * 2;
                    Assert.assertEquals(possible, count);
                }
            }
        }
    }

    @Test
    public void chainDiagonalEat() {
        for (int color = 2; color >= -2; color -= 4) {
            for (int pos = 0; pos < Constants.CHECKERS_SIZE; pos += 9) {
                // Given
                int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                board[pos][pos] = color;
                for (int capt = 1; capt < Constants.CHECKERS_SIZE - 1; capt += 2) {
                    board[capt][capt] = color > 0 ? color - 4 : color + 4;
                }
                Dame dame = new Dame(new Coord(pos, pos), color);

                // When
                List<Action> actions = dame.getDplPossible(board);

                // Then
                Assert.assertFalse(actions.isEmpty());
                Action last = actions.get(actions.size() - 1);
                Assert.assertFalse(last.getCapturedPieces().isEmpty());
                Assert.assertEquals(4, last.getCapturedPieces().size());
            }
        }
    }

    @Test
    public void blockedMovement() {
        for (int color = 2; color >= -2; --color) {
            if (color == 0) {
                continue;
            }

            // Given
            int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
            for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                    if ((x + y) % 2 == 1) {
                        continue;
                    }
                    board[x][y] = color;
                }
            }

            for (int color2 = 2; color2 >= -2; color2 -= 4) {
                for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                    for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                        if ((x + y) % 2 == 1) {
                            continue;
                        }
                        // When
                        board[x][y] = color2;
                        Dame dame = new Dame(new Coord(x, y), color2);

                        List<Action> actions = dame.getDplPossible(board);

                        // Assert
                        Assert.assertTrue(actions.isEmpty());
                    }
                }
            }
        }
    }

    @Test
    public void squareEat() {
        for (int color = 2; color >= -2; color -= 4) {
            // Given
            int color2 = color > 0 ? color - 4 : color + 4;
            int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
            board[4][0] = color;
            board[5][1] = color2;
            board[6][8] = color2;
            board[3][7] = color2;
            board[5][3] = color2;
            Dame dame = new Dame(new Coord(4, 0), color);

            // When
            List<Action> actions = dame.getDplPossible(board);

            // Then
            Assert.assertFalse(actions.isEmpty());
            Action last = actions.get(actions.size() - 1);
            Assert.assertFalse(last.getCapturedPieces().isEmpty());
            Assert.assertEquals(4, last.getCapturedPieces().size());

        }
    }

    @Test
    public void notBlocked() {
        for (int color = 1; color >= -1; color -= 2) {
            for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                    // Given
                    int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
                    damier[x][y] = color;
                    Dame dame = new Dame(new Coord(x, y), color);

                    // When
                    List<Coord> coords = dame.getBlockCoord(damier);

                    // Then
                    Assert.assertTrue(coords.isEmpty());
                }
            }
        }
    }

    @Test
    public void blockedByOpposingTeam() {
        for (int color = 1; color >= -1; color -= 2) {
            // Given
            int color2 = color > 0 ? color - 2 : color + 2;
            int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];

            for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                    Dame dame = new Dame(new Coord(x, y), color);

                    int left = x - 1;
                    int right = x + 1;
                    int up = y + 1;
                    int down = y - 1;

                    boolean hasAny = true;
                    while (hasAny) {

                        hasAny = false;

                        if (left >= 0 && left < Constants.CHECKERS_SIZE) {
                            if (up >= 0 && up < Constants.CHECKERS_SIZE) {
                                damier[left][up] = color2;
                                hasAny = true;
                            }
                            if (down >= 0 && down < Constants.CHECKERS_SIZE) {
                                damier[left][down] = color2;
                                hasAny = true;
                            }
                        }
                        if (right >= 0 && right < Constants.CHECKERS_SIZE) {
                            if (up >= 0 && up < Constants.CHECKERS_SIZE) {
                                damier[right][up] = color2;
                                hasAny = true;
                            }
                            if (down >= 0 && down < Constants.CHECKERS_SIZE) {
                                damier[right][down] = color2;
                                hasAny = true;
                            }
                        }

                        // When
                        List<Coord> coords = dame.getBlockCoord(damier);

                        // Then
                        Assert.assertTrue(coords.isEmpty());

                        if (left >= 0 && left < Constants.CHECKERS_SIZE) {
                            if (up >= 0 && up < Constants.CHECKERS_SIZE) {
                                damier[left][up] = 0;
                            }
                            if (down >= 0 && down < Constants.CHECKERS_SIZE) {
                                damier[left][down] = 0;
                            }
                        }
                        if (right >= 0 && right < Constants.CHECKERS_SIZE) {
                            if (up >= 0 && up < Constants.CHECKERS_SIZE) {
                                damier[right][up] = 0;
                            }
                            if (down >= 0 && down < Constants.CHECKERS_SIZE) {
                                damier[right][down] = 0;
                            }
                        }

                        --left;
                        ++right;
                        ++up;
                        --down;
                    }
                }
            }
        }
    }

    @Test
    public void blockedBySameTeam() {
        for (int color = 1; color >= -1; color -= 2) {
            // Given
            int[][] damier = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];

            for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {
                    Dame dame = new Dame(new Coord(x, y), color);

                    int left = x - 1;
                    int right = x + 1;
                    int up = y + 1;
                    int down = y - 1;
                    int count = 0;

                    boolean hasAny = true;
                    while (hasAny) {

                        hasAny = false;

                        if (left >= 0 && left < Constants.CHECKERS_SIZE) {
                            if (up >= 0 && up < Constants.CHECKERS_SIZE) {
                                damier[left][up] = color;
                                hasAny = true;
                                ++count;
                            }
                            if (down >= 0 && down < Constants.CHECKERS_SIZE) {
                                damier[left][down] = color;
                                hasAny = true;
                                ++count;
                            }
                        }
                        if (right >= 0 && right < Constants.CHECKERS_SIZE) {
                            if (up >= 0 && up < Constants.CHECKERS_SIZE) {
                                damier[right][up] = color;
                                hasAny = true;
                                ++count;
                            }
                            if (down >= 0 && down < Constants.CHECKERS_SIZE) {
                                damier[right][down] = color;
                                hasAny = true;
                                ++count;
                            }
                        }

                        // When
                        List<Coord> coords = dame.getBlockCoord(damier);

                        // Then
                        Assert.assertEquals(count, coords.size());

                        if (left >= 0 && left < Constants.CHECKERS_SIZE) {
                            if (up >= 0 && up < Constants.CHECKERS_SIZE) {
                                damier[left][up] = 0;
                            }
                            if (down >= 0 && down < Constants.CHECKERS_SIZE) {
                                damier[left][down] = 0;
                            }
                        }
                        if (right >= 0 && right < Constants.CHECKERS_SIZE) {
                            if (up >= 0 && up < Constants.CHECKERS_SIZE) {
                                damier[right][up] = 0;
                            }
                            if (down >= 0 && down < Constants.CHECKERS_SIZE) {
                                damier[right][down] = 0;
                            }
                        }

                        --left;
                        ++right;
                        ++up;
                        --down;
                        count = 0;
                    }
                }
            }
        }
    }

}