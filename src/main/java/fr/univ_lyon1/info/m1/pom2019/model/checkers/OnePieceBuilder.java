package fr.univ_lyon1.info.m1.pom2019.model.checkers;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;

/**
 * Pawn layout generator.
 */
public class OnePieceBuilder extends AbstractMatrixBuilder {


    /**
     * Generate black pawn.
     * @param matrix Id matrix
     */
    private void buildBlackPiece(int[][] matrix) {

        int posX = (int)(Math.random() * (Constants.CHECKERS_SIZE / 2));
        int posY = (int)(Math.random() * 4);


        matrix[(posX * 2) + ((Constants.CHECKERS_SIZE - posY - 1) % 2)]
                [Constants.CHECKERS_SIZE - posY - 1] = -1;
    }

    /**
     * Generate white pawn.
     * @param matrix Id matrix
     */
    private void buildWhitePiece(int[][] matrix) {

        int posX = (int)(Math.random() * (Constants.CHECKERS_SIZE / 2));
        int posY = (int)(Math.random() * 4);

        matrix[(posX * 2) + (posY % 2)][posY] = 1;
    }


    @Override
    public int[][] build() {
        int[][] result = this.initCheckers(0);

        this.buildBlackPiece(result);
        this.buildWhitePiece(result);

        return result;
    }

    @Override
    public String getName() {
        return "Single pawn";
    }
}
