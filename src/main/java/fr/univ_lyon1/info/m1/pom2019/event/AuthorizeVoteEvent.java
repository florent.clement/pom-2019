package fr.univ_lyon1.info.m1.pom2019.event;


import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;


/**
 * The type Authorize vote event.
 *
 * @param <G> the type parameter
 * @param <V> the type parameter
 */
public class AuthorizeVoteEvent<G extends Group, V extends Vote> extends Event<G, V> {

    private final G group;
    private final int[][] currentCheckers;
    private final int weight;


    /**
     * Instantiates a new Authorize vote event.
     *
     * @param src             the src
     * @param currentCheckers the current checkers
     * @param group           the group
     * @param weight          the weight
     */
    public AuthorizeVoteEvent(
            Listeners<G, V> src, int[][] currentCheckers, G group, int weight) {

        super(src);
        this.currentCheckers = currentCheckers;
        this.group = group;
        this.weight = weight;
    }

    @Override
    public Type typeEvent() {
        return Type.AUTHORIZE_VOTE_GROUP;
    }

    @Override
    public int[][] getCurrentCheckers() {
        return this.currentCheckers;
    }

    /**
     * Gets group.
     *
     * @return the group
     */
    public G getGroup() {
        return group;
    }

    /**
     * Gets weight.
     *
     * @return the weight
     */
    public int getWeight() {
        return weight;
    }
}