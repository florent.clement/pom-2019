package fr.univ_lyon1.info.m1.pom2019.action;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;

import java.util.Arrays;
import java.util.List;

/**
 * Builder class for Coord.
 */
public class CoordBuilder {

    /**
     * Method for building Coord from String
     * @param str Input string to build Coord.
     * @return Coord if build successful, null otherwise.
     */
    public static Coord buildFromString(String str) {
        Coord res = null;

        if (str != null && !str.isEmpty()) {

            String clean = str.replace("\n", "").trim();
            if (!clean.isEmpty() && clean.startsWith("(") && clean.endsWith(")")) {

                String unCapsule = clean.replace("(", "")
                        .replace(")", "");
                if (!unCapsule.isEmpty()) {

                    String[] args = unCapsule.split(",", 0);
                    List<String> arguments = Arrays.asList(args);
                    if (arguments.size() == 2) {

                        int x = -2;
                        int y = -2;

                        try {
                            x = Integer.parseInt(arguments.get(0));
                            y = Integer.parseInt(arguments.get(1));
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            return null;
                        }

                        if (x >= 0 && x < Constants.CHECKERS_SIZE
                                && y >= 0 && y < Constants.CHECKERS_SIZE) {

                            return new Coord(x, y);
                        }
                    }
                }
            }
        }

        return res;
    }
}
