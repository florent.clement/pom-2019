package fr.univ_lyon1.info.m1.pom2019.model.player;

import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.strategy.AbstractStrategy;
import fr.univ_lyon1.info.m1.pom2019.strategy.GroupLimitStrategy;


/**
 * The type Group limit player.
 */
public class GroupLimitPlayer extends IaPlayer {
    /**
     * Instantiates a new Group limit player.
     *
     * @param checkers   the checkers
     * @param whiteColor the white color
     * @param opponent   the opponent
     */
    public GroupLimitPlayer(int[][] checkers, boolean whiteColor, Player opponent) {
        super(checkers,whiteColor, opponent);
    }

    @Override
    public AbstractStrategy<?, ?> getStrategy(
            AbstractPawn abstractPawn,
            Player player, Player opponent) {
        return new GroupLimitStrategy<>(
                abstractPawn,
                player,
                opponent
        );
    }

    @Override
    public String getNameIa() {
        return "Player with group size limit";
    }
}
