package fr.univ_lyon1.info.m1.pom2019.model.group;

import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.Pawn;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class GroupBuilderTest {
    private int[][] damier = new int[10][10];

    @Test
    public void build1() {
        for (int color = 1; color >= -1; color -= 2) {
            // Given
            LinkedList<AbstractPawn> pions = new LinkedList<>();
            pions.add( new Pawn(new Coord(0, 0), color));
            pions.add(new Pawn(new Coord(1, 1), color));
            pions.add(new Pawn(new Coord(2, 2), color));

            // When
            List<Group> groupes = GroupBuilder.build(pions);

            // Then
            Assert.assertNotNull(groupes);
            Assert.assertFalse(groupes.isEmpty());
            int empties = 0;
            int singles = 0;
            int pairs = 0;
            int triplets = 0;
            int quadruplets = 0;
            System.out.println("Combinations");
            for (Group g : groupes) {
                Assert.assertNotNull(g);
                if (g.getGroup().size() == 0) {
                    ++empties;
                }
                if (g.getGroup().size() == 1) {
                    ++singles;
                }
                if (g.getGroup().size() == 2) {
                    ++pairs;
                }
                if (g.getGroup().size() == 3) {
                    ++triplets;
                }
                if (g.getGroup().size() == 4) {
                    ++quadruplets;
                }
            }
            // As defined in GroupBuilder
            Assert.assertEquals(0, empties);
            Assert.assertEquals(3, singles);
            // Using combinatorial calculus n!/(k!(n-k)!)
            Assert.assertEquals(3, pairs);
            Assert.assertEquals(1, triplets);
            Assert.assertEquals(0, quadruplets);
            // Verify sum of possible combinations is equal to resulting
            // number of groups
            Assert.assertEquals(7, groupes.size());
        }
    }

    @Test
    public void build2() {
        for (int color = 1; color >= -1; color -= 2) {
            // Given
            LinkedList<AbstractPawn> pions = new LinkedList<>();
            for (int i = 0; i < 15; ++i) {
                pions.add(new Pawn(new Coord(i % 10, Math.floorDiv(i, 10)), color));
            }

            // When
            List<Group> groupes = GroupBuilder.build(pions);

            // Then
            Assert.assertNotNull(groupes);
            Assert.assertFalse(groupes.isEmpty());
            int empties = 0;
            int singles = 0;
            int pairs = 0;
            int triplets = 0;
            int quadruplets = 0;
            for (Group g : groupes) {
                Assert.assertNotNull(g);
                if (g.getGroup().size() == 0) {
                    ++empties;
                }
                if (g.getGroup().size() == 1) {
                    ++singles;
                }
                if (g.getGroup().size() == 2) {
                    ++pairs;
                }
                if (g.getGroup().size() == 3) {
                    ++triplets;
                }
                if (g.getGroup().size() == 4) {
                    ++quadruplets;
                }
            }
            // As defined in GroupBuilder
            Assert.assertEquals(0, empties);
            Assert.assertEquals(15, singles);
            // Using combinatorial calculus n!/(k!(n-k)!)
            Assert.assertEquals(105, pairs);
            Assert.assertEquals(455, triplets);
            Assert.assertEquals(1365, quadruplets);
            // Verify sum of possible combinations is equal to resulting
            // number of groups
            Assert.assertEquals(1940, groupes.size());
        }
    }

    @Test
    public void build3() {
        for (int color = 1; color >= -1; color -= 2) {
            // Given
            LinkedList<AbstractPawn> pions = new LinkedList<>();
            for (int i = 0; i < 20; ++i) {
                pions.add(new Pawn(new Coord(i % 10, Math.floorDiv(i, 10)), color));
            }

            // When
            List<Group> groupes = GroupBuilder.build(pions);

            // Then
            Assert.assertNotNull(groupes);
            Assert.assertFalse(groupes.isEmpty());
            int empties = 0;
            int singles = 0;
            int pairs = 0;
            int triplets = 0;
            int quadruplets = 0;
            for (Group g : groupes) {
                Assert.assertNotNull(g);
                if (g.getGroup().size() == 0) {
                    ++empties;
                }
                if (g.getGroup().size() == 1) {
                    ++singles;
                }
                if (g.getGroup().size() == 2) {
                    ++pairs;
                }
                if (g.getGroup().size() == 3) {
                    ++triplets;
                }
                if (g.getGroup().size() == 4) {
                    ++quadruplets;
                }
            }
            // As defined in GroupBuilder
            Assert.assertEquals(0, empties);
            Assert.assertEquals(20, singles);
            // Using combinatorial calculus n!/(k!(n-k)!)
            Assert.assertEquals(190, pairs);
            Assert.assertEquals(1140, triplets);
            Assert.assertEquals(4845, quadruplets);
            // Verify sum of possible combinations is equal to resulting
            // number of groups
            Assert.assertEquals(6195, groupes.size());
        }
    }
}