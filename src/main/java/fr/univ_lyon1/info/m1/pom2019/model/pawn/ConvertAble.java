package fr.univ_lyon1.info.m1.pom2019.model.pawn;


import fr.univ_lyon1.info.m1.pom2019.action.Coord;

import java.util.List;

/**
 * The interface Convert able.
 */
public interface ConvertAble {

    /**
     * Gets conversion list.
     *
     * @return the conversion list
     */
    List<Class<? extends AbstractPawn>> getConversionList();

    /**
     * Is convertible boolean.
     *
     * @param classConversion the class conversion
     * @param board           the board
     * @param coord           the coord
     * @return the boolean
     */
    boolean isConvertible(
            Class<? extends AbstractPawn> classConversion, int[][] board, Coord coord);

    /**
     * Convert abstract pawn.
     *
     * @param classConversion the class conversion
     * @return the abstract pawn
     */
    AbstractPawn convert(Class<? extends AbstractPawn> classConversion);
}
