package fr.univ_lyon1.info.m1.pom2019.model.pawn;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.globals.Tools;
import fr.univ_lyon1.info.m1.pom2019.evaluation.PointEvaluation;

import java.util.LinkedList;
import java.util.List;

public class Dame extends AbstractPawn {

    public Dame(Coord pos, int id) {
        super(pos, id);
    }

    private static List<Action> move(int[][] board, Dame dame, int vecH, int vecV) {
        List<Action> res = new LinkedList<>();
        int x = dame.getPosition().getX() + vecH;
        int y = dame.getPosition().getY() + vecV;

        while (x < Constants.CHECKERS_SIZE && y < Constants.CHECKERS_SIZE
                && x >= 0 && y >= 0 && board[x][y] == 0) {

            Action action = new Action(dame);
            action.getMovements().add(new Coord(x, y));
            action.setStartingPos(dame.getPosition().clone());
            res.add(action);
            x = x + vecH;
            y = y + vecV;
        }

        return res;
    }

    private Coord getNextBlockedCoord(int[][] board, int vecH, int vecV,
                                      int startX, int startY) {

        int captureX = startX + vecH;
        int captureY = startY + vecV;

        while (captureX < Constants.CHECKERS_SIZE && captureY < Constants.CHECKERS_SIZE
                && captureX >= 0 && captureY >= 0 && board[captureX][captureY] == 0) {
            captureX = captureX + vecH;
            captureY = captureY + vecV;
        }

        return new Coord(captureX, captureY);
    }

    private List<Action> capture(int[][] board, int vecH, int vecV,
                                 int startX, int startY, Action prevAction) {
        List<Action> res = new LinkedList<>();

        Coord captureCoord = this.getNextBlockedCoord(board, vecH, vecV, startX, startY);

        boolean white = this.getId() > 0;

        if (AbstractPawn.canEat(board,
                captureCoord.getX(), captureCoord.getY(), white)) {


            int moveX = captureCoord.getX() + vecH;
            int moveY = captureCoord.getY() + vecV;

            while (moveX < Constants.CHECKERS_SIZE && moveY < Constants.CHECKERS_SIZE
                    && moveX >= 0 && moveY >= 0 && board[moveX][moveY] == 0) {

                Action action = AbstractPawn.eat(this,
                        prevAction, moveX, moveY, captureCoord.getX(), captureCoord.getY());

                res.add(action);
                moveX = moveX + vecH;
                moveY = moveY + vecV;
            }
        }

        return res;
    }

    @Override
    public List<Action> getDplPossible(int[][] board) {
        LinkedList<Action> possible = new LinkedList<>();
        LinkedList<Action> visited = new LinkedList<>();

        final int left = -1;
        final int right = 1;
        final int up = 1;
        final int down = -1;

        List<Action> moveLeftUp = move(board, this, left, up);
        List<Action> moveLeftDown = move(board, this, left, down);
        List<Action> moveRightUp = move(board, this, right, up);
        List<Action> moveRightDown = move(board, this, right, down);

        possible.addAll(moveLeftUp);
        possible.addAll(moveLeftDown);
        possible.addAll(moveRightUp);
        possible.addAll(moveRightDown);


        int x = this.getPosition().getX();
        int y = this.getPosition().getY();
        Action prevAction = null;
        boolean hasAny = true;
        int[][] newBoard;
        while (hasAny) {

            if (prevAction != null) {
                newBoard = Tools.applyActionOnTempBoard(board, prevAction);
            } else {
                newBoard = board;
            }

            List<Action> captures = capture(newBoard, left, up, x, y, prevAction);
            captures.addAll(capture(newBoard, left, down, x, y, prevAction));
            captures.addAll(capture(newBoard, right, up, x, y, prevAction));
            captures.addAll(capture(newBoard, right, down, x, y, prevAction));

            possible.addAll(captures);
            visited.addAll(captures);

            if (!visited.isEmpty()) {
                prevAction = visited.getFirst();
                visited.removeFirst();
                Coord update = prevAction.getMovements().get(
                        prevAction.getMovements().size() - 1);
                x = update.getX();
                y = update.getY();
            } else {
                hasAny = false;
            }
        }

        return possible;
    }

    @Override
    public List<Coord> getBlockCoord(int[][] board) {
        List<Coord> res = new LinkedList<>();

        final int startX = this.getPosition().getX();
        final int startY = this.getPosition().getY();
        boolean white = this.getId() > 0;
        final int left = -1;
        final int right = 1;
        final int up = 1;
        final int down = -1;

        List<Coord> coords = new LinkedList<>();
        coords.add(this.getNextBlockedCoord(board, left, up, startX, startY));
        coords.add(this.getNextBlockedCoord(board, right, up, startX, startY));
        coords.add(this.getNextBlockedCoord(board, left, down, startX, startY));
        coords.add(this.getNextBlockedCoord(board, right, down, startX, startY));

        for (Coord coord : coords) {
            if (AbstractPawn.isBlockedByTeam(board, coord.getX(), coord.getY(), white)) {
                res.add(coord);
            }
        }

        return res;
    }

    @Override
    public int typePawnId() {
        return 2;
    }

    @Override
    public Type getProfile(PointEvaluation pointEvaluation, int[][] board,
                           List<AbstractPawn> owner, List<AbstractPawn> opponent) {

        return pointEvaluation.determineProfile(this, board, owner, opponent);
    }
}
