package fr.univ_lyon1.info.m1.pom2019.model.checkers;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;

/**
 * Pawn layout generator.
 */
public class ClassicBuilder extends AbstractMatrixBuilder {


    /**
     * Generate black pawn.
     * @param matrix Id matrix
     */
    private void buildBlackPiece(int[][] matrix) {

        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {

            for (int y = 0; y < 4; y++) {
                if ((x + Constants.CHECKERS_SIZE - y - 1) % 2 == 1) {
                    continue;
                }
                matrix[x][Constants.CHECKERS_SIZE - y - 1] = -1;
            }
        }
    }

    /**
     * Generate white pawn.
     * @param matrix Id matrix
     */
    private void buildWhitePiece(int[][] matrix) {

        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {

            for (int y = 0; y < 4; y++) {
                if ((x + y) % 2 == 1) {
                    continue;
                }
                matrix[x][y] = 1;
            }
        }
    }


    @Override
    public int[][] build() {
        int[][] result = this.initCheckers();

        for (int i = 0; i < Constants.CHECKERS_SIZE; i++) {
            for (int j = 0; j < Constants.CHECKERS_SIZE; j++) {
                result[i][j] = 0;
            }
        }

        this.buildBlackPiece(result);
        this.buildWhitePiece(result);

        return result;
    }

    @Override
    public String getName() {
        return "Classic party";
    }
}
