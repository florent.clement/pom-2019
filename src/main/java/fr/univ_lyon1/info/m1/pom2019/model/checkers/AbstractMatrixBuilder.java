package fr.univ_lyon1.info.m1.pom2019.model.checkers;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.logger.LoggerApp;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Pawn layout generator.
 */
public abstract class AbstractMatrixBuilder {

    /**
     * Build matrix.
     *
     * @return Matrix int [ ] [ ]
     */
    public abstract int[][] build();

    /**
     * Returns the class of objects inheriting from AbstractMatrixBuilder.
     *
     * @return List de class
     */
    public static Set<Class<? extends AbstractMatrixBuilder>> getSubClass() {
        Reflections reflections = new Reflections("fr.univ_lyon1.info.m1.pom2019.model.checkers");

        return reflections.getSubTypesOf(AbstractMatrixBuilder.class);
    }

    /**
     * Returns the name of each AI.
     *
     * @return Map map
     */
    public static Map<String, Class<? extends AbstractMatrixBuilder>> mapTypeBuilder() {

        Map<String, Class<? extends AbstractMatrixBuilder>> result = new HashMap<>();

        for (Class<? extends  AbstractMatrixBuilder> classBuilder : getSubClass()) {

            try {
                AbstractMatrixBuilder abstractMatrixBuilder = classBuilder
                        .getConstructor().newInstance();

                result.put(abstractMatrixBuilder.getName(), classBuilder);
            } catch (Exception ex) {
                LoggerApp.error(ex.getMessage());
            }
        }
        return result;
    }

    /**
     * Init checkers int [ ] [ ].
     *
     * @return the int [ ] [ ]
     */
    protected int[][] initCheckers() {
        return new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
    }

    protected int[][] initCheckers(int value) {
        int[][] result = this.initCheckers();

        for (int i = 0; i < Constants.CHECKERS_SIZE; i++) {
            for (int j = 0; j < Constants.CHECKERS_SIZE; j++) {
                result[i][j] = value;
            }
        }
        return result;
    }

    /**
     * Builder's name.
     *
     * @return String name
     */
    public abstract String getName();
}
