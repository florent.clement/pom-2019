package fr.univ_lyon1.info.m1.pom2019.evaluation;

import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.evaluation.Evaluators;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class EvaluatorsTest {

    @Test
    public void evaluateImmediateCapture() {
        int[][] board = new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE];
        List<Coord> enemies = new ArrayList<>();

        for (int color = 1; color >= -1; color -= 2) {
            for (int type = 1; type <= 2; ++type) {
                for (int x = 0; x < Constants.CHECKERS_SIZE; ++x) {
                    for (int y = 0; y < Constants.CHECKERS_SIZE; ++y) {

                        if ((x + y) % 2 == 1) {
                            continue;
                        }


                        board[x][y] = color * type;

                        // Assert no immediate captures when board is empty.
                        int res = Evaluators.evaluateImmediateCapture(board, x, y, color * type);
                        Assert.assertEquals(0, res);

                        // Enemy distance to pawn.
                        int gap = 1;
                        // Biggest gap possible an enemy can have from pawn.
                        while (gap <= Constants.CHECKERS_SIZE - 2) {

                            for (int vecX = -1; vecX <= 1; vecX += 2) {
                                for (int vecY = -1; vecY <= 1; vecY += 2) {

                                    if (x - vecX < 0 || x - vecX >= Constants.CHECKERS_SIZE
                                            || y - vecY < 0 || y - vecY >= Constants.CHECKERS_SIZE
                                            || board[x - vecX][y - vecY] != 0
                                            || x + vecX * gap < 0
                                            || x + vecX * gap >= Constants.CHECKERS_SIZE
                                            || y + vecY * gap < 0
                                            || y + vecY * gap >= Constants.CHECKERS_SIZE
                                            || board[x + vecX * gap][y + vecY * gap] != 0) {
                                        continue;
                                    }

                                    int enemyX = x + vecX;
                                    int enemyY = y + vecY;
                                    enemies.add(new Coord(enemyX, enemyY));
                                    if (gap > 1) {
                                        board[enemyX][enemyY] = color * -1;
                                    } else {
                                        board[enemyX][enemyY] = color * -2;
                                    }
                                }
                            }

                            int count = Evaluators.evaluateImmediateCapture(board,
                                    x, y, color * type);
                            Assert.assertEquals(enemies.size(), count);

                            for (Coord coord : enemies) {
                                board[coord.getX()][coord.getY()] = 0;
                            }

                            enemies.clear();

                            ++gap;
                        }

                        board[x][y] = 0;
                    }
                }
            }
        }
    }
}