package fr.univ_lyon1.info.m1.pom2019.event;


import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;

/**
 * Class MoveEvent.
 */
public class MoveEvent<G extends Group, V extends Vote> extends Event<G, V> {

    private final AbstractPawn abstractPawn;
    private final int[][] currentCheckers;

    /**
     * Event constructor movement.
     *
     * @param src             Source of the event
     * @param currentCheckers Checkers source
     * @param abstractPawn    Pawn source
     */
    public MoveEvent(Listeners<G, V> src, int[][] currentCheckers, AbstractPawn abstractPawn) {
        super(src);
        this.currentCheckers = currentCheckers;
        this.abstractPawn = abstractPawn;
    }

    @Override
    public Type typeEvent() {
        return Type.MOVE;
    }

    @Override
    public int[][] getCurrentCheckers() {
        return this.currentCheckers;
    }

    /**
     * Gets abstract pawn.
     *
     * @return the abstract pawn
     */
    public AbstractPawn getAbstractPawn() {
        return abstractPawn;
    }
}