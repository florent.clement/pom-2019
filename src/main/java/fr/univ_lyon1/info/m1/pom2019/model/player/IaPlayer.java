package fr.univ_lyon1.info.m1.pom2019.model.player;

import fr.univ_lyon1.info.m1.pom2019.action.Action;
import fr.univ_lyon1.info.m1.pom2019.action.Coord;
import fr.univ_lyon1.info.m1.pom2019.event.Listeners;
import fr.univ_lyon1.info.m1.pom2019.exception.ConflictMoveException;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.thread.StrategyListenersRunner;
import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import fr.univ_lyon1.info.m1.pom2019.globals.Tools;
import fr.univ_lyon1.info.m1.pom2019.logger.LoggerApp;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.thread.Monitor;
import fr.univ_lyon1.info.m1.pom2019.vote.Vote;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implementation d'IA.
 */
public abstract class IaPlayer extends Player {

    private final List<Action> actionList = new ArrayList<>();
    /**
     * The Monitor.
     */
    protected Monitor<StrategyListenersRunner<? extends Group, ? extends Vote>> monitor;
    private int[][] newCheckers;


    /**
     * Instantiates a new Ia player.
     *
     * @param checkers   the checkers
     * @param whiteColor the white color
     * @param opponent   the opponent
     */
    public IaPlayer(int[][] checkers, boolean whiteColor, Player opponent) {
        super(checkers, whiteColor, opponent);
    }

    @Override
    public List<Action> doAction() throws InterruptedException {
        this.newCheckers = Tools.cloneCheckers(this.getCheckers());
        this.handlePieceInit(this.getPieces());
        this.actionList.clear();
        this.monitor.run();

        return actionList;
    }

    /**
     * For each group of pawns belonging to Player, removes pieces contained in capturedPieces.
     * If a group becomes void of pieces, the group is removed from the group list.
     * @param list Pawn list to delete
     */
    @Override
    public void removePawn(List<Coord> list) {
        List<AbstractPawn> capturedPieces = this.getPieces().stream().filter(abstractPawn ->
                list.contains(abstractPawn.getPosition())).collect(Collectors.toList());

        this.getPieces().removeAll(capturedPieces);
        this.handlePieceInit(this.getPieces());
    }

    @Override
    public final void handlePieceInit(List<AbstractPawn> pieces) {

        List<Listeners<? extends Group, ? extends Vote>> list = new ArrayList<>();
        for (AbstractPawn abstractPawn : pieces) {
            list.add(getStrategy(abstractPawn, this, this.getOpponent()));
        }
        this.monitor = StrategyListenersRunner.build(list);
    }

    @Override
    final boolean canMove() {
        for (AbstractPawn abstractPawn : this.getPieces()) {
            if (abstractPawn.getDplPossible(getCheckers()).size() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the class of objects inheriting from IaPlayer.
     *
     * @return List de class
     */
    public static Set<Class<? extends IaPlayer>> getSubClass() {
        Reflections reflections = new Reflections("fr.univ_lyon1.info.m1.pom2019.model.player");

        return reflections.getSubTypesOf(IaPlayer.class);
    }

    /**
     * Returns the name of each AI.
     *
     * @return Map map
     */
    public static Map<String, Class<? extends IaPlayer>> mapTypePlayer() {

        Map<String, Class<? extends IaPlayer>> result = new HashMap<>();

        for (Class<? extends  IaPlayer> classIaPlayer : getSubClass()) {

            try {
                IaPlayer iaPlayer = classIaPlayer
                        .getConstructor(int[][].class, boolean.class, Player.class)
                        .newInstance(new int[Constants.CHECKERS_SIZE][Constants.CHECKERS_SIZE],
                                true, null);

                result.put(iaPlayer.getNameIa(), classIaPlayer);
            } catch (Exception ex) {
                LoggerApp.error(ex.getMessage());
            }
        }
        return result;
    }

    /**
     * Gets name ia.
     *
     * @return the name ia
     */
    public abstract String getNameIa();


    /**
     * Add action.
     *
     * @param checkers the checker
     * @param action   the action
     * @throws ConflictMoveException the conflict move exception
     */
    public synchronized void notifyAction(
            int[][] checkers, Action action) throws ConflictMoveException {

        if (Tools.compareCheckers(checkers, this.newCheckers)) {
            this.actionList.add(action);
            this.newCheckers = Tools.applyActionOnTempBoard(checkers, action);

        } else {
            throw new ConflictMoveException();
        }
    }

    /**
     * Get new checkers int [ ] [ ].
     *
     * @return the int [ ] [ ]
     */
    public synchronized int[][] getNewCheckers() {
        return newCheckers;
    }
}
