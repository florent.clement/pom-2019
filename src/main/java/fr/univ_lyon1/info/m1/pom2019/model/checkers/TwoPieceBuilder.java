package fr.univ_lyon1.info.m1.pom2019.model.checkers;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;

/**
 * Pawn layout generator.
 */
public class TwoPieceBuilder extends AbstractMatrixBuilder {


    /**
     * Generate black pawn.
     * @param matrix Id matrix
     */
    private void buildBlackPiece(int[][] matrix) {

        int posX1 = (int)(Math.random() * (Constants.CHECKERS_SIZE / 2));
        int posY1 = (int)(Math.random() * 4);

        int posX2 = posX1;
        int posY2 = posY1;

        while (posX1 == posX2) {
            posX2 = (int)(Math.random() * (Constants.CHECKERS_SIZE / 2));
        }

        while (posY1 == posY2) {
            posY2 = (int)(Math.random() * (Constants.CHECKERS_SIZE / 2));
        }

        matrix[(posX1 * 2) + ((Constants.CHECKERS_SIZE - posY1 - 1) % 2)]
                [Constants.CHECKERS_SIZE - posY1 - 1] = -1;

        matrix[(posX2 * 2) + ((Constants.CHECKERS_SIZE - posY2 - 1) % 2)]
                [Constants.CHECKERS_SIZE - posY2 - 1] = -1;
    }

    /**
     * Generate white pawn.
     * @param matrix Id matrix
     */
    private void buildWhitePiece(int[][] matrix) {

        int posX1 = (int)(Math.random() * (Constants.CHECKERS_SIZE / 2));
        int posY1 = (int)(Math.random() * 4);

        int posX2 = posX1;
        int posY2 = posY1;

        while (posX1 == posX2) {
            posX2 = (int)(Math.random() * (Constants.CHECKERS_SIZE / 2));
        }

        while (posY1 == posY2) {
            posY2 = (int)(Math.random() * (Constants.CHECKERS_SIZE / 2));
        }

        matrix[(posX1 * 2) + (posY1 % 2)][posY1] = 1;
        matrix[(posX2 * 2) + (posY2 % 2)][posY2] = 1;
    }


    @Override
    public int[][] build() {
        int[][] result = this.initCheckers(0);

        this.buildBlackPiece(result);
        this.buildWhitePiece(result);

        return result;
    }

    @Override
    public String getName() {
        return "Two Pawns";
    }
}
