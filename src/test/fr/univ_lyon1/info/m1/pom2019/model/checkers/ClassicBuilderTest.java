package fr.univ_lyon1.info.m1.pom2019.model.checkers;

import fr.univ_lyon1.info.m1.pom2019.globals.Constants;
import org.junit.Assert;
import org.junit.Test;

public class ClassicBuilderTest {

    @Test
    public void build() {
        // On vérifie qu'il y a au moins un pion de chaque

        ClassicBuilder classicBuilder = new ClassicBuilder();
        int[][] matrice = classicBuilder.build();

        int pionBlanc = 0, pionNoir = 0;

        for (int x = 0; x < Constants.CHECKERS_SIZE; x++) {
            for (int y = 0; y < Constants.CHECKERS_SIZE; y++) {
                int id = matrice[x][y];

                if (id > 0) {
                    pionBlanc ++;
                } else if (id < 0) {
                    pionNoir ++;
                }
            }
        }

        Assert.assertEquals(pionBlanc,20);
        Assert.assertEquals(pionNoir,20);
    }

    @Test
    public void getName() {

        ClassicBuilder classicBuilder = new ClassicBuilder();

        Assert.assertNotNull(classicBuilder.getName());
    }
}