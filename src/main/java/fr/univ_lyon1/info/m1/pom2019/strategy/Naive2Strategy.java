package fr.univ_lyon1.info.m1.pom2019.strategy;

import fr.univ_lyon1.info.m1.pom2019.event.AuthorizeVoteEvent;
import fr.univ_lyon1.info.m1.pom2019.event.Listeners;
import fr.univ_lyon1.info.m1.pom2019.model.group.Group;
import fr.univ_lyon1.info.m1.pom2019.model.pawn.AbstractPawn;
import fr.univ_lyon1.info.m1.pom2019.model.player.Player;
import fr.univ_lyon1.info.m1.pom2019.thread.StrategyListenersRunner;
import fr.univ_lyon1.info.m1.pom2019.vote.NaiveVote;
import fr.univ_lyon1.info.m1.pom2019.evaluation.NaiveEvaluation;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Naive 2 strategy.
 */
public class Naive2Strategy<G extends Group>
        extends AbstractVoteStrategy<G, NaiveEvaluation, NaiveVote> {

    /**
     * Instantiates a new Naive 2 strategy.
     *
     * @param abstractPawn the abstract pawn
     * @param player       the player
     * @param opponent     the opponent
     */
    public Naive2Strategy(AbstractPawn abstractPawn, Player player, Player opponent) {
        super(abstractPawn, player, opponent);
    }

    @Override
    protected void choiceGroupStep(
            StrategyListenersRunner<G, NaiveVote> src,
            List<Listeners<G, NaiveVote>> list) {

        if (this.getGroup() != null) {

            if (this.getGroup().getOwner() == this.getPawn()) {

                AuthorizeVoteEvent<G, NaiveVote> authorizeVoteEvent =
                        new AuthorizeVoteEvent<>(
                                this,
                                this.getCheckers(),
                                this.getGroup(),
                                (int) this.getGroup().getCreatedAt()
                        );

                src.sendEventAllListeners(authorizeVoteEvent,
                        new ArrayList<>(this.getExistingGroup().keySet()));
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected G createGroup() {
        return (G) new Group();
    }

    @Override
    protected NaiveVote createVote() {
        return new NaiveVote();
    }


    @Override
    protected NaiveEvaluation createEvaluate() {
        return new NaiveEvaluation();
    }

    @Override
    protected Integer customEvaluationCalc(
            NaiveEvaluation evaluate, int[][] board, AbstractPawn abstractPawn) {
        return evaluate.evaluate(board, abstractPawn, this.getOwnerPawn(), this.getOpponentPawn());
    }

    @Override
    public void authorizeVoteEvent(
            AuthorizeVoteEvent<G, NaiveVote> event,
            StrategyListenersRunner<G, NaiveVote> strategyListenersRunner,
            List<Listeners<G, NaiveVote>> list) {

        if (this.getGroup() != null) {

            if (this.getGroup().getOwner() == this.getPawn() && this.isEnableGroup()) {

                if ((int)this.getGroup().getCreatedAt() < event.getGroup().getCreatedAt()) {
                    this.setEnableGroup(false);
                }
            }
        }
    }
}