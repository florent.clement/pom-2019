package fr.univ_lyon1.info.m1.pom2019.logger;

import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class LoggerApp {

    private static TextFlow loggerText;

    /**
     *  Initialise le pane de rendu.
     * @param textFlow TextFlow où afficher les logs
     */
    public static void init(TextFlow textFlow) {
        LoggerApp.loggerText = textFlow;
    }

    /**
     * Ajoute un success dans les logs ( couleur verte ).
     * @param log Text à ajouter
     */
    public static void success(String log) {
        Text text = new Text(log.concat("\n"));
        text.setFill(Color.GREEN);
        loggerText.getChildren().add(text);
    }

    /**
     * Ajoute un message dans les logs ( couleur noire ).
     * @param log Text à ajouter
     */
    public static void message(String log) {
        Text text = new Text(log.concat("\n"));
        text.setFill(Color.BLACK);
        loggerText.getChildren().add(text);
    }

    /**
     * Ajoute un message dans les logs.
     * @param log message
     * @param color couleur
     */
    public static void message(String log, Color color) {
        Text text = new Text(log.concat("\n"));
        text.setFill(color);
        loggerText.getChildren().add(text);
    }

    /**
     * Ajoute un warning dans les logs ( couleur jaune ).
     * @param log Text à ajouter
     */
    public static void warn(String log) {
        Text text = new Text(log.concat("\n"));
        text.setFill(Color.ORANGE);
        loggerText.getChildren().add(text);
    }

    /**
     * Ajoute une erreur dans les logs ( couleur rouge ).
     * @param log Text à ajouter
     */
    public static void error(String log) {
        if (log != null) {
            Text text = new Text(log.concat("\n"));
            text.setFill(Color.RED);
            loggerText.getChildren().add(text);
        }
    }

    /**
     * Function for clearing Logger text flow messages.
     */
    public static void clear() {
        loggerText.getChildren().clear();
    }
}
