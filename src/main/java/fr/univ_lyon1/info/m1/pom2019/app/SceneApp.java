package fr.univ_lyon1.info.m1.pom2019.app;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.datafx.controller.flow.Flow;
import org.datafx.controller.flow.FlowHandler;
import org.datafx.controller.flow.FlowView;

import java.io.FileReader;
import java.io.IOException;

/**
 * Main class for the application (structure imposed by JavaFX).
 */
public class SceneApp extends Application {

    private static final String NAME_APP = "pom-2019";

    /**
     * Main function.
     *
     * @param args Args
     */
    public static void run(String[] args) {
        SceneApp.launch(args);
    }

    /**
     * With javafx, start() is called when the application is launched.
     */
    @Override
    public void start(Stage stage) throws Exception {
        Flow flow = new Flow(AppController.class);
        FlowHandler handler = flow.createHandler();
        stage.setScene(new Scene(handler.start()));
        FlowView<?> view = handler.getCurrentView();
        AppController appController =
                (AppController) view.getViewContext().getController();

        this.setup(stage);

        stage.setOnCloseRequest(event -> {
            appController.stopCheckers();
            Platform.exit();
            System.exit(0);
        });
        stage.show();
    }

    /**
     * Setting the scene according to predefined options.
     *
     * @param stage Stage
     */
    private void setup(Stage stage) {
        MavenXpp3Reader reader = new MavenXpp3Reader();
        Model model;
        String title = NAME_APP;
        try {
            model = reader.read(new FileReader("pom.xml"));
            title += " " + model.getVersion();
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
        stage.setTitle(title);
    }
}
